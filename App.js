import React, { Component } from 'react';
import AppNavigator from './src/navigator';
import { store } from './src/store';
import { Provider } from 'react-redux';
import { pushNotifications } from './src/services';

export default class App extends Component {
  componentDidMount() {
    //pushNotifications.configureIOS();
    pushNotifications.configure();
  }
  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
};


