import React, { Component } from 'react';
import { ScrollView, View, Text, ActivityIndicator, Image, TouchableOpacity, Keyboard } from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import RadioForm, {
    RadioButton,
    RadioButtonInput,
    RadioButtonLabel,
} from 'react-native-simple-radio-button';
import DateTimePicker from '@react-native-community/datetimepicker';
import Moment from 'moment';
import { updateUserProfile, resetErrorSuccessMsg, uploadImage } from '../../../actions';
import DefaultInput from '../../../config/inputs';
import colors from '../../../config/colors';
import CustomText from '../../../config/text';
import fonts from '../../../config/fonts';
import MediumText from '../../../components/presentational/typography/MediumText';
import CustomHeader from '../../../components/container/Header/CustomHeader';
import { showErrToast, showSuccessToast } from '../../../config/functions';
import { styles } from './ProfileStyles';


class UpdateUserProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ufirstName: null,
            uLastName: null,
            uPhone: null,
            chooseImgFilename: null,
            dob: null,
            genderType: [
                { label: 'Female', value: 'Female' },
                { label: 'Male', value: 'Male' },
                { label: 'Other', value: 'Other' }
            ],
            selectedGenderType: null,
            showDatePicker: false
        };
    }

    componentDidMount = () => {
        const { user } = this.props;
        this.setState({
            ufirstName: user.fname,
            uLastName: user.lname,
            uPhone: user.phone,
            chooseImgFilename: user.user_avatar,
            selectedGenderType: user.details ? user.details.gender : null,
            dob: user.details && user.details.dob != null ? new Date(user.details.dob) : new Date()
        });
    }


    updateProfileHandler = () => {
        Keyboard.dismiss();
        const { ufirstName, uLastName, uPhone, selectedGenderType, dob } = this.state;
        console.log("this.state.dob", this.state.dob)
        let userData = {
            fname: ufirstName,
            lname: uLastName,
            phone: uPhone,
            gender: selectedGenderType,
            dob: Moment(dob).format('YYYY-MM-DD'),
        }
        this.props.updateUserProfile(userData);
    };

    // Photo
    selectPhoto = async () => {
        const options = {
            title: 'Select Photo',
            takePhotoButtonTitle: 'Take Photo',
            chooseFromLibraryButtonTitle: 'Choose from gallery',
            quality: 1,
            //chooseImgFilename: null
        };

        ImagePicker.showImagePicker(options, response => {
            //console.log("Response = ", response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log(response);
                let fileSize = response.fileSize;
                var fileType = response.type;
                let msg = '';
                console.log('fileType', fileType);
                if (fileSize > 5242880) {
                    msg = 'The photo Size can not be more than 5MB'
                    showErrToast(msg);
                    return false;
                }
                else if (fileType !== 'image/jpeg' && fileType !== 'image/jpg' && fileType !== 'image/png' && fileType !== 'image/bmp') {
                    // msg = 'The photo must be a file of type: jpeg, jpg, bmp, png'
                    showErrToast(msg);
                    return false;
                }

                else if (response.uri) {
                    this.setState({ chooseImgFilename: response.uri });
                }


                this.props.uploadImage(
                    response.uri,
                    response.fileName,
                    response.type,
                );
            }
        });
    };

    genderTypeRadioHandler = val => {
        this.setState({ selectedGenderType: val }, () => {
            console.log("selectedGenderType", this.state.selectedGenderType)
        })

    };

    setDate = (event, date) => {
        console.log("Selected DAte", date);
        if (date !== undefined) {
            this.setState({
                dob: date,
                showDatePicker: false
            });
        } else {
            this.setState({
                dob: new Date,
                showDatePicker: false
            });
        }
    }


    render() {
        const { user, isloading, successMsg, errorMsg } = this.props;
        const { ufirstName, uLastName, uPhone, chooseImgFilename, genderType, selectedGenderType, dob, showDatePicker } = this.state;
        //console.log("successMsg", successMsg);

        const renderUpdateButton = () => {
            if (isloading) {
                return (

                    <View style={[styles.updateBtn, { backgroundColor: '' }]} >
                        <ActivityIndicator />
                    </View>

                );

            }
            return (
                <TouchableOpacity style={styles.updateBtn} onPress={() => this.updateProfileHandler()}>
                    <Text style={styles.btntxt}>Update</Text>
                </TouchableOpacity>
            )
        }

        const renderToastComponent = () => {
            let errMsg = '';
            //   console.log("error", errMsg);
            if (errorMsg) {
                errMsg = errorMsg;
                this.props.resetErrorSuccessMsg();
                return (showErrToast.showToast(errMsg));
            }
            else if (successMsg) {
                console.log("successMsg", successMsg);
                let succesMsg = successMsg;
                this.props.resetErrorSuccessMsg();
                return (showSuccessToast.showToast(succesMsg));
            }
            return null;
        }


        const editPhoto = () => {
            return (
                <View style={{ alignItems: 'center', marginTop: 10 }}>
                    <Image source={{ uri: chooseImgFilename }}
                        style={styles.avatarCont2} />

                    <TouchableOpacity onPress={this.selectPhoto.bind(this)}>
                        <MediumText style={{ color: colors.primaryColor, textAlign: 'center', fontSize: 14, marginTop: 6 }}>Change Profile Photo</MediumText>
                    </TouchableOpacity>
                </View>
            );

        }



        return (
            <View style={{ flex: 1 }}>
                <CustomHeader title='EDIT PROFILE' navigation={this.props.navigation} />

                <ScrollView
                    keyboardShouldPersistTaps='always'
                    showsVerticalScrollIndicator={false}
                    style={{ backgroundColor: colors.white }}>
                    <View style={{ marginLeft: 30, marginRight: 30 }}>
                        {editPhoto()}

                        <View style={{ width: '100%', marginTop: 20 }}>
                            <CustomText>First Name:</CustomText>
                            <DefaultInput
                                value={ufirstName}
                                onChangeText={val => this.setState({ ufirstName: val })}
                                onSubmitEditing={() => {
                                    lastName.focus();
                                }}
                                returnKeyType="next"
                                blurOnSubmit={false} />
                        </View>

                        <View style={{ width: '100%', marginTop: 10 }}>
                            <CustomText>Last Name:</CustomText>
                            <DefaultInput
                                ref={ref => {
                                    lastName = ref;
                                }}
                                value={uLastName}
                                onChangeText={val => this.setState({ uLastName: val })}
                                onSubmitEditing={() => {
                                    phone.focus();
                                }}
                                returnKeyType="next"
                                blurOnSubmit={false} />
                        </View>

                        <View style={{ width: '100%', marginTop: 10 }}>
                            <CustomText>Phone:</CustomText>
                            <DefaultInput
                                ref={ref => {
                                    phone = ref;
                                }}
                                value={uPhone}
                                onChangeText={val => this.setState({ uPhone: val })} />
                        </View>


                        <View >
                            <CustomText>Gender:</CustomText>
                            <View style={{ flexDirection: 'row', marginTop: 6 }}>
                                {genderType.map((obj, i) => (
                                    <View
                                        style={[
                                            styles.cardOptContent,
                                            {
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            },
                                            ,
                                        ]}
                                        key={i}>
                                        <RadioForm>
                                            <RadioButton>
                                                <RadioButtonInput
                                                    obj={obj}
                                                    index={i}
                                                    isSelected={selectedGenderType == obj.value}
                                                    borderWidth={1}
                                                    onPress={this.genderTypeRadioHandler}
                                                    buttonInnerColor={'#616184'}
                                                    buttonOuterColor={'#D8D8E1'}
                                                    buttonSize={8.56}
                                                    buttonOuterSize={17.11}
                                                    buttonWrapStyle={{
                                                        marginLeft: 10,
                                                        lineHeight: 50,
                                                    }}
                                                />
                                                <RadioButtonLabel
                                                    obj={{
                                                        label: obj.label,
                                                        value: obj.value,
                                                    }}
                                                    labelStyle={{
                                                        fontSize: 14,
                                                        color: colors.black,
                                                        marginLeft: 15,
                                                        fontFamily: fonts.barlowRegular
                                                    }}
                                                    resizeMode="contain"
                                                    labelWrapStyle={{}}
                                                    onPress={this.genderTypeRadioHandler}
                                                />
                                            </RadioButton>
                                        </RadioForm>
                                    </View>

                                ))}
                            </View>
                        </View>

                        <View style={{ width: '100%', marginTop: 10 }}>
                            <CustomText>Date of Birth:</CustomText>

                            <TouchableOpacity style={styles.dateContent} onPress={() => this.setState({ showDatePicker: true })}>
                                <MediumText>
                                    {Moment(this.state.dob).format('YYYY-DD-MM')}
                                </MediumText>
                                {showDatePicker && <DateTimePicker
                                    value={dob}
                                    mode='date'
                                    display='calendar'
                                    onChange={this.setDate}
                                />
                                }
                            </TouchableOpacity>

                        </View>


                        <View style={{ alignItems: 'center' }}>{renderUpdateButton()}</View>
                        {renderToastComponent()}
                    </View>
                </ScrollView >
            </View>
        );


    }


}

const mapStateToProps = state => {
    const { user, successMsg, errorMsg, isloading } = state.profile;
    return { user, successMsg, errorMsg, isloading };
}

export default UpdateUserProfileScreen = connect(mapStateToProps, {
    updateUserProfile, resetErrorSuccessMsg, uploadImage
})(UpdateUserProfileScreen);
