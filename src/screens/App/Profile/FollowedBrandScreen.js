import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { View, FlatList, ActivityIndicator, Text, TouchableOpacity } from 'react-native';
import SwipeableFlatList from 'react-native-swipeable-list';
import { unFollowBrand, showFollowedBrandList } from '../../../actions';
import CustomHeader from '../../../components/container/Header/CustomHeader';
import { styles } from './ProfileStyles';
import SingleFollowedBrand from '../../../components/container/Profile/SingleFollowedBrand';

class FollowedBrandScreen extends PureComponent {

    removeBrandFromFollow = (brandId) => {
        this.props.unFollowBrand(brandId, 'profile');
    }

    render() {
        const { followBrand, followBrandLoading, refreshList } = this.props;
        const followBData = followBrand ? followBrand.data : null;

        const quickActions = ({ item }) => {
            return (
                <View style={styles.qaContainer}>

                    <TouchableOpacity style={[styles.button, styles.delButton]}
                        onPress={() => this.removeBrandFromFollow(item.id)}>
                        <Text style={styles.buttonText}>UnFollow</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        const renderFollowBData = () => {
            if (Array.isArray(followBData)) {
                if (followBData.length) {
                    return (
                        <View style={styles.flatListCont}>
                            <SwipeableFlatList
                                data={followBData}
                                showVerticalScrollIndicator={false}
                                renderItem={({ item }) => (
                                    <SingleFollowedBrand
                                        followBData={item}
                                        removeBrandFromFollow={this.removeBrandFromFollow}

                                    />
                                )}
                                keyExtractor={(item, index) => (item.id.toString())}
                                contentContainerStyle={{ marginBottom: 30 }}
                                extraData={refreshList}
                                maxSwipeDistance={80}
                                renderQuickActions={quickActions}
                                shouldBounceOnMount={true}
                            //   onRefresh={() => onRefreshFavList()}
                            //  refreshing={favListRefreshing}

                            />

                        </View >
                    );
                }
                else {
                    return (
                        <View>
                            <Text>No brand is followed</Text>

                        </View>
                    )

                }
            }
            else {
                return (
                    <View>
                        <ActivityIndicator />
                    </View>
                )
            }
        };

        return (
            <View style={{ flex: 1 }}>
                <CustomHeader title='FOLLOWED BRAND' navigation={this.props.navigation} />
                {renderFollowBData()}
            </View>
        );
    }
}

const mapStateToProps = state => {
    const {
        followBrand,
        followBrandLoading,
        refreshList } = state.profile;
    return {
        followBrand,
        followBrandLoading,
        refreshList
    };
}
export default FollowedBrandScreen = connect(mapStateToProps, {
    unFollowBrand,
    showFollowedBrandList
})(FollowedBrandScreen);
