import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import colors from '../../../config/colors';
import CustomText from '../../../config/text';
import MediumText from '../../../components/presentational/typography/MediumText';
import CustomHeader from '../../../components/container/Header/CustomHeader';
import { styles } from './ProfileStyles';

class PersonalDetailScreen extends Component {
    state = {}
    render() {
        const { user } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <CustomHeader title='PERSONAL DETAIL' navigation={this.props.navigation} />

                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ backgroundColor: colors.white }}>

                    <View style={styles.topDetailCont}>
                        <Image source={{ uri: user.user_avatar }}
                            style={styles.avatarCont2} />
                        <View style={styles.detailCont}>
                            <CustomText style={{ fontSize: 10 }}>NAME</CustomText>
                            <MediumText style={styles.profileTxt}>{user.full_name}</MediumText>
                        </View>

                        <View style={styles.detailCont}>
                            <CustomText style={{ fontSize: 10 }}>EMAIL</CustomText>
                            <MediumText style={styles.profileTxt}>{user.email}</MediumText>
                        </View>

                        <View style={styles.detailCont}>
                            <CustomText style={{ fontSize: 10 }}>PHONE NUMBER </CustomText>
                            <MediumText style={styles.profileTxt}>{user.phone}</MediumText>
                        </View>

                        <View style={styles.detailCont}>
                            <CustomText style={{ fontSize: 10 }}>GENDER</CustomText>
                            <MediumText style={styles.profileTxt}>{user.details ? user.details.gender : null}</MediumText>
                        </View>
                        <View style={styles.detailCont}>
                            <CustomText style={{ fontSize: 10 }}>DATE OF BIRTH</CustomText>
                            <MediumText style={styles.profileTxt}>{user.details ? user.details.dob : null}</MediumText>
                        </View>
                    </View>
                    <View style={styles.editMainCont}>
                        <TouchableOpacity style={styles.editCont}
                            onPress={() => this.props.navigation.navigate("UpdateProfile")}>
                            <MediumText style={styles.editTxt}>
                                Edit Profile</MediumText>
                        </TouchableOpacity>
                    </View>
                </ScrollView >
            </View>

        );
    }
}
const mapStateToProps = state => {
    const {
        user,
    } = state.profile;
    return {
        user
    };
}

export default PersonalDetailScreen = connect(mapStateToProps, {})(PersonalDetailScreen);