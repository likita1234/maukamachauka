import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Linking } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import EnvelopIcon from 'react-native-vector-icons/FontAwesome';
import AppLogo from '../../../assets/img/appLogo.png';
import MediumText from '../../../components/presentational/typography/MediumText';
import CustomHeader from '../../../components/container/Header/CustomHeader';
import colors from '../../../config/colors';
import { styles } from './ProfileStyles';

class AboutUsScreen extends Component {


    render() {

        return (

            <View style={styles.container}>

                <CustomHeader title='ABOUT US' navigation={this.props.navigation} />
                <View style={styles.subHeader}>
                    <Image source={AppLogo} resizeMode="contain" style={{ width: 120, height: 120, marginBottom: 30 }} />
                    <MediumText>Version : {DeviceInfo.getVersion()}</MediumText>
                    <MediumText>Build No : {DeviceInfo.getBuildNumber()}</MediumText>
                </View>
                <View style={{
                    alignItems: 'center', margin: 20,
                }}>
                    <MediumText style={{ fontWeight: 'bold', fontSize: 18 }}>Don't miss any offers !</MediumText>
                    <MediumText style={{ textAlign: 'justify', marginTop: 10 }}>Worried that you might be missing any offers or planning to buy something and waiting for offers, Mauka Ma Chauka is just a perfect application for you.</MediumText>
                </View>
                <View style={{
                    alignItems: 'center', margin: 20,
                }}>
                    <MediumText style={{ fontWeight: 'bold', marginBottom: 10 }}>Any Queries?</MediumText>
                    <View style={{ flexDirection: 'row' }} >
                        <MediumText style={{ fontWeight: 'bold', fontSize: 12 }}>Visit us on :</MediumText>
                        <TouchableOpacity
                            style={{ flexDirection: 'row', marginLeft: 20 }} onPress={() =>
                                Linking.openURL('https://maukamachauka.com/')
                            }>
                            <Icon name="web" size={18} color={colors.primaryColor} />
                            <MediumText style={{ color: colors.primaryColor, marginLeft: 4, fontSize: 12 }}>https://maukamachauka.com/</MediumText>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row' }} >
                        <MediumText style={{ fontWeight: 'bold', fontSize: 12 }}>Email :</MediumText>
                        <TouchableOpacity
                            style={{ flexDirection: 'row', marginLeft: 40 }} onPress={() =>
                                Linking.openURL('mailto:' + 'maukamachauka@gmail.com')
                            }>
                            <EnvelopIcon name="envelope-o" size={18} color={colors.primaryColor} />
                            <MediumText style={{ color: colors.primaryColor, marginLeft: 4, fontSize: 12 }}>maukamachauka@gmail.com</MediumText>
                        </TouchableOpacity>
                    </View>
                </View>

            </View >
        );
    }
}

export default AboutUsScreen;
