import React, { Component, PureComponent } from 'react';
import { View, Text, ActivityIndicator, FlatList } from 'react-native';
import { connect } from 'react-redux';
import {
    unSubscribedOffer,
    subscribedOffer,
    showSubscribeOfferList,
    showUnSubscribeOfferList
} from '../../../actions';
import CustomHeader from '../../../components/container/Header/CustomHeader';
import { styles } from './ProfileStyles';
import SingleSubscribedOffer from '../../../components/container/Profile/SingleSubscribeOffer';
import SingleUnSubscribedOffer from '../../../components/container/Profile/SingleUnSubscribedOffer';
import colors from '../../../config/colors';

class SubscribeOfferScreen extends PureComponent {

    state = {
        idToUnSubscribe: null,
        idToSubscribe: null
    };

    unSubscribeOfferHandler = idToUnSubscribe => {
        this.setState({ idToUnSubscribe: idToUnSubscribe });
        this.props.unSubscribedOffer(idToUnSubscribe, () => {
            this.props.showUnSubscribeOfferList(() => {
                this.scrollUnSubscribeOfferListToTop();
            });

        });

    }


    subscribeOfferHandler = idToSubscribe => {
        this.setState({ idToSubscribe: idToSubscribe });
        this.props.subscribedOffer(idToSubscribe, () => {
            this.props.showSubscribeOfferList(() => {
                this.scrollSubscribeOfferListToEnd();
            });
        });
    }


    scrollSubscribeOfferListToEnd = () => {
        const { subscribedOffers } = this.props;
        const { idToSubscribe } = this.state;
        let objIndex = subscribedOffers.findIndex((obj => obj.id === idToSubscribe));
        const wait = new Promise((resolve) => setTimeout(resolve, 500));
        wait.then(() => {
            this.subscribeListRef.scrollToIndex({ animated: true, index: objIndex, viewPosition: 1 })
        });
    };

    scrollUnSubscribeOfferListToTop = () => {
        const { UnSubscribedOffers } = this.props;
        const { idToUnSubscribe } = this.state;
        let objIndex = UnSubscribedOffers.findIndex((obj => obj.id === idToUnSubscribe));
        const wait = new Promise((resolve) => setTimeout(resolve, 500));
        wait.then(() => {
            this.unSubscribeListRef.scrollToIndex({ animated: true, index: objIndex, viewPosition: 0 })
        });
    };





    render() {
        const {
            subOffListLoading,
            subscribedOffers,
            currIdUnSubscribe,
            UnSubOffListLoading,
            UnSubscribedOffers,
            currIdSubscribe,
        } = this.props;

        const renderSubscribedOfferList = () => {
            if (Array.isArray(subscribedOffers)) {
                if (subscribedOffers.length) {
                    return (
                        <View style={[styles.flatListCont, { marginRight: 0, marginTop: 6 }]}>
                            <Text style={styles.topHeading}>SUBSCRIBED CATEGORIES</Text>
                            <FlatList
                                ref={ref => this.subscribeListRef = ref}
                                data={subscribedOffers}
                                showVerticalScrollIndicator={false}
                                renderItem={({ item }) => (
                                    <SingleSubscribedOffer
                                        subscribedOffer={item}
                                        unSubscribeOffer={this.unSubscribeOfferHandler}
                                        currIdUnSubscribe={currIdUnSubscribe}
                                        idToSubscribe={this.state.idToSubscribe}
                                    />
                                )}
                                keyExtractor={(item, index) => (item.id.toString())}
                                contentContainerStyle={{ marginBottom: 30 }}
                            />


                        </View >
                    );
                }
                else {
                    return (
                        <View>
                            <Text>No offers added to subscribe list</Text>

                        </View>
                    )

                }
            }
            else {
                return (
                    <View>
                        <ActivityIndicator />
                    </View>
                )
            }
        };

        const renderUnSubscribedOfferList = () => {
            if (Array.isArray(UnSubscribedOffers)) {
                if (UnSubscribedOffers.length) {
                    return (
                        <View style={[styles.flatListCont, { marginTop: 8, borderTopColor: colors.darkgray, borderTopWidth: 0.4 }]}>

                            <Text style={[styles.topHeading, { marginTop: 14 }]}>UNSUBSCRIBED CATEGORIES</Text>
                            <FlatList
                                ref={ref => this.unSubscribeListRef = ref}
                                data={UnSubscribedOffers}
                                showVerticalScrollIndicator={false}
                                renderItem={({ item }) => (
                                    <SingleUnSubscribedOffer
                                        unSubscribedOffer={item}
                                        subscribeOffer={this.subscribeOfferHandler}
                                        currIdSubscribe={currIdSubscribe}
                                        idToUnSubscribe={this.state.idToUnSubscribe}

                                    />
                                )}
                                keyExtractor={(item, index) => (item.id.toString())}

                            />
                        </View>
                    );
                }
                else {
                    return (
                        <View>
                            <Text>No offers added to UnSubscribe list</Text>
                        </View>
                    )

                }
            }
            else {
                return (
                    <View>
                        <ActivityIndicator />
                    </View>
                )
            }
        };


        return (
            <View style={{ flex: 1 }}>
                <CustomHeader title='SUBSCRIBED/UNSUBSCRIBED CATEGORIES' navigation={this.props.navigation} />

                <View style={{ flex: 1, width: '100%' }}>
                    {renderSubscribedOfferList()}
                </View>

                <View style={{ flex: 1, width: '100%' }}>
                    {renderUnSubscribedOfferList()}
                </View>

            </View>
        );
    }
}

const mapStateToProps = state => {
    const {
        subOffListLoading,
        subscribedOffers,
        currIdUnSubscribe,
        UnSubOffListLoading,
        UnSubscribedOffers,
        currIdSubscribe,
    } = state.profile;
    return {
        subOffListLoading,
        subscribedOffers,
        currIdUnSubscribe,
        UnSubOffListLoading,
        UnSubscribedOffers,
        currIdSubscribe,
    };
}
export default SubscribeOfferScreen = connect(mapStateToProps, {
    unSubscribedOffer,
    subscribedOffer,
    showSubscribeOfferList,
    showUnSubscribeOfferList
})(SubscribeOfferScreen);