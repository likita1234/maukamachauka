import React, { PureComponent } from 'react';
import { View, ScrollView, Text, TouchableOpacity, Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import OfferIcon from 'react-native-vector-icons/MaterialIcons';
import UserIcon from 'react-native-vector-icons/AntDesign';
import {
    logOut,
    showUserFavOfferList,
    fetchUserInfo,
    showSubscribeOfferList,
    showUnSubscribeOfferList,
    showFollowedBrandList
} from '../../../actions';
import ProgressiveImage from '../../../components/container/Profile/ProgressiveImage';
import DefaultPerson from '../../../assets/img/defaultPerson.png';
import { styles } from './ProfileStyles';
import colors from '../../../config/colors';
import NoInternetNotice from '../../../components/presentational/notice/NoInternetNotice';


class ProfileScreen extends PureComponent {

    componentDidMount() {
        //   this.props.fetchUserInfo();
        this.props.showUserFavOfferList();
        this.props.showSubscribeOfferList();
        this.props.showUnSubscribeOfferList();
        this.props.showFollowedBrandList();
    };

    onretryNetConnectionHandler = () => {
        this.props.fetchUserInfo();
        this.props.showUserFavOfferList();
        this.props.showSubscribeOfferList();
        this.props.showUnSubscribeOfferList();
        this.props.showFollowedBrandList();
    };

    render() {
        const { favOffers, user, subscribedOffers, followBrand, noNetConnection } = this.props;

        const userAvatar = () => {
            if (user && user.user_avatar) {
                // return <FastImage source={{ uri: user.user_avatar, priority: FastImage.priority.high }}
                //     style={styles.avatarCont} />
                return <ProgressiveImage
                    thumbnailSource={{ uri: user.user_avatar }}
                    source={{ uri: user.user_avatar }}
                    style={{
                        width: 100,
                        height: 100,
                        borderRadius: 80,
                        borderWidth: 2,
                        borderColor: '#fff',
                    }}
                //resizeMode="cover"
                />
            } else {
                return <FastImage source={DefaultPerson}
                    style={styles.avatarCont} />
            }
        };

        const renderUserNameEmail = () => {
            if (user) {
                return (
                    <View >
                        <Text style={styles.nameTxt}>{user.full_name}</Text>
                        <Text style={styles.emailTxt}>{user.email}</Text>
                    </View>
                );
            }
            return null;
        };



        return (
            <ScrollView
                style={styles.topCont}
                contentContainerStyle={{ alignItems: 'center' }}
                showsVerticalScrollIndicator={false}>
                <View style={styles.subTopCont} >
                    <View style={{
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        {noNetConnection && (
                            <View>
                                <NoInternetNotice
                                    onretryNetConnection={this.onretryNetConnectionHandler}
                                />

                            </View>

                        )}
                        {userAvatar()}
                        <View style={styles.userInfoCont}>
                            {renderUserNameEmail()}
                        </View>

                    </View>

                </View>

                <View style={styles.favCont}>
                    {/* <View style={styles.mainTitle}>
                        <Text style={styles.mainHeadTxt}>General</Text>
                    </View> */}
                    <View style={styles.titleCont}>
                        <TouchableOpacity style={styles.subFavCont}
                            onPress={() => this.props.navigation.navigate("PersonalInfo")} >
                            <View style={{ flexDirection: 'row' }}>
                                <UserIcon name="user" size={24} color={colors.primaryColor} />
                                <Text style={styles.titleTxt}>
                                    Personal Detail</Text>
                            </View>
                            <View style={{ marginRight: 8 }}>
                                <Icon name="chevron-right"
                                    color={colors.primaryColor}
                                    size={28} />
                            </View>
                        </TouchableOpacity>
                    </View>



                    <View style={styles.titleCont}>
                        <TouchableOpacity style={styles.subFavCont} onPress={() => this.props.navigation.navigate("Subscribe")}>
                            <View style={{ flexDirection: 'row' }}>
                                <OfferIcon name="local-offer"
                                    color={colors.primaryColor}
                                    size={24} />
                                <Text style={styles.titleTxt}>
                                    Subscribed List ({subscribedOffers ? subscribedOffers.length : 0})</Text>
                            </View>
                            <View style={{ marginRight: 8 }}>
                                <Icon name="chevron-right"
                                    color={colors.primaryColor}
                                    size={28} />
                            </View>
                        </TouchableOpacity>

                    </View>

                    <View style={styles.titleCont}>
                        <TouchableOpacity style={styles.subFavCont} onPress={() => this.props.navigation.navigate("Follow")}>
                            <View style={{ flexDirection: 'row' }}>
                                <OfferIcon name="local-offer"
                                    color={colors.primaryColor}
                                    size={24} />
                                <Text style={styles.titleTxt}>
                                    Brand Preference ({followBrand ? followBrand.total : 0})</Text>
                            </View>
                            <View style={{ marginRight: 8 }}>
                                <Icon name="chevron-right"
                                    color={colors.primaryColor}
                                    size={28} />
                            </View>
                        </TouchableOpacity>

                    </View>




                    <View style={styles.titleCont}>
                        <TouchableOpacity style={styles.subFavCont} onPress={() => this.props.navigation.navigate("About")}>
                            <View style={{ flexDirection: 'row' }}>
                                <Icon name="application" size={24} color={colors.primaryColor} />
                                <Text style={styles.titleTxt}>About us</Text>
                            </View>
                            <View style={{ marginRight: 8 }}>
                                <Icon name="chevron-right"
                                    color={colors.primaryColor}
                                    size={28} />
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.titleCont}>
                        <TouchableOpacity style={styles.subFavCont} onPress={() => this.props.logOut()}>
                            <View style={{ flexDirection: 'row' }}>
                                <Icon name="logout" size={24} color={colors.primaryColor} />
                                <Text style={styles.titleTxt}>Logout</Text>
                            </View>
                            <View style={{ marginRight: 8 }}>
                                <Icon name="chevron-right"
                                    color={colors.primaryColor}
                                    size={28} />
                            </View>
                        </TouchableOpacity>
                    </View>

                </View >

            </ScrollView >
        );
    };
};

const mapStateToProps = state => {
    const {
        user,
        subscribedOffers,
        followBrand,
    } = state.profile;
    const { noNetConnection } = state.ui;
    const { favOffers } = state.dashboard;
    return {
        favOffers,
        user,
        subscribedOffers,
        followBrand,
        noNetConnection
    };
}

export default ProfileScreen = connect(mapStateToProps, {
    fetchUserInfo,
    logOut,
    showUserFavOfferList,
    showUnSubscribeOfferList,
    showSubscribeOfferList,
    showFollowedBrandList
})(ProfileScreen);
