import React, { PureComponent } from 'react';
import { View, Text, ActivityIndicator, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import SwipeableFlatList from 'react-native-swipeable-list';
import { updateUnFavouritePref, showUserFavOfferList, fetchUserOffers } from '../../../actions';
import { styles } from './ProfileStyles';
import SingleFavouriteOffer from '../../../components/container/Profile/SingleFavouriteOffer';
import colors from '../../../config/colors';
import NoInternetNotice from '../../../components/presentational/notice/NoInternetNotice';

class FavouriteOfferScreen extends PureComponent {
    state = {
        favListRefreshing: false,
        idToRemove: null
    }

    componentDidMount() {
        this.props.showUserFavOfferList();
    }

    onRefreshFavList = () => {
        this.setState({ favListRefreshing: true }, () => {
            this.props.showUserFavOfferList(() => {
                this.setState({ favListRefreshing: false })
            })
        })
    };


    removeOfferFromFavHandler = (offerId) => {
        this.setState({ idToRemove: offerId }, () => {
            this.props.updateUnFavouritePref(offerId, () => {
                this.props.fetchUserOffers(1, () => {
                    this.setState({ idToRemove: null })
                });
            });
        });
    };

    favListEmpty = () => {
        return (
            <View>
                <Text>No offers added to Favourite list</Text>
            </View>
        );
    }


    render() {
        const { favOffListLoading, favOffers, refreshList, noNetConnection } = this.props;
        const favOfferList = favOffers ? favOffers.data : null;
        const { favListRefreshing } = this.state;

        const renderHeader = () => {
            return (
                <View style={styles.offerHeader}>
                    <View style={{ alignItems: 'center', width: '100%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 14, marginLeft: 20, color: colors.white }}>MY FAVOURITE OFFERS</Text>
                    </View>


                </View>
            )
        };

        const quickActions = ({ item }) => {
            return (
                <View style={styles.qaContainer}>

                    <TouchableOpacity style={[styles.button, styles.delButton]}
                        onPress={() => this.removeOfferFromFavHandler(item.id)}>
                        <Text style={styles.buttonText}>Delete</Text>
                    </TouchableOpacity>
                </View>
            );
        }

        const renderFavListData = () => {
            if (Array.isArray(favOfferList)) {

                return (
                    <View style={styles.favListCont}>
                        <FlatList
                            data={favOfferList}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item }) => (
                                <SingleFavouriteOffer
                                    favOffer={item}
                                    idToRemove={this.state.idToRemove}
                                    removeOfferFromFav={this.removeOfferFromFavHandler}
                                />
                            )}
                            keyExtractor={(item, index) => (item.id.toString())}
                            onRefresh={() => this.onRefreshFavList()}
                            refreshing={favListRefreshing}
                            extraData={refreshList}
                            ListEmptyComponent={() => this.favListEmpty()}
                        // maxSwipeDistance={80}
                        // renderQuickActions={quickActions}
                        // shouldBounceOnMount={true}
                        />
                    </View>
                );
            }

            else {
                return (
                    <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, }}>
                        <ActivityIndicator />
                    </View>
                )
            }
        };

        return (
            <View style={{ flex: 1 }}>
                {/* <CustomHeader title='FAVOURITE OFFERS' navigation={this.props.navigation} /> */}
                {renderHeader()}
                {noNetConnection && (
                    <View>
                        <NoInternetNotice
                            onretryNetConnection={this.onretryNetConnectionHandler}
                        />

                    </View>

                )}
                {renderFavListData()}
            </View>
        );
    }
}

const mapStateToProps = state => {
    const { favOffListLoading,
        favOffers, refreshList } = state.dashboard;
    const { noNetConnection } = state.ui;

    return {
        favOffListLoading,
        favOffers,
        refreshList,
        noNetConnection
    };
}
export default FavouriteOfferScreen = connect(mapStateToProps,
    { updateUnFavouritePref, showUserFavOfferList, fetchUserOffers })(FavouriteOfferScreen);