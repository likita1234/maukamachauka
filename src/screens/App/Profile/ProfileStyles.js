import { StyleSheet } from 'react-native';
import colors from '../../../config/colors';
import shadow from '../../../config/shadow';
import constants from '../../../config/constants';


export const styles = StyleSheet.create({
    avatarCont: {
        width: 100,
        height: 100,
        borderRadius: 80,
        borderWidth: 2,
        borderColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 20,
        marginTop: 50,

    },
    mainTitle: {
        backgroundColor: colors.primaryColor,
        height: 34,
        marginTop: 2,
        marginBottom: 2,
        padding: 4
    },
    mainHeadTxt: {
        color: colors.white,
        fontWeight: 'bold',
        marginLeft: 10,
        fontSize: 16
    },
    txtStyle: {
        fontWeight: 'bold',
        marginTop: 4,
        textAlign: 'center'
    },
    titleTxt: {
        marginTop: 4,
        textAlign: 'center',
        marginBottom: 6,
        fontSize: 16,
        marginLeft: 8
    },
    titleCont: {
        backgroundColor: colors.white,
        height: 52,
        marginBottom: 2,
        justifyContent: 'center'
    },
    topCont: {
        marginBottom: 6,
        width: '100%',
        margin: 0,
        flex: 1,
    },
    subTopCont: {
        width: '100%',
        backgroundColor: colors.primaryColor,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    userInfoCont: {
        marginLeft: 20,
        marginTop: 20,
        marginBottom: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    nameTxt: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    },
    emailTxt: {
        color: '#fff',
        fontSize: 16,
        textAlign: 'center'
    },
    editIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 16,
        marginLeft: 10
    },
    logoutIcon: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    favCont: {
        width: '100%',
        backgroundColor: colors.darkgray,
        opacity: 0.9,
        justifyContent: 'space-evenly',
        marginTop: 10
    },
    subFavCont: {
        marginLeft: 20,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'

    },
    updateBtn: {
        width: 120,
        height: 36,
        backgroundColor: colors.primaryColor,
        marginTop: 8,
        borderRadius: 4
    },
    avatarCont2: {
        width: 80,
        height: 80,
        borderRadius: 80,
        borderWidth: 1,
        borderColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',

    },
    btntxt: {
        color: '#fff',
        textAlign: 'center',
        marginTop: 6,
        fontWeight: 'bold'
    },
    favListCont: {
        width: '100%',
        flex: 1,
        marginTop: 10,
        alignItems: 'center'
    },
    flatListCont: {
        width: '100%',
        flex: 1,
        marginTop: 10,
        // marginLeft: 10,
        // marginRight: 10
    },
    detailCont: {
        width: '100%',
        marginTop: 20,
        borderBottomColor: colors.lightgray,
        borderBottomWidth: 1
    },
    topDetailCont: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10
    },
    profileTxt: {
        fontSize: 16,
        marginBottom: 6,
        color: colors.black
    },
    editMainCont: {
        marginTop: 30,
        marginLeft: 20,
        marginRight: 20
    },
    editCont: {
        borderColor: colors.lightishgray,
        borderWidth: 1,
        width: '100%',
        height: 40,
        alignItems: 'center'
    },
    editTxt: {
        textAlign: 'center',
        padding: 6,
        fontSize: 14
    },
    offerHeader: {
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        padding: constants.viewPadding,
        paddingTop: 10,
        backgroundColor: colors.primaryColor,
        height: 58,
        ...shadow.shadowStyle,
    },
    qaContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 5
    },
    button: {
        width: 80,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 0.1,
        borderRadius: 10,
        // marginBottom: 2
    },

    delButton: {
        backgroundColor: colors.primaryColor,
    },
    buttonText: {
        color: colors.white,
        fontWeight: 'bold',
    },
    contentContainerStyle: {
        flexGrow: 1,
    },
    dateContent: {
        width: '100%',
        borderWidth: 1,
        borderColor: '#D8D8E1',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 24,
        paddingRight: 24,
        marginTop: 2,
        height: 50,
        marginBottom: 8,
        backgroundColor: 'white',
        color: 'black',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.0,
        elevation: 1,
    },
    topHeading: {
        fontSize: 10,
        marginBottom: 4,
        marginLeft: 10,
        color: colors.gray,
        fontWeight: 'bold',
    },
    container: {
        flex: 1,
    },
    subHeader: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40

    },
});