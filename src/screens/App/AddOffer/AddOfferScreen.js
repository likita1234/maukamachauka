import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image, Keyboard, YellowBox } from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import DateTimePicker from '@react-native-community/datetimepicker';
import Moment from 'moment';
import Icon from 'react-native-vector-icons/AntDesign';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import UploadFeat from '../../../assets/img/uploadFeat.jpg';
import { showErrToast, showSuccessToast } from '../../../config/functions';
import MediumText from '../../../components/presentational/typography/MediumText';
import DefaultInput from '../../../config/inputs';
import CustomText from '../../../config/text';
import fonts from '../../../config/fonts';
import colors from '../../../config/colors';
import { styles } from './AddOfferStyle';
import AddNewBrandModal from '../../../components/container/Modal/AddNewBrandModal';

YellowBox.ignoreWarnings(['VirtualizedLists should never be nested']);

class AddOfferScreen extends Component {
    state = {
        title: '',
        desc: '',
        chooseImgFilename: null,
        chooseBImgFilename: null,
        selectedCategory: [],
        selectedCatItem: [{}],
        //  selectedBrand:[],
        showDatePicker: false,
        validDate: new Date(),
        location: '',
        showAddBrandModal: false,

    }


    selectPhoto = async () => {
        const options = {
            title: 'Select Photo',
            takePhotoButtonTitle: 'Take Photo',
            chooseFromLibraryButtonTitle: 'Choose from gallery',
            quality: 1,
            //chooseImgFilename: null
        };

        ImagePicker.showImagePicker(options, response => {
            //console.log("Response = ", response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log(response);
                let fileSize = response.fileSize;
                var fileType = response.type;
                let msg = '';
                console.log('fileType', fileType);
                if (fileSize > 5242880) {
                    msg = 'The photo Size can not be more than 5MB'
                    showErrToast(msg);
                    return false;
                }
                else if (fileType !== 'image/jpeg' && fileType !== 'image/jpg' && fileType !== 'image/png' && fileType !== 'image/bmp') {
                    // msg = 'The photo must be a file of type: jpeg, jpg, bmp, png'
                    showErrToast(msg);
                    return false;
                }

                else if (response.uri) {
                    this.setState({ chooseImgFilename: response.uri });
                }


                // this.props.uploadImage(
                //     response.uri,
                //     response.fileName,
                //     response.type,
                // );
            }
        });
    };

    onSelectedCatItemsChange = (selectedCategory) => {
        console.log("selectedCategory", selectedCategory)
        this.setState({ selectedCategory });
    };


    onSelectedCatItemsObjectChange = (selectedCatItem) => {
        console.log(" onSelectedCatItemsObjectChange selectedCategory", selectedCatItem)
        this.setState({ selectedCatItem });
    };

    setDate = (event, date) => {
        if (date !== undefined) {
            this.setState({
                validDate: date,
                showDatePicker: false
            });
        } else {
            this.setState({
                validDate: new Date,
                showDatePicker: false
            });
        }
    };

    clearLocationText = () => {
        this.GooglePlacesRef.setAddressText("");
        this.setState({ location: '' })
        Keyboard.dismiss();
    };

    openAddBrandModal = () => {
        this.setState({ showAddBrandModal: true });
    };

    closeAddBrandModal = () => {
        this.setState({ showAddBrandModal: false });
    };

    scrollHandler = () => {
        if (this.GooglePlacesRef.getAddressText() != "") {
            this.scroll.scrollToEnd({ animated: true })
        }
    };

    submitForm = () => {
        const { title, desc, selectedCatItem, chooseImgFilename, validDate, location } = this.state;
        const formData = {
            title: title,
            desc: desc,
            selectedCatItem: selectedCatItem,
            chooseImgFilename: chooseImgFilename,
            validDate: validDate,
            location: location
        }
        console.log("formData", formData);
    }

    render() {
        const { categoryList } = this.props;
        const { showDatePicker, validDate, chooseImgFilename } = this.state;
        let date = new Date();
        const minDateValue = date.setDate(date.getDate() + 1);   // disabled today's date
        const renderHeader = () => {
            return (
                <View style={styles.offerHeader}>
                    <View style={{ alignItems: 'center', width: '100%' }}>
                        <Text style={styles.headingTxt}>ADD MY  OFFER</Text>
                    </View>
                </View>
            )
        };

        const renderCategoryList = () => {
            return (
                <View style={styles.catCont}>
                    <CustomText style={styles.titleTxt}>Categories:</CustomText>
                    <View style={styles.catSubCont}>
                        <SectionedMultiSelect
                            styles={{
                                selectToggle: {
                                    fontSize: 12,
                                    padding: 10
                                },
                                selectToggleText: {
                                    fontSize: 14,
                                    color: colors.default,
                                    fontFamily: fonts.barlowRegular
                                },
                                itemText: {
                                    fontWeight: "normal"
                                },
                                button: {
                                    backgroundColor: colors.primaryColor
                                },

                            }}
                            selectText="Select Categories"
                            items={categoryList}
                            uniqueKey="id"
                            showDropDowns={true}
                            onSelectedItemsChange={this.onSelectedCatItemsChange}
                            onSelectedItemObjectsChange={this.onSelectedCatItemsObjectChange}
                            selectedItems={this.state.selectedCategory}
                        />
                    </View>

                </View>

            )
        };

        const renderFeatImage = () => {
            return (
                <View style={styles.featCont}>
                    <CustomText style={styles.titleTxt}>Feature Image:</CustomText>
                    <TouchableOpacity style={styles.imgBtn} onPress={this.selectPhoto.bind(this)}>
                        <Image source={chooseImgFilename ? { uri: chooseImgFilename } : UploadFeat}
                            style={{ width: '100%', height: 180 }}
                            resizeMode="cover" />
                    </TouchableOpacity>
                </View >
            )
        };

        const renderBrandList = () => {
            return (
                <View style={styles.catCont}>
                    <CustomText style={styles.titleTxt}>Brand:</CustomText>
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <View style={styles.catSubCont}>
                            <SectionedMultiSelect
                                styles={{
                                    selectToggle: {
                                        fontSize: 12,
                                        padding: 10,
                                        color: colors.default
                                    },
                                    selectToggleText: {
                                        fontSize: 14,
                                        color: colors.default,
                                        fontFamily: fonts.barlowRegular
                                    },
                                    itemText: {
                                        fontWeight: "normal"
                                    },
                                    button: {
                                        backgroundColor: colors.primaryColor
                                    },

                                }}
                                single={true}
                                selectText="Select Brand"
                                items={categoryList}
                                uniqueKey="id"
                                showDropDowns={true}
                                onSelectedItemsChange={this.onSelectedCatItemsChange}
                                selectedItems={this.state.selectedCategory}
                            />
                        </View>
                        <TouchableOpacity
                            style={{ width: 40, marginLeft: 10, }}
                            onPress={() => this.setState({ showAddBrandModal: true })}>
                            <Text style={{ fontSize: 14, textAlign: 'center', color: colors.default, fontFamily: fonts.barlowRegular }}>Add Other</Text>
                        </TouchableOpacity>
                    </View>

                </View>

            )

        };

        const renderValidDate = () => {
            return (
                <View style={styles.dateCont}>
                    <CustomText style={styles.titleTxt}>Valid date:</CustomText>
                    <TouchableOpacity style={styles.dateContent} onPress={() => this.setState({ showDatePicker: true })}>
                        <MediumText style={{ color: colors.default }}>
                            {Moment(this.state.validDate).format('YYYY-DD-MM')}
                        </MediumText>
                        {showDatePicker && <DateTimePicker
                            value={validDate}
                            mode='date'
                            display='calendar'
                            onChange={this.setDate}
                            minimumDate={minDateValue}
                        />
                        }
                    </TouchableOpacity>

                </View>

            )
        };

        const renderLocation = () => {
            return (
                <View>
                    <View style={styles.locationCont}>
                        <CustomText style={styles.titleTxt}>Add Location:</CustomText>
                        <View style={{ width: '72%' }}>
                            <GooglePlacesAutocomplete
                                ref={(instance) => { this.GooglePlacesRef = instance }}
                                placeholder='Search'
                                placeholderTextColor={colors.default}
                                minLength={2}
                                autoFocus={false}
                                enablePoweredByContainer={false}
                                returnKeyType={'search'}
                                listViewDisplayed={false}
                                fetchDetails={true}
                                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                    console.log(data, details);
                                    this.setState({ location: data.description });
                                }}

                                textInputProps={{
                                    multiline: true,
                                    blurOnSubmit: true,

                                }}

                                query={{
                                    key: 'AIzaSyCG3TJHzznW44L8Rr4FMFPOyOrh_YUCa0I',
                                    language: 'en',
                                    components: 'country:np',
                                }}
                                renderRightButton={() =>
                                    <TouchableOpacity onPress={() => this.clearLocationText()} style={{ margin: 10, }}>
                                        <Icon name="closecircle" size={20} color={colors.lightgray} />
                                    </TouchableOpacity>}
                                styles={{
                                    textInputContainer: {
                                        height: 66,
                                        width: '100%',
                                        borderColor: colors.lightgray,
                                        borderWidth: 1,
                                        backgroundColor: 'white',

                                    },
                                    textInput: {
                                        fontSize: 14,
                                        height: 56,
                                        borderWidth: 0,
                                        fontFamily: fonts.barlowRegular

                                    },
                                    listView: {
                                        borderWidth: 1,
                                        borderColor: colors.lightgray,
                                    }

                                }}
                            />
                        </View>
                    </View>

                </View >
            );
        };

        return (
            <ScrollView
                keyboardShouldPersistTaps='always'
                showsVerticalScrollIndicator={false}
                style={{ backgroundColor: colors.white, flex: 2, paddingBottom: 120 }}
                //contentContainerStyle={{ alignItems: 'center' }}
                ref={re => this.scroll = re}
                onContentSizeChange={() => this.scrollHandler()}>
                {renderHeader()}
                < View style={{ flex: 1 }}>

                    <View style={{ width: '100%', marginTop: 20, flexDirection: 'row', marginLeft: 10, marginRight: 10, }}>
                        <CustomText style={{ width: 84, fontSize: 14 }}>Title:</CustomText>
                        <View style={{ width: '72%' }}>
                            <DefaultInput
                                style={{ elevation: 0, }}
                                placeholder={"Offer Title"}
                                placeholderTextColor={colors.default}
                                onChangeText={val => this.setState({ title: val })}
                                returnKeyType="next"
                                blurOnSubmit={false}
                            />
                        </View>
                    </View>

                    <View style={{ width: '100%', marginTop: 20, flexDirection: 'row', marginLeft: 10, marginRight: 10, }}>
                        <CustomText style={{ width: 84, fontSize: 14 }}>Description:</CustomText>
                        <View style={{ width: '72%' }}>
                            <DefaultInput
                                style={{ elevation: 0 }}
                                placeholder={"Offer Desciption"}
                                placeholderTextColor={colors.default}
                                onChangeText={val => this.setState({ desc: val })}
                                returnKeyType="next"
                                blurOnSubmit={false}
                                multiline={true} />
                        </View>
                    </View>

                </View >
                {renderCategoryList()}
                {renderFeatImage()}
                {renderBrandList()}
                {renderValidDate()}
                {renderLocation()}

                <View style={{ width: '100%', alignItems: 'center' }}>
                    <TouchableOpacity style={styles.submitBtn}
                        onPress={() => this.submitForm()}>
                        <Text style={styles.btnTxt}>Submit</Text>
                    </TouchableOpacity>
                </View>

                <AddNewBrandModal
                    visible={this.state.showAddBrandModal}
                    close={this.closeAddBrandModal}
                />

            </ScrollView >

        );
    }
}

const mapStateToProps = state => {
    const { categoryList } = state.dashboard;
    return { categoryList };

}

export default connect(mapStateToProps, {
})(AddOfferScreen);