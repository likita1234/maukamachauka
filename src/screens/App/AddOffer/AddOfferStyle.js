import { StyleSheet } from 'react-native';
import shadow from '../../../config/shadow';
import constants from '../../../config/constants';
import colors from '../../../config/colors';

export const styles = StyleSheet.create({
    headingTxt: {
        textAlign: 'center',
        fontSize: 14,
        marginLeft: 20,
        color: colors.white
    },
    offerHeader: {
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        padding: constants.viewPadding,
        paddingTop: 10,
        backgroundColor: colors.primaryColor,
        height: 58,
        ...shadow.shadowStyle,
    },
    dateCont: {
        marginTop: 20,
        flexDirection: 'row',
        marginRight: 10,
        width: '100%',
        marginLeft: 10
    },
    dateContent: {
        width: '72%',
        borderWidth: 1,
        borderColor: colors.lightgray,
        padding: 10,
        height: 44,
        marginBottom: 8,
        backgroundColor: 'white',
        color: 'black',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.0,
    },
    searchAddr: {
        height: 49,
        width: '100%',
        borderColor: colors.lightgray,
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.0,
        flexDirection: 'row'
    },
    autoList: {
        position: 'relative',
        top: -4,
        paddingTop: Platform.OS === 'ios' ? 20 : 40,
        zIndex: 8,
        maxHeight: 160,
        backgroundColor: '#fff',
        width: '100%',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        borderWidth: 1,
        borderColor: colors.darkgray
    },
    titleTxt: {
        width: 84,
        fontSize: 14,
        margin: 0
    },
    catCont: {
        marginTop: 20,
        flexDirection: 'row',
        marginRight: 10,
        width: '100%',
        marginLeft: 10
    },
    catSubCont: {
        width: '72%',
        borderColor: colors.lightgray,
        borderWidth: 1
    },
    featCont: {
        marginTop: 20,
        flexDirection: 'row',
        marginRight: 10,
        width: '100%',
        marginLeft: 10
    },
    imgBtn: {
        borderColor: colors.lightgray,
        borderWidth: 1,
        width: '72%',
        padding: 6
    },
    locationCont: {
        paddingTop: 20,
        flexDirection: 'row',
        marginLeft: 10, width: '100%',
        marginLeft: 10,
        marginBottom: 40
    },
    submitBtn: {
        backgroundColor: colors.primaryColor,
        width: 180,
        height: 40,
        borderRadius: 10,
        alignItems: 'center'
    },
    btnTxt: {
        color: colors.white,
        textAlign: 'center',
        fontSize: 16, marginTop: 6
    }
});