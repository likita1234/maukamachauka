import { StyleSheet } from 'react-native';
import spacing from '../../../config/spacing';
import colors from '../../../config/colors';
import shadow from '../../../config/shadow';
import constants from '../../../config/constants';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    offerList: {
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingBottom: 20
    },
    btnStyle: {
        width: 200,
        height: 36,
        backgroundColor: colors.primaryColor,
        marginTop: 8,
        borderRadius: 4
    },
    txtStyle: {
        color: '#fff',
        fontWeight: 'bold',
        marginTop: 4,
        textAlign: 'center'
    },
    headingTxt: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 6,
        //  marginTop: 6
    },
    subHeadTxt: {
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 10,
    },
    categoryTxt: {
        fontSize: 18,
        borderRadius: 6,
        color: colors.ypsDark,
        borderWidth: 1,
        borderColor: colors.gray,
        padding: 6,
        marginBottom: 6

    },
    categoryImgCont: {
        justifyContent: 'flex-end',
        height: 135,
    },
    catImgCont: {
        marginLeft: 10,
        width: 170,
        height: 180,
        marginTop: 10,
        marginRight: 10,
    },
    subView: {
        marginLeft: 40,
        marginRight: 40,
        height: '40%'
    },
    descTxt: {
        fontSize: 16,
        textAlign: 'justify'
    },
    brandCont: {
        top: -30,
        marginLeft: 8,
    },
    brandImg: {
        width: 60,
        height: 60,
        borderColor: colors.white,
        borderWidth: 1,
    },
    offerImg: {
        width: '100%',
        height: 180
    },
    btmCont: {
        width: '60%',
        height: 180,
        marginLeft: 10
    },
    brandTxt: {
        fontSize: 14,
        marginBottom: 6
    },
    brandPara: {
        fontSize: 12,
        textAlign: 'justify'
    },
    brandImgCont: {
        width: 400,
        height: 40,
        borderRadius: 180,
        marginRight: 10

    },
    brandImgCont2: {
        width: 90,
        height: 90,
        borderRadius: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 3,
        borderColor: colors.white,
    },
    mapViewHolder: {
        height: 200

    },
    mapContainer: {
        height: '100%',
    },
    followBtnTxt: {
        textAlign: 'center',
        padding: 2,
        color: colors.white,
        fontSize: 11,
        fontWeight: 'bold'
    },
    enquiryBtn: {
        backgroundColor: colors.white,
        borderRadius: 6,
        width: 80,
        height: 24,
        marginLeft: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginBottom: 10,
        borderColor: colors.lightgray,
        borderWidth: 1
    },
    enquiryBtnTxt: {
        textAlign: 'center',
        padding: 4,
        fontSize: 11,
        fontWeight: 'bold'
    },
    topBrandCont: {
        flex: 1,
        backgroundColor: colors.white
    },
    subTopBrandCont: {
        flexDirection: 'row',
        //   marginTop: 10,
        marginBottom: 20,
        alignItems: 'center'
    },
    brandImgHolder: {
        borderWidth: 2,
        width: 110,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        height: 110,
        borderRadius: 100,
        borderColor: colors.primaryColor,
    },
    brandName: {
        fontSize: 22,
        fontWeight: 'bold',
        //  textAlign: 'center',
    },
    bannerImg: {
        width: 80,
        height: 40
    },
    viewPager: {
        flex: 1,
    },

    catCont: {
        flex: 1,
        width: '100%',
        marginLeft: 10
    },
    offerHeader: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: colors.primaryColor,
        position: 'relative',
        zIndex: 10,
    },
    notificationCount: {
        fontSize: 10,
        marginTop: -2,
        color: colors.primaryColor,
    },
    countBox: {
        position: 'absolute',
        backgroundColor: colors.white,
        height: 25,
        minWidth: 20,
        borderWidth: 2,
        top: -5,
        right: -5,
        borderColor: colors.primaryColor,
        paddingHorizontal: 5,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    searchIcon: {
        marginRight: 6,
        marginTop: 8,
    },
    searchBtn: {
        flexDirection: 'row',
        borderRadius: 3,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingHorizontal: 10,
        backgroundColor: colors.white,
        marginLeft: 10,
        height: 41,
        flex: 1,
    },
    btnStyle: {
        width: '100%',
        height: 35,
    },
    avatarCont: {
        width: 38,
        height: 38,
        borderRadius: 80,
        borderWidth: 2,
        borderColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
    },
    searchBox: {
        width: '80%',
        alignSelf: 'center',
    },
    notificationBox: {
        width: '10%',
        justifyContent: 'center',
        alignItems: 'center',
        maxHeight: 40,
    },
    autocompleteContainer: {
        // flex: 1,
        position: "absolute",
        top: 0,
        right: 0,
        left: 0,
        //zIndex: 10,
        // width: '100%',
        backgroundColor: colors.white,

    },
    descriptionContainer: {
        backgroundColor: colors.white,
        //  marginTop: 6,
        width: '100%'
    },
    searchContainer: {
        flex: 1,
        height: 40,
        // width: '100%',
        backgroundColor: colors.white
        // alignItems: 'center',
    },
    itemText: {
        fontSize: 15,
        margin: 2
    },
    autoList: {
        backgroundColor: colors.white,
        flex: 1,
        borderWidth: 0,
        borderLeftColor: colors.primaryColor,
        borderWidth: 1,
        borderTopWidth: 0,
        borderRightColor: colors.primaryColor,
        borderBottomColor: colors.primaryColor,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
})