import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { View, Text, ActivityIndicator, Image, TouchableOpacity, Linking, Dimensions } from 'react-native';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FIcon from 'react-native-vector-icons/FontAwesome';
import WebIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Swiper from 'react-native-swiper';
import {
    showBrandDetail,
    showBrandOffer,
    followBrand,
    unFollowBrand,
    removeErrorSuccessMsg,
    updatelikePreference,
    updateDislikePreference,
    updateUnFavouritePref,
    updateFavouritePref,
    addlikePrefBrandOffers,
    removelikePrefBrandOffers,
    addFavPrefBrandOffers,
    removeFavPrefBrandOffers
} from '../../../actions';
import BrandOfferList from '../../../components/container/Dashboard/BrandOfferList';
import EnquiryMessageModal from '../../../components/container/Modal/EnquiryMessageModal';
import CustomHeader from '../../../components/container/Header/CustomHeader';
import MediumText from '../../../components/presentational/typography/MediumText';
import { styles } from './DashboardStyles';
import colors from '../../../config/colors';

const SwiperImgItem = props => {
    return (
        <View style={styles.slideContainer}>
            {props.item.image_src &&
                <Image source={{ uri: props.item.image_src }} style={{ width: '100%', height: 80 }} />}
        </View>
    );
};

class BrandDetailScreen extends PureComponent {
    state = {
        activeTxt: null,
        btnPress: false,
        showEnquiryModal: false,
        startswiper: false,
        snippetTextShown: -1,
    };
    componentDidMount() {
        const { navigation } = this.props;
        const brandId = navigation.getParam('brandId', null);
        this.props.showBrandDetail(brandId);
        setTimeout(() => {
            this.setState({ startswiper: true });
        }, 1000);
        this.props.showBrandOffer(brandId);
    };

    brandListEmpty = () => {
        return (
            <View>
                <Text>No Brand Offer</Text>
            </View>
        );
    };
    componentWillUnmount() {
        this.setState({ activeTxt: '' });
    };

    followUnfollowBrand = () => {
        const { activeTxt } = this.state;
        const { brandInfo } = this.props;
        let brandId = brandInfo.id
        if (activeTxt === 'Follow') {

            this.props.followBrand(brandId, () => {
                console.log("brandInfo after follow", brandInfo);
                this.setState({ activeTxt: 'UnFollow' });
            });

        }
        else {
            this.props.unFollowBrand(brandId, 'dashboard', () => {
                console.log("brandInfo after unfollow", brandInfo);
                this.setState({ activeTxt: 'Follow' });
            });
        }
    };

    openEnquiryModal = () => {
        this.setState({ showEnquiryModal: true });
    };

    closeEnquiryModal = () => {
        console.log("closeEnquiryModal");
        this.setState({ showEnquiryModal: false }, () => {
            this.props.removeErrorSuccessMsg();
        }
        );
    };

    _renderSwiper() {
        return (
            <View
                style={{
                    height: 120,
                    width: '100%'
                }}>
                <Swiper
                    showsButtons={false}
                    removeClippedSubviews={false}
                    autoplay={true}
                    autoplayTimeout={3}
                    autoplayDirection={true}
                    activeDotColor={colors.primaryColor}
                >
                    {this.renderFeatureData()}
                </ Swiper>
            </View>
        );
    };


    renderFeatureData = () => {
        const { brandInfo, brandDetailLoading } = this.props;
        let bannerData = brandInfo ? brandInfo.banners : null;
        // if (!brandDetailLoading || bannerData)
        //     return <ActivityIndicator size="small" />;
        let res = bannerData.map((banner, i) => {
            return <SwiperImgItem key={i} item={banner} />;
        });
        return res;
    };


    toggleNumOfSnippetLines = index => {
        this.setState({
            snippetTextShown: this.state.snippetTextShown === index ? -1 : index,
        });
    };

    onClickOfferPreference = (preferenceVal, offerId) => {
        console.log("onClickOfferPreference");
        if (preferenceVal === 'alreadyliked') {
            this.props.removelikePrefBrandOffers(offerId);
            this.props.updateDislikePreference(offerId);
        }
        if (preferenceVal === 'unliked') {
            this.props.addlikePrefBrandOffers(offerId);
            this.props.updatelikePreference(offerId);
        }
    };

    onClickFavOfferHandler = (FavPreferenceVal, offerId) => {
        if (FavPreferenceVal === 'alreadyFav') {
            this.props.removeFavPrefBrandOffers(offerId);
            this.props.updateUnFavouritePref(offerId);
        }

        if (FavPreferenceVal === 'unFav') {
            console.log('Favourite offer');
            this.props.addFavPrefBrandOffers(offerId);
            this.props.updateFavouritePref(offerId);
        }

    };
    render() {
        const { brandDetailLoading, brandInfo, brandOfferList, brandListLoading, refreshList, error, success } = this.props;
        if (brandInfo && brandInfo.followed_status === false) {
            this.setState({ activeTxt: 'Follow' });
        } else {
            this.setState({ activeTxt: 'UnFollow' });

        }


        const { activeTxt } = this.state;

        const renderFollowBtn = () => {
            return (
                <TouchableOpacity
                    style={{ backgroundColor: activeTxt === 'Follow' ? colors.accentColor : colors.red, borderRadius: 6, width: 80, height: 24 }}
                    onPress={() => {
                        this.followUnfollowBrand(activeTxt);
                    }}
                >
                    <Text style={styles.followBtnTxt}>
                        {this.state.activeTxt}</Text>
                </TouchableOpacity>
            );
        }

        const renderEnquiryBtn = () => {
            return (
                <TouchableOpacity style={styles.enquiryBtn}
                    onPress={() =>
                        //   this.props.navigation.navigate("enquiry", { idToEnquiry: brandInfo.id, fromScreen: 'brand' });
                        this.openEnquiryModal()
                    } >
                    <Icon name="message" size={12} />
                    <Text style={styles.enquiryBtnTxt}>
                        Enquiry</Text>
                </TouchableOpacity>
            );

        }

        const renderFindMoreDetail = () => {
            return (
                <View style={{ marginBottom: 10 }} >
                    <Text style={styles.brandName}>{brandInfo.name}</Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 12, marginBottom: 4, color: colors.gray, marginTop: 10 }}>Find more on:</Text>
                    <TouchableOpacity style={{ flexDirection: 'row' }}
                        onPress={() =>
                            Linking.openURL('mailto:' + brandInfo.email)
                        }>
                        <FIcon name="envelope-o" size={20} color={colors.primaryColor} />
                        <Text style={{ color: colors.primaryColor, marginLeft: 4, fontSize: 13 }}>{brandInfo.email}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ flexDirection: 'row' }}
                        onPress={() =>
                            Linking.openURL(brandInfo.url)
                        }>
                        <WebIcon name="web" size={20} color={colors.primaryColor} />
                        <Text style={{ color: colors.primaryColor, marginLeft: 4, fontSize: 13 }}>{brandInfo.url}</Text>
                    </TouchableOpacity>

                    <Text style={{ fontWeight: 'bold', fontSize: 14, marginBottom: 4, marginTop: 10 }}>Connect with us on:</Text>
                    <View style={{ flexDirection: 'row', width: '40%', justifyContent: 'space-evenly' }}>
                        <FIcon name="facebook-official" size={18} />
                        <FIcon name="instagram" size={18} />
                        <FIcon name="twitter" size={18} />

                    </View>

                </View>
            );

        }

        const renderBrandData = () => {
            return (
                <View>

                    <View style={styles.subTopBrandCont}>
                        <View style={styles.brandImgHolder}>
                            <FastImage
                                source={{
                                    uri: brandInfo.logo_src,
                                    priority: FastImage.priority.high,
                                    cache: FastImage.cacheControl.immutable,
                                }}
                                style={styles.brandImgCont2} />
                        </View>
                        <View>

                            <View style={{ marginTop: 20, marginLeft: 10 }}>

                                {renderFindMoreDetail()}
                                <View style={{ marginLeft: 10, marginTop: 10, flexDirection: 'row' }}>
                                    {renderFollowBtn()}
                                    {renderEnquiryBtn()}
                                </View>
                            </View>
                        </View>
                    </View>



                    <MediumText style={{ fontSize: 14, textAlign: 'justify', marginBottom: 10 }}>{brandInfo.description}</MediumText>

                    {this.state.startswiper === true && brandInfo.banners.length ? this._renderSwiper() : null}



                    <Text style={styles.headingTxt}>RECENT OFFERS</Text>
                </View>
            )
        }


        return (
            <View style={styles.topBrandCont}>
                <CustomHeader title='BRAND DETAIL' navigation={this.props.navigation} />
                {!brandDetailLoading && brandInfo ?
                    <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>

                        <View style={{ flex: 1, width: "100%", }}>

                            <BrandOfferList
                                brandOfferList={brandOfferList ? brandOfferList.data : null}
                                brandListLoading={brandListLoading}
                                navigation={this.props.navigation}
                                snippetTextShown={this.state.snippetTextShown}
                                toggleNumOfSnippetLines={this.toggleNumOfSnippetLines}
                                onClickPreference={this.onClickOfferPreference}
                                onClickFavOffer={this.onClickFavOfferHandler}
                                refreshList={refreshList}
                                renderBrandData={renderBrandData}
                                brandListEmpty={this.brandListEmpty}
                            />
                        </View>

                        <EnquiryMessageModal
                            visible={this.state.showEnquiryModal}
                            idToEnquiry={brandInfo.id}
                            fromScreen={'brand'}
                            close={this.closeEnquiryModal}
                            error={error}
                            success={success}
                        />
                    </View>
                    :
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <ActivityIndicator />
                    </View>
                }
            </View>
        );
    }
}

const mapStateToProps = state => {
    const { brandDetailLoading,
        brandInfo,
        brandOfferList,
        brandListLoading,
        followStatus,
        unFollowStatus,
        error,
        success,
        refreshList } = state.dashboard;
    return {
        brandDetailLoading,
        brandInfo,
        brandOfferList,
        brandListLoading,
        followStatus,
        unFollowStatus,
        error,
        success,
        refreshList
    };
}

export default BrandDetailScreen = connect(mapStateToProps, {
    showBrandDetail,
    showBrandOffer,
    followBrand,
    unFollowBrand,
    removeErrorSuccessMsg,
    updatelikePreference,
    updateDislikePreference,
    updateUnFavouritePref,
    updateFavouritePref,
    addlikePrefBrandOffers,
    removelikePrefBrandOffers,
    addFavPrefBrandOffers,
    removeFavPrefBrandOffers
})(BrandDetailScreen);