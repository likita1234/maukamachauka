import React, { Component, PureComponent } from 'react';
import { View, Text, ActivityIndicator, FlatList, TouchableOpacity, ImageBackground } from 'react-native';
import FastImage from 'react-native-fast-image';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import Moment from 'moment';
import BackIcon from 'react-native-vector-icons/Ionicons';
import {
    getNearbyAddressList,
    fetchCatOfferLists,
    updateDislikePreference,
    updatelikePreference,
    updateUnFavouritePref,
    updateFavouritePref,
    addlikePrefCatOffers,
    removelikePrefCatOffers,
    addFavPrefCatOffers,
    removeFavPrefCatOffers
} from '../../../actions';
import colors from '../../../config/colors';
import MediumText from '../../../components/presentational/typography/MediumText';
import { styles } from './DashboardStyles';
import SingleCategoryOffer from '../../../components/container/Dashboard/SingleCategoryOffer';
import SkeletonScreen from '../../../components/presentational/skeleton/SkeletonScreen';

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20
    return layoutMeasurement.height + contentOffset.y >=
        contentSize.height - paddingToBottom
};

class CategoryOfferListScreen extends PureComponent {
    state = {
        activeCatId: 1,
        catListRefreshing: false,
        pageNum: 1,
        showLoadMoreLoader: false,
        isLoading: false,
        snippetTextShown: -1,
        currLikedOff: [],
        currDislikedOff: [],
        currFavOff: [],
        currUnFavOff: [],
        noOfferMsg: null
    }

    componentDidMount() {
        console.log("componentDidMount in catOffer")
        const { pageNum } = this.state;
        const { navigation } = this.props
        const catId = navigation.getParam('catId', null);
        this.setState({ activeCatId: catId, isLoading: true }, () => {
            this.props.fetchCatOfferLists(pageNum, this.state.activeCatId, () => {
                this.setState({ isLoading: false, noOfferMsg: "No offer is available under this category" })
            });
        })
    };

    componentWillMount() {
        this.setState({ noOfferMsg: null });
    };


    goBack = () => {
        const { navigation } = this.props;
        let screenFrom = navigation.getParam('originScreen', null);
        //   console.log('screen', screenFrom);
        if (screenFrom) {
            navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    key: null,
                    actions: [
                        NavigationActions.navigate({
                            routeName: 'Dashboard',
                        }),
                    ],
                }),
            );
            navigation.navigate(screenFrom);
        } else {
            navigation.goBack(null);
        }
    };


    onRefreshCatListHandler = () => {
        this.setState({ catListRefreshing: true }, () => {
            this.props.fetchCatOfferLists(1, this.state.activeCatId, () => {
                this.setState({ catListRefreshing: false })
            });
        });
    };

    loadMoreCatListHandler = () => {
        const { catLastPage } = this.props;
        if (this.state.pageNum != catLastPage) {
            this.setState({ pageNum: this.state.pageNum + 1, showLoadMoreLoader: true }, () => {
                this.props.fetchCatOfferLists(
                    this.state.pageNum,
                    this.state.activeCatId,
                    () => {
                        // after offer load as on scroll
                        this.setState({ showLoadMoreLoader: false });
                    },
                );
            })
        }
    };

    renderLoadMoreLoader = () => {
        const { showLoadMoreLoader } = this.state;
        if (!showLoadMoreLoader) {
            return null;
        }
        return (
            <ActivityIndicator size="small" />
        );
    };

    toggleNumOfSnippetLines = index => {
        this.setState({
            snippetTextShown: this.state.snippetTextShown === index ? -1 : index,
        });
    };

    onClickOfferPreference = (preferenceVal, offerId) => {

        console.log("preferenceVal", preferenceVal)
        if (preferenceVal === 'alreadyliked' || preferenceVal === 'recentlyLiked') {
            this.props.removelikePrefCatOffers(offerId);
            this.props.updateDislikePreference(offerId);
        }
        if (preferenceVal === 'unliked' || preferenceVal === 'recentlyUnliked') {
            this.props.addlikePrefCatOffers(offerId)
            this.props.updatelikePreference(offerId);
        }


    };

    onClickFavOfferHandler = (FavPreferenceVal, offerId) => {
        if (FavPreferenceVal === 'alreadyFav' || FavPreferenceVal === 'recentlyFav') {
            this.props.removeFavPrefCatOffers(offerId);
            this.props.updateUnFavouritePref(offerId);
        };
        if (FavPreferenceVal === 'unFav' || FavPreferenceVal === 'recentlyUnFav') {
            this.props.addFavPrefCatOffers(offerId)
            this.props.updateFavouritePref(offerId);
        };
    };

    showBrandDetailHandler = brandId => {
        this.props.navigation.navigate('BrandDetail', { brandId: brandId });
    };


    CatOfferlistEmpty = () => {
        return (
            <View style={{ alignItems: 'center', flex: 1 }}>
                <MediumText>{this.state.noOfferMsg}</MediumText>
            </View>
        );
    }


    render() {
        const { catofferLists, catoffListLoading, navigation, refreshList } = this.props;
        const { catListRefreshing, isLoading, noOfferMsg } = this.state;
        const catName = navigation.getParam('catName', null);
        //      console.log("catofferLists", catofferLists);


        const renderHeader = () => {
            return (
                <View style={styles.offerHeader}>
                    <TouchableOpacity
                        onPress={() => this.goBack()}
                        style={{ width: 40, height: 38, alignItems: 'center', justifyContent: 'center' }}>
                        <BackIcon
                            name="md-arrow-back"
                            size={24}
                            style={{ marginLeft: 8 }}
                            color={colors.white} />
                    </TouchableOpacity>
                    <View style={{ width: 40, height: 38, alignItems: 'center', width: '90%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 14, marginLeft: 20, color: colors.white, textTransform: 'uppercase', marginTop: 6 }}>{catName}</Text>
                    </View>

                </View>
            )
        }


        const renderCategoryOfferList = () => {
            if (Array.isArray(catofferLists)) {

                if (isLoading) {
                    return (
                        <View style={{ paddingTop: 20 }}>
                            <SkeletonScreen />
                        </View>
                    );
                }
                return (
                    <View style={[styles.container, { paddingBottom: 40, marginLeft: 10, marginRight: 10, width: '100%' }]}>

                        <FlatList
                            data={catofferLists}
                            showVerticalScrollIndicator={false}
                            renderItem={({ item }) => (
                                <SingleCategoryOffer
                                    catoffer={item}
                                    navigation={this.props.navigation}
                                    snippetTextShown={this.state.snippetTextShown}
                                    toggleNumOfSnippetLines={this.toggleNumOfSnippetLines}
                                    onClickPreference={this.onClickOfferPreference}
                                    currLikedOff={this.state.currLikedOff}
                                    currDislikedOff={this.state.currDislikedOff}
                                    currFavOff={this.state.currFavOff}
                                    currUnFavOff={this.state.currUnFavOff}
                                    onClickFavOffer={this.onClickFavOfferHandler}
                                    showBrandDetail={this.showBrandDetailHandler}
                                />

                            )}

                            keyExtractor={(item, index) => (item.id.toString())}
                            onRefresh={() => this.onRefreshCatListHandler()}
                            refreshing={catListRefreshing}
                            onMomentumScrollEnd={({ nativeEvent }) => {

                                if (isCloseToBottom(nativeEvent)) {
                                    console.log('LOAD more --- ');
                                    this.loadMoreCatListHandler();
                                }
                            }}
                            onEndReachedThreshold={0}
                            ListFooterComponent={() => this.renderLoadMoreLoader()}
                            extraData={refreshList}
                            ListEmptyComponent={() => this.CatOfferlistEmpty()}
                        />
                    </View>
                )
            }

            else {
                return (
                    <View style={{ paddingTop: 20 }}>
                        <SkeletonScreen />
                    </View>
                );
            }
        }


        return (
            <View style={{ flex: 1, backgroundColor: colors.white }}>
                {renderHeader()}
                {renderCategoryOfferList()}
            </View>
        )

    }
}

const mapStateToProps = state => {
    const { catofferLists, catoffListLoading, catLastPage, refreshList } = state.dashboard;

    return {
        catofferLists,
        catoffListLoading,
        catLastPage,
        refreshList
    };
};
export default CategoryOfferListScreen = connect(mapStateToProps, {
    getNearbyAddressList,
    fetchCatOfferLists,
    updateDislikePreference,
    updatelikePreference,
    updateUnFavouritePref,
    updateFavouritePref,
    addlikePrefCatOffers,
    removelikePrefCatOffers,
    addFavPrefCatOffers,
    removeFavPrefCatOffers
})(CategoryOfferListScreen);