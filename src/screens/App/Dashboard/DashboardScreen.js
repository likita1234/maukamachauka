import React, { PureComponent } from 'react';
import { View, ScrollView, RefreshControl, ActivityIndicator, TextInput, Text, TouchableOpacity, TouchableNativeFeedback, Keyboard } from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';
import IconOct from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Close from 'react-native-vector-icons/MaterialIcons'
import { connect } from 'react-redux';
import {
    logOut,
    fetchUserInfo,
    fetchUserOffers,
    updatelikePreference,
    updateDislikePreference,
    updateUnFavouritePref,
    updateFavouritePref,
    fetchCatOfferLists,
    searchOffer,
    addlikePrefOffers,
    removelikePrefOffers,
    addFavPrefOffers,
    removeFavPrefOffers,
    showUserFavOfferList
} from '../../../actions';
import OfferList from '../../../components/container/Dashboard/OfferList';
import DashboardHeader from '../../../components/container/Dashboard/DashboardHeader';
import CategoryList from '../../../components/container/Dashboard/CategoryLists';
import NoInternetNotice from '../../../components/presentational/notice/NoInternetNotice';
import LargeText from '../../../components/presentational/typography/LargeText';
import MediumText from '../../../components/presentational/typography/MediumText';
import colors from '../../../config/colors';
import { styles } from './DashboardStyles';


class DashboardScreen extends PureComponent {

    state = {
        offListRefreshing: false,
        showLoadMoreLoader: false,
        page: 1,
        showSearchForm: false,
        searchText: '',
        snippetTextShown: -1, // to show snippet text in list
        offerData: [],
        catPage: 1,
        isLoading: false,
        showUpdateInfo: true

    };

    componentDidMount = () => {
        this.props.fetchUserInfo();
        this.props.fetchUserOffers(this.state.page);
        this.props.showUserFavOfferList();
    };

    onretryNetConnectionHandler = () => {
        this.setState({ searchText: '', page: 1 }, () => {
            this.props.fetchUserInfo();
            this.props.fetchUserOffers(this.state.page);
            this.props.showUserFavOfferList();
        });
    }

    onRefreshOffListHandler = () => {
        console.log("onRefreshOffListHandler")
        this.setState({ offListRefreshing: true }, () => {
            this.props.fetchUserOffers(1, () => {
                this.setState({ offListRefreshing: false })
            });
        });
    };


    loadMoreOffersHandler = () => {
        const { offerListLoading, lastPage } = this.props;
        console.log("lastPage", lastPage);

        if (this.state.page != lastPage) {
            this.setState({ page: this.state.page + 1, showLoadMoreLoader: true }, () => {

                console.log('Load more start page -', this.state.page);
                //setTimeout(() => {
                this.props.fetchUserOffers(
                    this.state.page,
                    () => {
                        // after offer load as on scroll
                        this.setState({ showLoadMoreLoader: false });
                    },
                );
            }
            );
        }
        //else {
        //     console.log("In progress")
        // }


    };

    showDetailScreenHandler = offerDetailInfo => {
        this.props.navigation.navigate('OfferDetail', { offerDetailInfo: offerDetailInfo.offer });
    };

    toggleSearchForm = () => {
        this.setState({ showSearchForm: !this.state.showSearchForm });
    };

    onSearchTextChangeHandler = searchText => {
        this.setState({ searchText: searchText });
    };

    onResetSearchFormHandler = () => {
        if (this.state.searchText) {
            this.setState({ searchText: '' }, () => {
                Keyboard.dismiss();
                this.props.fetchUserOffers(this.state.page);
            });
        }
    };

    toggleNumOfSnippetLines = index => {
        this.setState({
            snippetTextShown: this.state.snippetTextShown === index ? -1 : index,
        });
    };

    onClickOfferPreference = (preferenceVal, offerId) => {
        if (preferenceVal === 'alreadyliked') {
            //  console.log('Dis Liked offer');
            //calling props to change liked offer into dislike
            this.props.removelikePrefOffers(offerId);
            this.props.updateDislikePreference(offerId);
        }
        //calling props to change disliked offer into like
        if (preferenceVal === 'unliked') {
            this.props.addlikePrefOffers(offerId);
            this.props.updatelikePreference(offerId);
        }
    };

    onClickFavOfferHandler = (FavPreferenceVal, offerId) => {
        //    console.log("FavPreferenceVal", FavPreferenceVal)
        if (FavPreferenceVal === 'alreadyFav') {
            //  console.log('UnFavourite offer');
            //calling props to change Favourite offer into UnFavourite
            this.props.removeFavPrefOffers(offerId);
            this.props.updateUnFavouritePref(offerId);
        }
        //calling props to change UnFavourite offer into Favourite
        if (FavPreferenceVal === 'unFav') {
            console.log('Favourite offer');
            this.props.addFavPrefOffers(offerId);
            this.props.updateFavouritePref(offerId);
        }

    };

    showBrandDetailHandler = brandId => {
        this.props.navigation.navigate('BrandDetail', { brandId: brandId });
    };

    renderLoadMoreLoader = () => {
        const { showLoadMoreLoader } = this.state;

        if (!showLoadMoreLoader) {
            return null;
        }
        console.log("renderLoadMoreLoader", showLoadMoreLoader);
        return (
            <ActivityIndicator size="small" />
        );
    };

    offerListEmptyMsg = () => {
        return (
            <View style={{ alignItems: 'center', flex: 1 }}>
                <MediumText>No offer found</MediumText>
            </View>
        );
    };

    fetchCatOfferListHandler = (categoryId) => {
        this.setState({ activeCatId: categoryId, catPage: 1, isLoading: true }, () => {
            const { activeCatId, page } = this.state;
            this.props.fetchCatOfferLists(page, activeCatId, () => {
                this.setState({ isLoading: false })

            });
        });
    }

    filterBySearchTextHandler = () => {
        if (this.state.searchText) {
            this.props.searchOffer(this.state.searchText);
        } else {
            console.log('No search text');
        }

    }

    render() {
        const { noNetConnection, offers, error, offerListLoading, user, navigation, refreshList, categoryList } = this.props;



        const renderNoNetInfo = () => {
            if (noNetConnection)
                return (
                    <View>
                        <NoInternetNotice
                            onretryNetConnection={this.onretryNetConnectionHandler}
                        />

                    </View>
                );
        }
        const renderUpdateProfileInfo = () => {
            const { showUpdateInfo } = this.state;
            if (user && (!user.phone || !user.details) && showUpdateInfo) {
                return (
                    <View style={{ flexDirection: 'row', backgroundColor: colors.accentColor, height: 30, width: '100%', justifyContent: 'space-between' }}>
                        <Text style={{ color: colors.white, marginLeft: 10, textAlign: 'center', marginTop: 4 }}>Please update your profile</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("UpdateProfile")}
                                style={{ borderColor: colors.white, borderWidth: 1, margin: 4, width: 60 }}>
                                <Text style={{ color: colors.white, textAlign: 'center' }}> Ok</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ showUpdateInfo: false })}
                                style={{ borderColor: colors.white, borderWidth: 1, margin: 4, width: 60 }}>
                                <Text style={{ color: colors.white, textAlign: 'center' }}> Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View >
                )
            }
        }


        return (
            <ScrollView
                style={styles.container}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.offListRefreshing}
                        onRefresh={() => this.onRefreshOffListHandler()} />
                }
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps="always"
            >

                <DashboardHeader
                    user={user}
                    navigation={navigation}
                    searchText={this.state.searchText}
                    onSearchTextChange={this.onSearchTextChangeHandler}
                    filterBySearchText={this.filterBySearchTextHandler}
                    onResetSearchForm={this.onResetSearchFormHandler} />

                {renderNoNetInfo()}
                {renderUpdateProfileInfo()}
                <CategoryList
                    categoriesList={categoryList}
                    fetchCategoryOffers={this.fetchCatOfferListHandler}
                    navigation={this.props.navigation}
                />
                <View style={styles.offerList}>
                    <OfferList
                        offers={offers}
                        // error={error}
                        //  renderHeaderAndCatList={renderHeaderAndCatList}
                        offListRefreshing={this.state.offListRefreshing}
                        onRefreshOffList={this.onRefreshOffListHandler}
                        offerListLoading={offerListLoading}
                        showOfferDetail={this.showDetailScreenHandler}
                        snippetTextShown={this.state.snippetTextShown}
                        toggleNumOfSnippetLines={this.toggleNumOfSnippetLines}
                        onClickPreference={this.onClickOfferPreference}
                        showBrandDetail={this.showBrandDetailHandler}
                        onClickFavOffer={this.onClickFavOfferHandler}
                        loadMoreOffers={this.loadMoreOffersHandler}
                        renderLoadMoreLoader={this.renderLoadMoreLoader}
                        offerListEmpty={this.offerListEmptyMsg}
                        refreshList={refreshList}
                    />

                </View>
            </ScrollView>
        );
    }
};

const mapStateToProps = state => {
    const { noNetConnection } = state.ui;
    const { offers, error, offerListLoading, lastPage, refreshList, categoryList, } = state.dashboard;
    const { user } = state.profile;
    return { noNetConnection, offers, user, error, offerListLoading, lastPage, refreshList, categoryList };

}

export default DashboardScreen = connect(mapStateToProps, {
    logOut,
    fetchUserInfo,
    fetchUserOffers,
    updatelikePreference,
    updateDislikePreference,
    updateUnFavouritePref,
    updateFavouritePref,
    fetchCatOfferLists,
    searchOffer,
    addlikePrefOffers,
    removelikePrefOffers,
    addFavPrefOffers,
    removeFavPrefOffers,
    showUserFavOfferList
})(DashboardScreen);