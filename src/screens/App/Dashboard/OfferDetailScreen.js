import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, Share, ActivityIndicator, } from 'react-native';
import FastImage from 'react-native-fast-image';
import MapView, { Marker } from 'react-native-maps';
import { NavigationActions, StackActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import IONIcon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import {
    removeErrorSuccessMsg,
    updatelikePreference,
    updateDislikePreference,
    fetchUserOffers,
    updateFavouritePref,
    updateUnFavouritePref,
    addlikePrefOffers,
    removelikePrefOffers,
    addFavPrefOffers,
    removeFavPrefOffers,
    addlikePrefBrandOffers,
    removelikePrefBrandOffers,
    addFavPrefBrandOffers,
    removeFavPrefBrandOffers,
    addlikePrefCatOffers,
    removelikePrefCatOffers,
    addFavPrefCatOffers,
    removeFavPrefCatOffers,
    showSubscribeOfferList,
    showUnSubscribeOfferList,
    unSubscribedOffer,
    subscribedOffer
} from '../../../actions';
import CustomHeader from '../../../components/container/Header/CustomHeader';
import EnquiryMessageModal from '../../../components/container/Modal/EnquiryMessageModal';
import colors from '../../../config/colors';
import CustomText from '../../../config/text';
import LargeText from '../../../components/presentational/typography/LargeText';
import MediumText from '../../../components/presentational/typography/MediumText';
import { styles } from '../../../components/container/Dashboard/DashboardStyles';

class OfferDetailScreen extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {

            region: null,
            latitude: null,
            longitude: null,
            latitudeDelta: null,
            longitudeDelta: null,
            showEnquiryModal: false,
            liked: false,
            disliked: false,
            fav: false,
            unfav: false,
            changeFavColor: false,
            changeColor: false,
            likeCount: null,
            showLoading: false

        }
    };

    componentDidMount() {
        this.props.showSubscribeOfferList();
        this.props.showUnSubscribeOfferList();
        const { navigation } = this.props;
        const offerDetail = navigation.getParam('offerDetailInfo', null);
        let locationLists = offerDetail.location ? offerDetail.location : null;
        this.setState({ likeCount: offerDetail.likes_count }, () => {
            if (locationLists) {
                return locationLists.map((item, index) => {
                    this.setState({
                        region: {
                            latitude: item.lat,
                            longitude: item.long,
                            latitudeDelta: 0.009,
                            longitudeDelta: 0.009,
                        },

                    })
                });
            }
        });
    };

    goBack = () => {
        const { navigation } = this.props;
        let screenFrom = navigation.getParam('originScreen', null);
        if (screenFrom) {
            navigation.dispatch(
                StackActions.reset({
                    index: 0,
                    key: null,
                    actions: [
                        NavigationActions.navigate({
                            routeName: 'Dashboard',
                        }),
                    ],
                }),
            );
            navigation.navigate(screenFrom);
        } else {
            navigation.goBack(null);
        }
    };

    showOfferBrandDetail = brandId => {
        this.props.navigation.navigate('BrandDetail', { brandId: brandId });
    };

    openEnquiryModal = () => {
        this.setState({ showEnquiryModal: true });
    }

    closeEnquiryModal = () => {
        this.setState({ showEnquiryModal: false }, () => {
            this.props.removeErrorSuccessMsg();
        }
        );
    }

    onClickPreference = (preferenceVal, offerId) => {
        const { likeCount } = this.state;
        const { navigation } = this.props;
        let screenFrom = navigation.getParam('originScreen', null);
        console.log("screenFrom", screenFrom);

        let currLikeCount = null;
        if (preferenceVal === 'unliked' || preferenceVal === 'recentlyUnliked') {
            currLikeCount = likeCount >= 1 ? likeCount + 1 : 1
            this.setState({ changeColor: true, liked: true, disliked: false, likeCount: currLikeCount }, () => {
                this.props.updatelikePreference(offerId, () => {
                    if (screenFrom === null) {
                        this.props.addlikePrefOffers(offerId);
                    } else if (screenFrom === 'brand') {
                        this.props.addlikePrefBrandOffers(offerId);
                    } else {
                        this.props.addlikePrefCatOffers(offerId);
                    }
                });
            });

        }


        if (preferenceVal === 'alreadyLiked' || preferenceVal === 'recentlyLiked') {
            currLikeCount = likeCount > 1 ? likeCount - 1 : null
            this.setState({
                changeColor: true, disliked: true, liked: false,
                likeCount: currLikeCount
            }, () => {
                this.props.updateDislikePreference(offerId, () => {
                    if (screenFrom === null) {
                        this.props.removelikePrefOffers(offerId);
                    } else if (screenFrom === 'brand') {
                        this.props.removelikePrefBrandOffers(offerId);
                    } else {
                        this.props.removelikePrefCatOffers(offerId);
                    }

                });
            });
        }
    };

    onClickFavOffer = (preferenceVal, offerId) => {
        const { navigation } = this.props;
        let screenFrom = navigation.getParam('originScreen', null);
        if (preferenceVal === 'unFav' || preferenceVal === 'recentlyUnFav') {
            this.setState({ changeFavColor: true, fav: true, unfav: false }, () => {
                this.props.updateFavouritePref(offerId, () => {
                    if (screenFrom === null) {
                        this.props.addFavPrefOffers(offerId);
                    } else if (screenFrom === 'brand') {
                        this.props.addFavPrefBrandOffers(offerId);
                    } else {
                        this.props.addFavPrefCatOffers(offerId);
                    }
                });
            });

        }

        else {
            this.setState({
                changeFavColor: true, unfav: true, fav: false,
            }, () => {
                this.props.updateUnFavouritePref(offerId, () => {
                    if (screenFrom === null) {
                        this.props.removeFavPrefOffers(offerId);
                    } else if (screenFrom === 'brand') {
                        this.props.removeFavPrefBrandOffers(offerId);
                    } else {
                        this.props.removeFavPrefBrandOffers(offerId);
                    }
                });
            });
        }
    };


    unSubscribeOfferHandler = idToUnSubscribe => {
        this.setState({ showLoading: true }, () => {

            this.props.unSubscribedOffer(idToUnSubscribe, () => {

                this.props.showSubscribeOfferList(() => {
                    this.setState({ showLoading: false });
                });
                this.props.showUnSubscribeOfferList();
            });
        });

    }

    subscribeOfferHandler = idToSubscribe => {
        this.setState({ showLoading: true }, () => {
            this.props.subscribedOffer(idToSubscribe, () => {
                this.props.showSubscribeOfferList(() => {
                    this.setState({ showLoading: false });
                });
            });
        });
    }

    render() {
        const { navigation, error, success, subscribedOffers } = this.props;
        const { liked, disliked, changeColor, changeFavColor, fav, unfav, showLoading } = this.state;
        const offerDetail = navigation.getParam('offerDetailInfo', null);
        //console.log("offerDetail", offerDetail);
        let locationLists = offerDetail.location;
        let catList = offerDetail.categories;

        const renderHeader = () => {
            return (
                <View style={styles.offerHeader}>
                    <TouchableOpacity
                        onPress={() => this.goBack()}>
                        <IONIcon
                            name="md-arrow-back"
                            size={24}
                            style={{ marginLeft: 8 }}
                            color={colors.white} />
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center', width: '90%' }}>
                        <Text style={{ textAlign: 'center', fontSize: 14, marginLeft: 20, color: colors.white }}>OFFER DETAIL</Text>
                    </View>

                </View>
            )
        };

        const offerBrandTitle = () => {
            return (
                <View>
                    <LargeText style={styles.offerTitle}>{offerDetail.brand.name}</LargeText>
                </View>
            );
        };

        const renderCategoryList = () => {
            if (Array.isArray(catList) && catList.length > 0) {
                let catImage = catList.map((item, index) => {
                    return (
                        <View style={styles.tagContainer} key={item.id}>

                            {subscribedOffers && subscribedOffers.some(el => el.id === item.id) ?
                                <TouchableOpacity onPress={() => this.unSubscribeOfferHandler(item.id)}
                                    disabled={showLoading ? true : false}
                                >
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', width: '100%' }}>
                                        <Text style={{ textAlign: 'center', fontSize: 12, marginBottom: 4, color: colors.primaryColor, fontWeight: 'bold', }} >
                                            {item.name}
                                        </Text>
                                        {showLoading ?
                                            <ActivityIndicator /> :

                                            <FAIcon name="check-circle" size={18} color={colors.accentColor} />
                                        }
                                    </View>
                                </TouchableOpacity> :
                                <TouchableOpacity onPress={() => this.subscribeOfferHandler(item.id)}
                                    disabled={showLoading ? true : false}
                                    style={{ flexDirection: 'row' }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', width: '100%' }}>
                                        <Text style={{ textAlign: 'center', fontSize: 12, marginBottom: 4, color: colors.primaryColor, fontWeight: 'bold', }} >
                                            {item.name}
                                        </Text>
                                        {showLoading ?
                                            <ActivityIndicator /> :
                                            <IONIcon name="ios-close-circle" size={18} color={colors.primaryColor} />
                                        }
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>

                    );

                });
                return catImage;

            }
        };

        const renderMarker = () => {
            if (locationLists) {


                let locationCoordinate = locationLists.map((item, index) => {
                    return (
                        < MapView.Marker key={index}
                            coordinate={{
                                latitude: item.lat,
                                longitude: item.long
                            }}
                            pinColor={'red'}
                            title={item.name}
                        />

                    )
                });
                return locationCoordinate;
            }
        };

        const renderEnquiryBtn = () => {
            return (
                <TouchableOpacity style={styles.enquiryBtn}
                    onPress={() =>
                        // this.props.navigation.navigate("enquiry", { idToEnquiry: offerDetail.id, fromScreen: 'offerDetail' });
                        this.openEnquiryModal()
                    }>
                    <Icon name="message" size={12} color={colors.white} />
                    <Text style={styles.enquiryBtnTxt}>
                        Send Enquiry</Text>
                </TouchableOpacity>
            );

        };

        const renderLikecount = () => {
            const { likeCount } = this.state;
            if (likeCount > 0) {
                if (likeCount === 1) {
                    return <CustomText style={{ fontSize: 10, color: colors.white }}>{likeCount} like</CustomText>
                }
                return <CustomText style={{ fontSize: 10, color: colors.white }}>{likeCount} likes</CustomText>
            }
            return null;
        }

        const likePreferenceButton = () => {
            let color = colors.black;
            let bgColor;
            let prefvalOnClick = 'unliked'

            if (changeColor) {
                if (liked) {
                    color = colors.white;
                    bgColor = colors.primaryColor
                    prefvalOnClick = 'recentlyLiked';
                }
                else {
                    color = colors.primaryColor;
                    bgColor = colors.white;
                    prefvalOnClick = 'recentlyUnliked';

                }
            } else {
                color = offerDetail.liked_status === true ? colors.white : colors.primaryColor;
                bgColor = offerDetail.liked_status === true ? colors.primaryColor : colors.white;
                prefvalOnClick = offerDetail.liked_status === true ? 'alreadyLiked' : 'unliked';

            }


            return (
                <TouchableOpacity
                    onPress={() => {
                        this.onClickPreference(prefvalOnClick, offerDetail.id);
                    }}
                    style={styles.opinionIcons}>
                    <View style={{
                        alignItems: 'center',
                        backgroundColor: bgColor,
                        height: 45,
                        width: 45,
                        borderWidth: 1,
                        borderColor: colors.primaryColor,
                        borderRadius: 100,
                        justifyContent: 'center',
                    }}>

                        <FAIcon name="thumbs-up" size={22} solid color={color} />
                        {/* {renderLikecount()} */}
                    </View>

                </TouchableOpacity>
            );
        };

        const addToFavouriteBtn = () => {
            let color = colors.black;
            let bgColor;
            let prefvalOnClick = 'unFav'
            if (changeFavColor) {
                if (fav) {
                    color = colors.white;
                    bgColor = colors.primaryColor
                    prefvalOnClick = 'recentlyFav';
                }
                else {
                    color = colors.primaryColor;
                    bgColor = colors.white
                    prefvalOnClick = 'recentlyUnFav';
                }
            }
            else {
                color = offerDetail.favorite_status === true ? colors.white : colors.primaryColor;
                bgColor = offerDetail.favorite_status === true ? colors.primaryColor : colors.white;
                prefvalOnClick = offerDetail.favorite_status === true ? 'alreadyFav' : 'unFav';
            }

            return (
                <TouchableOpacity
                    onPress={() => this.onClickFavOffer(prefvalOnClick, offerDetail.id)}
                    style={styles.opinionIcons}>
                    <View style={{
                        alignItems: 'center',
                        backgroundColor: bgColor,
                        height: 45,
                        width: 45,
                        borderWidth: 1,
                        borderColor: colors.primaryColor,
                        borderRadius: 100,
                        justifyContent: 'center',
                    }}>
                        <FAIcon name="heart" size={22} color={color} />
                    </View>

                </TouchableOpacity>
            );
        };

        const shareOfferMsg = () => {
            let msg =
                'Hey, check out this offer, see if you like it :-) Offer:\n ' +
                offerDetail.title

            Share.share({
                message: msg,
            });
        };

        const shareButton = () => {
            return (
                <TouchableOpacity
                    onPress={() => shareOfferMsg()}
                    style={{ marginTop: 10 }}
                    style={styles.opinionIcons}>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            backgroundColor: colors.white,
                            height: 45,
                            width: 45,
                            borderWidth: 1,
                            borderColor: colors.primaryColor,
                            borderRadius: 100,
                            justifyContent: 'center',
                        }}>
                        <FAIcon name="share-alt" size={22} color={colors.primaryColor} />
                    </View>
                </TouchableOpacity>
            );
        };

        return (
            <ScrollView style={{ flex: 1, backgroundColor: colors.white }}>
                <CustomHeader title='OFFER DETAIL' navigation={this.props.navigation} />
                <View style={{ flex: 1, margin: 10, marginTop: 20 }}>
                    <View style={[styles.offerBrandDetail, { marginBottom: 10, justifyContent: 'space-between' }]}>
                        <TouchableOpacity
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}
                            onPress={() => this.showOfferBrandDetail(offerDetail.brand.id)}>
                            <View style={styles.brandImgholder}>
                                <FastImage
                                    source={{
                                        uri: offerDetail.brand.logo_src,
                                        priority: FastImage.priority.high,
                                        cache: FastImage.cacheControl.immutable,
                                    }}
                                    style={styles.brandImgCont}
                                />
                            </View>
                            {offerBrandTitle()}
                        </TouchableOpacity>

                    </View>

                    {renderEnquiryBtn()}

                    <View style={styles.offerBox}>
                        <View style={styles.offerImgHolder}>

                            <FastImage
                                source={{
                                    uri: offerDetail.image_src,
                                    priority: FastImage.priority.high,
                                }}
                                style={styles.imgCont}
                            />

                        </View>

                        <View style={styles.offerDetails}>

                            <View style={styles.opinionStyle}>
                                <View style={{ width: '100%' }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <LargeText style={styles.brandTxt} bold={true}>
                                            {offerDetail.title}
                                        </LargeText>
                                    </View>
                                </View>
                            </View>
                            <MediumText style={styles.descTxt}>{offerDetail.description}</MediumText>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                width: '100%',
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: -25,
                            }}>
                            {likePreferenceButton()}
                            {addToFavouriteBtn()}
                            {shareButton()}
                        </View>

                        <View style={{ width: '100%', marginTop: 20, marginBottom: 20 }}>
                            <LargeText style={styles.offerTitle}>Tags</LargeText>
                            {renderCategoryList()}
                        </View>

                        {offerDetail.location &&
                            <View style={styles.mapViewHolder} >
                                <MapView
                                    region={this.state.region}
                                    style={styles.mapContainer}
                                    zoomEnabled={true}
                                    showsUserLocation={true}>
                                    {renderMarker()}
                                </MapView>
                            </View>
                        }

                        <EnquiryMessageModal
                            visible={this.state.showEnquiryModal}
                            idToEnquiry={offerDetail.id}
                            fromScreen={'offerDetail'}
                            close={this.closeEnquiryModal}
                            error={error}
                            success={success}
                        />
                    </View>
                </View >
            </ScrollView>

        );
    }
}

const mapStateToProps = state => {
    const { error, success } = state.dashboard;
    const { subscribedOffers } = state.profile;
    return { error, success, subscribedOffers };
}

export default OfferDetailScreen = connect(mapStateToProps, {
    removeErrorSuccessMsg,
    updatelikePreference,
    updateDislikePreference,
    fetchUserOffers,
    updateFavouritePref,
    updateUnFavouritePref,
    addlikePrefOffers,
    removelikePrefOffers,
    addFavPrefOffers,
    removeFavPrefOffers,
    addlikePrefBrandOffers,
    removelikePrefBrandOffers,
    addFavPrefBrandOffers,
    removeFavPrefBrandOffers,
    addlikePrefCatOffers,
    removelikePrefCatOffers,
    addFavPrefCatOffers,
    removeFavPrefCatOffers,
    showSubscribeOfferList,
    showUnSubscribeOfferList,
    unSubscribedOffer,
    subscribedOffer
})(OfferDetailScreen);
