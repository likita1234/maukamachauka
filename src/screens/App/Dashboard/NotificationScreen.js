import React, { Component } from 'react';
import { View } from 'react-native';
import CustomHeader from '../../../components/container/Header/CustomHeader';

class NotificationScreen extends Component {
    state = {}
    render() {
        return (
            <View>
                <CustomHeader title='NOTIFICATION' navigation={this.props.navigation} />
            </View>
        );
    }
}

export default NotificationScreen;