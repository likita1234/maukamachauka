import { StyleSheet } from 'react-native';
import colors from '../../../config/colors';

export const styles = StyleSheet.create({
    mainContainer: {
        //alignItems: 'center',
        //justifyContent: 'center'
        justifyContent: 'space-between'
    },
    topCont: {
        marginTop: 10,
        flex: 1,
    },
    subCont: {
        flex: 1,
        alignItems: 'center',
        marginTop: 10,
    },
    btmCont: {
        flex: 1,
        marginTop: 10,
        width: "90%",
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
    },
    subBtmCont: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        // marginTop: 8
    },
    headingTxt: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10
    },
    btnStyle: {
        width: 200,
        height: 36,
        backgroundColor: colors.primaryColor,
        marginTop: 8,
        borderRadius: 4
    },
    txtStyle: {
        fontWeight: 'bold',
        marginTop: 4,
        textAlign: 'center'
    },
    mapViewHolder: {
        marginTop: 20,
        width: '100%',
        minHeight: '100%',
        // zIndex: -1,
        //position: 'relative',
        //backgroundColor: "red"
    },
    mapContainer: {
        height: '100%',
        width: '100%',
    },
    textInput: {
        height: 40,
        width: 300,
        borderWidth: 1,
        paddingHorizontal: 16,
    },
    screenContainer: {
        backgroundColor: '#fff',
        height: '100%',
        overflow: 'hidden',
        paddingBottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    emailInput: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    divider: {
        marginBottom: 10,
        marginTop: 20,
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
    },
    dividerLine: {
        flex: 1,
        width: '50%',
        height: 2,
        top: '50%',
        backgroundColor: colors.lightgray,
        position: 'absolute'
    },
    circleBg: {
        flex: 1,
        backgroundColor: colors.primaryColor,
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50
    },
    dividerTxt: {
        textTransform: 'uppercase',
        color: 'white'
    }
});