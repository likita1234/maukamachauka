import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import DefaultInput from '../../../config/inputs';
import { showErrToast } from '../../../config/functions';
import ForgetPswEmailValidate from '../../../utility/validation/ForgetPswEmailValidate';
import { connect } from 'react-redux';
import { resetForgetPassword } from '../../../actions';
import CustomHeader from '../../../components/container/Header/CustomHeader'
import { styles } from './SocialLoginStyles';

class ForgetPasswordScreen extends Component {
    state = {
        emailAddress: '',
    };

    setEmail = (email) => {
        this.setState({ emailAddress: email });
    };


    render() {
        let UserEmail = this.state.emailAddress;
        const sentEmailLink = () => {
            let validate = ForgetPswEmailValidate(UserEmail);
            let validError = Object.keys(validate).length;
            if (parseInt(validError)) {
                console.log(validate);
                let errMsg = validate[Object.keys(validate)[0]];
                showErrToast.showToast(errMsg);
            } else {
                this.props.resetForgetPassword(UserEmail);
            }
        };


        return (
            <View style={{
                alignItems: 'center',
                flex: 1
            }}>
                <CustomHeader title='FORGET PASSWORD' navigation={this.props.navigation} />

                <View style={{ alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                    <Text >Forgot your password? </Text>
                    <Text>No problem, we'll send you a link to reset it.</Text>
                </View>
                <View style={{ width: '80%' }}>
                    <DefaultInput
                        style={styles.emailInput}
                        placeholder={'Email Address'}
                        onChangeText={email => this.setEmail(email)}
                        // autoFocus={true}
                        blurOnSubmit={false}
                    />
                </View>
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        style={[styles.btnStyle, { width: 140 }]}
                        onPress={() => sentEmailLink()}>
                        <Text style={[styles.txtStyle, { color: "#fff" }]}>Send a Link</Text>
                    </TouchableOpacity>

                </View>
            </View >

        );
    };
}

export default ForgetPasswordScreen = connect(null, {
    resetForgetPassword,
})(ForgetPasswordScreen);