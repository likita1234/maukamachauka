import React, { Component } from 'react';
import { ScrollView, View, Text, Keyboard, Animated, Dimensions, TextInput, UIManager, TouchableOpacity, Alert, ProgressBarAndroid, Platform, Image } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { LoginButton, AccessToken, LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { iosClientID, androidClientID } from '../../../../app.json';
import SignInByEmailForm from '../../../components/container/SocialLogin/SignInByEmailForm';
import SignUpByEmailForm from '../../../components/container/SocialLogin/SignUpByEmailForm';
import CustomText from '../../../config/text';
import { connect } from 'react-redux';
import {
    onSocialLogin,
    registerUserByEmail,
    loginUserByEmail,
    resetErrorSuccessMsg,
    sendEmailVerification
} from '../../../actions';
import { SvgIcon, Google } from '../../../components/container/Common/SvgIcons';
import AppLogo from '../../../assets/img/appLogo.png';
import { showErrToast, showSuccessToast } from '../../../config/functions';
import { styles } from './SocialLoginStyles';
import colors from '../../../config/colors.js';

const _responseInfoCallback = (error, result) => {
    if (error) {
        Alert.alert('Error fetching data: ' + error.toString());
    } else {
        Alert.alert('Welcome to MaukamaChauka offer ' + result.name, result.email);
    }
};

class SocialLogin extends Component {
    state = {
        showSignInForm: true,
        showSignUpForm: false,
        fname: '',
        lname: '',
        email: '',
        password: '',
        shift: new Animated.Value(0),
        showLoader: false,
        isSigninInProgress: false,


    }

    componentDidMount() {
        this.configureGoogleSignIn();
        this.keyboardDidShowSub = Keyboard.addListener(
            'keyboardDidShow',
            this.handleKeyboardDidShow,
        );
        this.keyboardDidHideSub = Keyboard.addListener(
            'keyboardDidHide',
            this.handleKeyboardDidHide,
        );
    }
    componentWillUnmount() {
        this.keyboardDidShowSub.remove();
        this.keyboardDidHideSub.remove();
    }

    handleKeyboardDidShow = event => {
        const { height: windowHeight } = Dimensions.get('window');
        const keyboardHeight = event.endCoordinates.height;
        const currentlyFocusedField = TextInput.State.currentlyFocusedField();
        UIManager.measure(
            currentlyFocusedField,
            (originX, originY, width, height, pageX, pageY) => {
                const fieldHeight = height;
                const fieldTop = pageY;
                const gap = windowHeight - keyboardHeight - (fieldTop + fieldHeight) - 30;
                if (gap >= 0) { return; }
                Animated.timing(this.state.shift, {
                    toValue: gap,
                    duration: 300,
                    useNativeDriver: true,
                }).start();
            },
        );
    };

    handleKeyboardDidHide = () => {
        Animated.timing(this.state.shift, {
            toValue: 0,
            duration: 300,
            useNativeDriver: true,
        }).start();
    };

    configureGoogleSignIn() {
        const androidClientId = androidClientID;
        GoogleSignin.configure({
            iosClientId: iosClientID,
            webClientId: androidClientId,  //Replace with your own client id 
            offlineAccess: false,
        });
    }

    onFnameChange = fname => {
        this.setState({ fname });
    };

    onLnameChange = lname => {
        this.setState({ lname });
    };

    onEmailChange = email => {
        this.setState({ email });
    };

    onPasswordChange = password => {
        this.setState({ password });
    };


    signUpUserHandler = (fname, lname, email, pwd) => {
        const { registerUserByEmail } = this.props;

        let formData = {
            fname: fname,
            lname: lname,
            email: email,
            password: pwd,
            action: 'signup',
        };
        registerUserByEmail(formData, () => {
            this.hideShowFormsHandler(true, false);
        });
    }
    signInUserHandler = (email, pwd) => {
        const { loginUserByEmail } = this.props;

        let formData = {
            email: email,
            password: pwd,
            action: 'signin',
        };
        loginUserByEmail(formData);
    };

    hideShowFormsHandler = (showsIn, showsUp) => {
        this.setState({ showSignInForm: showsIn, showSignUpForm: showsUp });
    }

    googleSignIn = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            await GoogleSignin.revokeAccess();
            //   console.log('Success:', userInfo);
            if (userInfo) {
                this.setState({ showLoader: true }, () => {
                    this.props.onSocialLogin(userInfo.idToken, 'google')
                })
            }

        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // sign in was cancelled
                //Alert.alert('Are you sure you want to cancel gooogle signIn');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation in progress already
                Alert.alert('in progress');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                Alert.alert('play services not available or outdated');
            } else {
                console.log('Something went wrong:', error.toString());
                Alert.alert('Something went wrong', error.toString());
                this.setState({
                    error,
                });
            }
        }
    };


    facebookSignIn = async () => {
        const { onSocialLogin } = this.props;
        // native_only config will fail in the case that the user has
        // not installed in his device the Facebook app. In this case we
        // need to go for webview.
        let result, FBaccessToken;
        const facebookSignInhandler = (FBaccessToken, serviceType) => {
            this.setState({ showLoader: true }, () => {
                this.props.onSocialLogin(FBaccessToken, serviceType)
            })

        }
        try {
            //    if (Platform.OS === 'android') LoginManager.setLoginBehavior("NATIVE_ONLY");
            result = await LoginManager.logInWithPermissions(["public_profile", "email"]);
            FBaccessToken = await AccessToken.getCurrentAccessToken();

        } catch (nativeError) {
            console.log("nativeError", nativeError);
            try {
                //    if (Platform.OS === 'android') LoginManager.setLoginBehavior("WEB_ONLY");
                result = await LoginManager.logInWithPermissions(["public_profile", "email"]);
                FBaccessToken = await AccessToken.getCurrentAccessToken();

            } catch (webError) {
                console.log("Web error", webError);
            }
        }
        if (FBaccessToken && FBaccessToken.accessToken && !result.isCancelled) {
            facebookSignInhandler(FBaccessToken.accessToken, 'facebook')
            //onSocialLogin(FBaccessToken.accessToken, 'facebook')
        }

        if (result.isCancelled) {
            Alert.alert("Are you sure you want to cancel login with facebook")
        }
    };

    render() {
        const { showSignUpForm, showSignInForm, showLoader } = this.state;
        const { error, success, loading } = this.props;
        console.log("success", success);
        // Show loader while user logging in by Google or Facebook
        if (showLoader)
            return (
                <View style={styles.screenContainer}>
                    <Text style={styles.txtStyle}>Signing In...</Text>
                    {Platform.OS === 'android' ?
                        <ProgressBarAndroid styleAttr="Horizontal" color="#2196F3" />
                        : null}
                </View>
            );

        const signUpFormSection = () => {
            if (!showSignUpForm) {
                return null;
            }
            return (
                <SignUpByEmailForm
                    hideShowForms={this.hideShowFormsHandler}
                    userSignUp={this.signUpUserHandler}
                    loading={loading}
                />
            );
        };
        const signInFormSection = () => {
            if (!showSignInForm) return null;

            return (
                <SignInByEmailForm
                    hideShowForms={this.hideShowFormsHandler}
                    userSignIn={this.signInUserHandler}
                    loading={loading}
                    navigation={this.props.navigation} />
            );
        };

        const renderGoogleSignInBtn = () => {
            return (
                <View>
                    <TouchableOpacity onPress={() => this.googleSignIn()}
                        disabled={this.state.isSigninInProgress}
                        style={{ flexDirection: 'row', backgroundColor: colors.white, width: 120, justifyContent: 'space-evenly', height: 30, borderRadius: 10, paddingTop: 4 }}>
                        <CustomText>Login with</CustomText>
                        <SvgIcon
                            icon={Google}
                            width={18}
                            height={18}
                        />
                    </TouchableOpacity>
                </View>
            );
        }

        const renderFBSignInBtn = () => {
            return (
                <View>
                    <TouchableOpacity onPress={() => this.facebookSignIn()}
                        disabled={this.state.isSigninInProgress}
                        style={{ flexDirection: 'row', backgroundColor: colors.white, width: 120, justifyContent: 'space-evenly', height: 30, borderRadius: 10, paddingTop: 4 }}>
                        <CustomText>Login with</CustomText>
                        <Icon name="facebook-square" size={18} color="#4285F4" />
                    </TouchableOpacity>
                </View>
            );
        }


        const renderToastComponent = () => {
            let errMsg = '';
            console.log("error", error);
            if (error) {
                if (error.type === 'unverifiedEmail') {
                    errMsg = error.message;
                    return Alert.alert(
                        'Verification',
                        errMsg,
                        [
                            {
                                text: 'Cancel',
                                onPress: () => {
                                    return false;
                                },
                                style: 'cancel',
                            },

                            {
                                text: 'Verify Again',
                                onPress: () => this.props.sendEmailVerification(error.url)
                            },
                        ],
                        { cancelable: false },
                    );
                }
                errMsg = error;
                this.props.resetErrorSuccessMsg();
                return (showErrToast.showToast(errMsg));
            }
            else if (success) {
                let succesMsg = success;
                this.props.resetErrorSuccessMsg();
                return (showSuccessToast.showToast(succesMsg));
            }
            return null;
        };

        return (

            <ScrollView
                keyboardShouldPersistTaps='always'
                contentContainerStyle={styles.mainContainer}
                style={styles.topCont}>
                <View style={styles.subCont}>
                    <Image source={AppLogo} resizeMode="contain" style={{ width: 120, height: 120 }} />
                    {/* <Text style={{ marginTop: 6 }}>Don't miss any offer!</Text> */}
                    {signInFormSection()}
                    {signUpFormSection()}
                </View>
                <View style={styles.btmCont}>

                    <View style={styles.divider}>
                        <View style={styles.dividerLine}></View>
                        <View style={styles.circleBg}>
                            <CustomText style={styles.dividerTxt}>Or</CustomText>
                        </View>
                    </View>

                    {/* <CustomText>login with </CustomText> */}
                    <View style={styles.subBtmCont}>
                        {renderGoogleSignInBtn()}
                        {renderFBSignInBtn()}
                    </View>
                    {renderToastComponent()}
                </View>


            </ScrollView>
        );
    }
}
const mapStateToProps = state => {
    const { error, success, loading } = state.auth;
    return {
        error,
        success,
        loading,
    };
};

export default SocialLogin = connect(mapStateToProps, {
    onSocialLogin,
    registerUserByEmail,
    loginUserByEmail,
    resetErrorSuccessMsg,
    sendEmailVerification
})(SocialLogin);