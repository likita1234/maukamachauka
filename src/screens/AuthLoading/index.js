import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import { authAutoSignIn } from '../../actions';
import AppLogo from '../../assets/img/appLogo.png';
import NoInternetNotice from '../../components/presentational/notice/NoInternetNotice';

class AuthLoading extends Component {

    componentDidMount() {
        setTimeout(() => {
            this.props.authAutoSignIn();
        }, 3000);
    }

    onretryNetConnectionHandler = () => {
        this.props.authAutoSignIn();
    };

    render() {
        const { noNetConnection } = this.props;

        return (
            <View style={{ backgroundColor: '#fff', flex: 1, justifyContent: "center", alignItems: 'center' }}>
                {noNetConnection && (
                    <NoInternetNotice
                        onretryNetConnection={this.onretryNetConnectionHandler}
                        showPopUp={true}
                    />
                )}
                <Image source={AppLogo} resizeMode="contain" style={{ width: 200, height: 200 }} />
            </View>
        );
    }
}

const mapStateToProps = state => {
    const { noNetConnection } = state.ui;
    return {
        noNetConnection,
    };
};

export default AuthLoading = connect(mapStateToProps, { authAutoSignIn })(AuthLoading);
