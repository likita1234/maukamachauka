import {
    FETCH_USER_OFFER,
    FETCH_CATEGORY_OFFER_LISTS,
    OFFER_LIKE_BY_USER,
    OFFER_DISLIKE_BY_USER,
    ADD_LIKE_PREFERENCE_OF_OFFERS,
    ADD_LIKE_PREFERENCE_OF_CAT_OFFERS,
    ADD_LIKE_PREFERENCE_OF_BRAND_OFFERS,
    REMOVE_LIKE_PREFERENCE_OF_OFFERS,
    REMOVE_LIKE_PREFERENCE_OF_CAT_OFFERS,
    REMOVE_LIKE_PREFERENCE_OF_BRAND_OFFERS,
    FETCH_BRAND_DETAIL,
    OFFER_ADDED_TO_FAVOURITE,
    OFFER_REMOVED_FROM_FAVOURITE,
    FETCH_USER_FAVOURITE_OFFER_LIST,
    ADD_FAVOURITE_PREFERENCE_OF_OFFERS,
    ADD_FAVOURITE_PREFERENCE_OF_CAT_OFFERS,
    ADD_FAVOURITE_PREFERENCE_OF_BRAND_OFFERS,
    REMOVE_FAVOURITE_PREFERENCE_OF_OFFERS,
    REMOVE_FAVOURITE_PREFERENCE_OF_CAT_OFFERS,
    REMOVE_FAVOURITE_PREFERENCE_OF_BRAND_OFFERS,
    FETCH_BRAND_OFFER_LISTS,
    FOLLOW_BRAND_OF_OFFERS,
    ADD_BRAND_FOLLOWED_BY_USER,
    UNFOLLOW_BRAND_OF_OFFERS,
    REMOVE_BRAND_FOLLOWED_BY_USER,
    SEND_ENQUIRY_MESSAGE,
    RESET_SUCCESS_ERROR_MESSAGE,
    SEARCH_OFFER_BY_TEXT
} from './actionTypes';
import executeApiAction from './helpers/actionHelper';
import { showFollowedBrandList, removeFBrandFromBrandList } from './ProfileActions';

export const fetchUserOffers = (pageNum, callback = () => { }) => {
    //console.log("pageNum", pageNum);

    return executeApiAction(
        'get',
        `offers?page=${pageNum}`,
        FETCH_USER_OFFER,
        true,
        {},
        res => {
            //  console.log("Succesfully fetched  offer list", res)
            callback()
        },
    );
}

export const fetchCatOfferLists = (pageNo, categoryId, callback = () => { }) => {
    return executeApiAction(
        'get',
        `category/offers/${categoryId}?page=${pageNo}`,
        FETCH_CATEGORY_OFFER_LISTS,
        true,
        {},
        res => {
            console.log("Fetched Category offer list successfully", res);
            callback();
        },
    );
}

export const showUserFavOfferList = (callback = () => { }) => {
    return executeApiAction(
        'get',
        'offer/favorites',
        FETCH_USER_FAVOURITE_OFFER_LIST,
        true,
        {},
        res => {
            //console.log("Succesfully fetched  favourite offer list", res)
            callback()
        },
    );
}
export const updatelikePreference = (offerId, callback = () => { }) => {
    return (dispatch) => {
        dispatch(executeApiAction(
            'get',
            `offer/like/add/${offerId}`,
            OFFER_LIKE_BY_USER,
            true,
            {},
            res => {
                //   console.log("callback after pref like in action", res);
                callback();
            }
        ));
    }
}

//like offer
export const addlikePrefOffers = offerID => {
    return dispatch => {
        dispatch({
            type: ADD_LIKE_PREFERENCE_OF_OFFERS,
            payload: offerID,
        });
    };
};

export const addlikePrefCatOffers = offerID => {
    return dispatch => {
        dispatch({
            type: ADD_LIKE_PREFERENCE_OF_CAT_OFFERS,
            payload: offerID,
        });
    };
};
export const addlikePrefBrandOffers = offerID => {
    return dispatch => {
        dispatch({
            type: ADD_LIKE_PREFERENCE_OF_BRAND_OFFERS,
            payload: offerID,
        });
    };
};



export const updateDislikePreference = (offerId, callback = () => { }) => {
    return (dispatch) => {
        dispatch(executeApiAction(
            'delete',
            `offer/like/remove/${offerId}`,
            OFFER_DISLIKE_BY_USER,
            true,
            {},
            res => {
                // console.log("callback after pref dislike in action", res);
                callback();
            },
        ));
    }
}

//remove like offer 
export const removelikePrefOffers = offerID => {
    return dispatch => {
        dispatch({
            type: REMOVE_LIKE_PREFERENCE_OF_OFFERS,
            payload: offerID,
        });
    };
};

export const removelikePrefCatOffers = offerID => {
    return dispatch => {
        dispatch({
            type: REMOVE_LIKE_PREFERENCE_OF_CAT_OFFERS,
            payload: offerID,
        });
    };
};

export const removelikePrefBrandOffers = offerID => {
    return dispatch => {
        dispatch({
            type: REMOVE_LIKE_PREFERENCE_OF_BRAND_OFFERS,
            payload: offerID,
        });
    };
};


export const updateFavouritePref = (offerId, callback = () => { }) => {
    return (dispatch) => {
        dispatch(executeApiAction(
            'get',
            `offer/favorite/add/${offerId}`,
            OFFER_ADDED_TO_FAVOURITE,
            true,
            {},
            res => {
                // console.log("callback after added favourite in action", res);
                callback();
                dispatch(showUserFavOfferList());
            }
        ))
    }
}
//Favourite offers of users 
export const addFavPrefOffers = offerID => {
    return dispatch => {
        dispatch({
            type: ADD_FAVOURITE_PREFERENCE_OF_OFFERS,
            payload: offerID,
        });
    };
};

export const addFavPrefCatOffers = offerID => {
    return dispatch => {
        dispatch({
            type: ADD_FAVOURITE_PREFERENCE_OF_CAT_OFFERS,
            payload: offerID,
        });
    };
};

export const addFavPrefBrandOffers = offerID => {
    return dispatch => {
        dispatch({
            type: ADD_FAVOURITE_PREFERENCE_OF_BRAND_OFFERS,
            payload: offerID,
        });
    };
};

export const updateUnFavouritePref = (offerId, callback = () => { }) => {
    return (dispatch) => {
        dispatch(executeApiAction(
            'delete',
            `offer/favorite/remove/${offerId}`,
            OFFER_REMOVED_FROM_FAVOURITE,
            true,
            {},
            res => {
                //  console.log("callback after removed favourite in action", res);
                callback();
                dispatch(showUserFavOfferList());
            },
        ));
    }
}

//remove user Favourite offer 
export const removeFavPrefOffers = offerID => {
    return dispatch => {
        dispatch({
            type: REMOVE_FAVOURITE_PREFERENCE_OF_OFFERS,
            payload: offerID,
        });
    };
};

export const removeFavPrefCatOffers = offerID => {
    return dispatch => {
        dispatch({
            type: REMOVE_FAVOURITE_PREFERENCE_OF_CAT_OFFERS,
            payload: offerID,
        });
    };
};

export const removeFavPrefBrandOffers = offerID => {
    return dispatch => {
        dispatch({
            type: REMOVE_FAVOURITE_PREFERENCE_OF_BRAND_OFFERS,
            payload: offerID,
        });
    };
};


export const showBrandDetail = (brandId) => {
    return executeApiAction(
        'get',
        `brands/${brandId}`,
        FETCH_BRAND_DETAIL,
        true,
        {},
        res => {
            console.log("Fetched brand detail successfully", res);

        },
    );
};

export const followBrand = (brandId, callback = () => { }) => {
    return (dispatch) => {
        dispatch(executeApiAction(
            'get',
            `user/brand/follow/${brandId}`,
            FOLLOW_BRAND_OF_OFFERS,
            true,
            {},
            res => {
                //  console.log("callback after follow brand in action", res);
                dispatch(addFollowedBrand(brandId));
                callback();
                dispatch(showFollowedBrandList());
            },
        ));
    }
};

export const addFollowedBrand = brandId => {
    return dispatch => {
        dispatch({
            type: ADD_BRAND_FOLLOWED_BY_USER,
            payload: brandId,
        });
    };
};

export const unFollowBrand = (brandId, fromScreen, callback = () => { }) => {
    return (dispatch) => {
        dispatch(executeApiAction(
            'delete',
            `user/brand/unFollow/${brandId}`,
            UNFOLLOW_BRAND_OF_OFFERS,
            true,
            {},
            res => {
                // console.log("callback after unfollow brand in action", res);
                if (fromScreen === 'profile') {
                    dispatch(removeFBrandFromBrandList(brandId));
                    callback();
                    dispatch(showBrandDetail(brandId))
                }
                dispatch(removeFollowedBrand(brandId));
                callback();
                dispatch(showFollowedBrandList());
            }
        ));
    }
};

export const removeFollowedBrand = brandId => {
    return dispatch => {
        dispatch({
            type: REMOVE_BRAND_FOLLOWED_BY_USER,
            payload: brandId,
        });
    };
};

export const showBrandOffer = (brandId) => {
    return executeApiAction(
        'get',
        `brand/offers/${brandId}`,
        FETCH_BRAND_OFFER_LISTS,
        true,
        {},
        res => {
            //  console.log("Fetched brand offer list successfully", res);

        },
    );
};

export const sendBrandEnquiry = (idToEnquiry, message, fromBrand, callback = () => { }) => {
    return executeApiAction(
        'post',
        `user/brand/enquiry`,
        SEND_ENQUIRY_MESSAGE,
        true,
        fromBrand === 'brand' ?
            { brand_id: idToEnquiry, offer_id: null, message: message }
            :
            { brand_id: null, offer_id: idToEnquiry, message: message },
        res => {
            //   console.log("Send brand enquiry successfully", res);
            callback();

        },
    );

};

export const removeErrorSuccessMsg = () => {
    return dispatch => {
        dispatch({
            type: RESET_SUCCESS_ERROR_MESSAGE
        });
    }
};

export const searchOffer = (searchTxt) => {
    return executeApiAction(
        'post',
        `offer/search`,
        SEARCH_OFFER_BY_TEXT,
        true,
        { search: searchTxt },
        res => {
            console.log("Search offer list successfully", res);

        },
    );
};