import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import executeApiAction from './helpers/actionHelper';
import {
    AUTH_SET_TOKEN,
    AUTH_SET_ACCESS_TOKEN,
    SET_DEVICE_TOKEN,
    SET_LIST_OF_NEARBY_LOCATION,
    REGISTER_BY_EMAIL,
    SIGNIN_BY_EMAIL,
    SIGNIN_BY_SOCIAL_LOGIN,
    RESET_ERROR_SUCCESS_MSG,
    CLEAR_AUTH,
    SEND_EMAIL_VERIFICATION,
    SEND_EMAIL_VERIFICATION_SUCCESS,
    SEND_EMAIL_VERIFICATION_ERROR,
    NO_INTERNET_CONNECTION

} from '../actions/actionTypes';
import DeviceInfo from 'react-native-device-info';
import { apiBaseUrl } from '../../app.json';
import { pushNotifications } from '../services';
import NavigationService from '../navigator/NavigationService';

let deviceId = DeviceInfo.getUniqueId();

export const registerUserByEmail = (formData, callback = () => { }) => {
    return (dispatch) => {
        dispatch(getDeviceToken())
            .then(token => {
                console.log('Inside authAutoSignIn - device Token', token);
                if (token === 'NOTOKEN') {
                    // generating random device token manually if user haven't allowed notf permission
                    token = token + '-' + deviceId + '-' + Math.random();
                }
                dispatch(executeApiAction(
                    'post',
                    'register_user',
                    REGISTER_BY_EMAIL,
                    true,
                    {
                        fname: formData.fname,
                        lname: formData.lname,
                        email: formData.email,
                        password: formData.password,
                        device_id: deviceId,
                        device_token: token
                    },
                    response => {
                        console.log('User signed Up Successfully!', response)
                        callback();
                    },

                ))
                    .catch(err => {
                        console.log('Error on signup!', err);
                    });
            })
            .catch(err => {
                console.log('Failed to fetch device token!', err);
            });
    };
};

export const loginUserByEmail = (formData) => {
    return (dispatch) => {

        dispatch(getDeviceToken())
            .then(token => {
                console.log('Inside authAutoSignIn - device Token', token);
                if (token === 'NOTOKEN') {
                    // generating random device token manually if user haven't allowed notf permission
                    token = token + '-' + deviceId + '-' + Math.random();
                }
                dispatch(executeApiAction(
                    'post',
                    'login',
                    SIGNIN_BY_EMAIL,
                    true,
                    {
                        email: formData.email,
                        password: formData.password,
                        device_id: deviceId,
                        device_token: token
                    },
                    response => {
                        console.log('User signed in Successfully!', response)
                        if (response && response.tokens.access_token) {
                            console.log("access_token", response.tokens.access_token);
                            let accessToken = response.tokens.access_token;
                            let refreshToken = response.tokens.refresh_token;

                            dispatch(authStoreToken(accessToken, refreshToken));
                            NavigationService.navigate('App');

                        }
                    }
                )).catch(err => {
                    console.log('Error on signin!', err);

                });

            })
            .catch(err => {
                console.log('Failed to fetch device token!', err);
            });
    };
};

export const onSocialLogin = (userToken, socialServiceType) => {
    console.log("userToken", userToken);
    //console.log("socialService", socialServiceType);
    return (dispatch) => {
        dispatch(getDeviceToken())
            .then(token => {
                console.log('Inside authAutoSignIn - device Token', token);
                if (token === 'NOTOKEN') {
                    // generating random device token manually if user haven't allowed notf permission
                    token = token + '-' + deviceId + '-' + Math.random();
                }
                dispatch(executeApiAction(
                    'post',
                    'socialLogin',
                    SIGNIN_BY_SOCIAL_LOGIN,
                    true,
                    { client: socialServiceType, token: userToken, device_id: deviceId, device_token: token },
                    response => {
                        console.log("response on social login", response)
                        if (response && response.tokens.access_token) {
                            let accessToken = response.tokens.access_token;
                            let refreshToken = response.tokens.refresh_token;

                            dispatch(authStoreToken(accessToken, refreshToken));
                            NavigationService.navigate('App');

                        }
                    })).catch(err => {
                        console.log('Error on social login!', err.response);

                    });
            })
            .catch(err => {
                console.log('Failed to fetch device token!', err);
            });
    }
};

export const authStoreToken = (accessToken, refToken) => {
    return async dispatch => {
        AsyncStorage.setItem('ap:auth:aToken', accessToken);
        AsyncStorage.setItem('ap:auth:rToken', refToken);
        dispatch(authSetToken(accessToken, refToken));
    };
};

export const authSetToken = (accessToken, refToken) => {
    return {
        type: AUTH_SET_TOKEN,
        aToken: accessToken,
        rToken: refToken,
    };
};

export const authGetToken = () => {
    return (dispatch, getState) => {
        const promise = new Promise((resolve, reject) => {
            const token = getState().auth.aToken; // first check from global state
            if (!token) {
                console.log('Nothing in state copying from store to state -----');
                AsyncStorage.getItem('ap:auth:aToken')
                    .catch(err => reject(err))
                    .then(tokenFromStorage => {
                        if (!tokenFromStorage) {
                            console.log('No token in storage');
                            reject();
                            return;
                        }
                        console.log('Token found on storage ++++++ ');
                        dispatch({ type: AUTH_SET_ACCESS_TOKEN, payload: tokenFromStorage });
                        resolve(tokenFromStorage);
                    });
            } else {
                console.log('Returning token from state');
                resolve(token);
            }
        });
        promise.catch(err => {
            console.log(err);
            dispatch(authClearStorage());
        });
        return promise;
    };
};

export const setDeviceToken = deviceToken => {
    console.log('set device token in state ');
    return {
        type: SET_DEVICE_TOKEN,
        payload: deviceToken,
    };
};

export const getDeviceToken = () => {
    return (dispatch, getState) => {
        const promise = new Promise((resolve, reject) => {
            const token = getState().auth.deviceToken; // first check from global state
            if (!token) {
                console.log('Nothing in state copying from store to state -----');
                AsyncStorage.getItem('ap:auth:deviceToken')
                    .catch(err => reject(err))
                    .then(tokenFromStorage => {
                        if (!tokenFromStorage) {
                            console.log('No token in storage');

                            // ask permission from FCM for IOS to get token
                            return resolve("NOTOKEN");
                            /* temporary
                            pushNotifications.configureIOS(res => {
                                console.log('Token from configure IOS - ', res);
                                return resolve(res);
                            }); */
                        } else {
                            console.log('Token found on storage ++++++ ');
                            dispatch(setDeviceToken(tokenFromStorage));
                            resolve(tokenFromStorage);
                        }
                    });
            } else {
                console.log('Returning token from state');
                resolve(token);
            }
        });
        promise.catch(err => {
            console.log(err);
        });
        return promise;
    };
};

// check if token exist or not at first
export const authAutoSignIn = () => {
    return async dispatch => {
        // Check if we have internet connection
        let hasNetwork = await NetInfo.fetch();

        if (!hasNetwork.isConnected) { // No Connection
            dispatch({ type: NO_INTERNET_CONNECTION, payload: 'No Internet Connection!' });
            return;
        }

        dispatch(authGetToken())
            .then(token => {
                console.log('Inside authAutoSignIn - Token exist Not to login');
                if (token) {
                    NavigationService.navigate('App');
                }

            })
            .catch(err => {
                console.log('Failed to fetch token!', err);
                NavigationService.navigate('Login');
                // dispatch(logOut());
            });
    };
};

export const resetErrorSuccessMsg = () => {
    return {
        type: RESET_ERROR_SUCCESS_MSG
    }
};

export const logOut = () => {
    return dispatch => {
        dispatch(authClearStorage());
        NavigationService.navigate('Auth');
    };
};

export const getNearbyAddressList = (lat, long) => {
    return (dispatch) => {
        let url = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${lat},${long}&radius=1500&type=restaurant&key=AIzaSyAI8HjAm2YlutVZE0aADKlFRzia5YMrwhY`;

        //dispatch(uiStartLoading());
        //dispatch(authGetToken())
        axios
            .get(url, {
            })
            .then(response => {

                if (response.status == 200) {
                    console.log('responseMap', response);

                    // const coordinates = {
                    //     latitudeVal: response.data.results.geometry.location.lat,
                    //     longitudeVal: response.data.results.geometry.location.lng

                    // }
                    dispatch(nearByLocationLatLong(response.data.results))
                } else {
                    throw new Error('Something went wrong!');
                }
            })
            .catch(error => {
                console.log('Error!', error);

            });
    };
};

export const authClearStorage = () => {
    return dispatch => {
        dispatch(authClearState());
        AsyncStorage.removeItem('ap:auth:aToken');
        AsyncStorage.removeItem('ap:auth:rToken');
    };
};

export const authClearState = () => {
    return { type: CLEAR_AUTH };
};

// export const nearByLocationLatLong = (nearbylocationData) => {
//     return {
//         type: SET_LIST_OF_NEARBY_LOCATION,
//         payload: nearbylocationData

//     };
// };

export const sendEmailVerification = (verificationUrl) => {
    return (dispatch, getState) => {
        dispatch({ type: SEND_EMAIL_VERIFICATION });
        const url = verificationUrl;
        const accessToken = getState().auth.aToken;
        axios
            .get(url, {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            })
            .then(response => {
                console.log('Successfully Verified!');
                if (response.status === 200) {
                    console.log("response", response.data.message);
                    dispatch({ type: SEND_EMAIL_VERIFICATION_SUCCESS, payload: response.data.message });
                }
            })
            .catch(error => {
                console.log(error.response);
                dispatch({ type: SEND_EMAIL_VERIFICATION_ERROR, payload: error.response });
            })
    };
};

export const resetForgetPassword = (email) => {
    console.log("User Email", email)
}