import {
    FETCH_USER_INFO,
    UPDATE_USER_PROFILE,
    UPDATE_USER_PROFILE_IMAGE,
    FETCH_USER_SUBSCRIBED_OFFER_LIST,
    FETCH_USER_UNSUBSCRIBED_OFFER_LIST,
    OFFER_ADDED_TO_SUBSCRIBE,
    REMOVE_OFFER_FROM_UNSUBSCRIBE_LIST,
    SUBSCRIBE_OFFER_BTN_CLICK,
    SUBSCRIBE_OFFER_ACTION_COMPLETE,
    OFFER_ADDED_TO_UNSUBSCRIBE,
    REMOVE_OFFER_FROM_SUBSCRIBE_LIST,
    UNSUBSCRIBE_OFFER_BTN_CLICK,
    UNSUBSCRIBE_OFFER_ACTION_COMPLETE,
    FETCH_USER_FOLLOWED_BRAND_LIST,
    REMOVE_FBRAND_FROM_FBRAND_LIST
} from '../actions/actionTypes';
import executeApiAction from './helpers/actionHelper';


export const fetchUserInfo = () => {

    return executeApiAction(
        'get',
        'profile',
        FETCH_USER_INFO,
        true,
        {},
        res => {
            //    console.log("Succesfully fetched User Info", res)
        },
    );
}

export const updateUserProfile = (data) => {
    console.log("Date of Birth", data.dob);
    return (dispatch) => {
        dispatch(executeApiAction(
            'post',
            'profile/update',
            UPDATE_USER_PROFILE,
            true,
            { fname: data.fname, lname: data.lname, phone: data.phone, dob: data.dob, gender: data.gender },
            res => {
                console.log("Succesfully updated User Info", res)
                dispatch(fetchUserInfo());

            },
        )

        );
    }

}
//To upload user profile image
export const uploadImage = (image_uri, file_name, image_type) => {
    return (dispatch) => {
        var photo = new FormData();
        photo.append('photo', {
            uri: image_uri,
            name: file_name,
            type: image_type,
        });
        dispatch(executeApiAction(
            'post',
            'profile/update',
            UPDATE_USER_PROFILE_IMAGE,
            true,
            photo,
            res => {
                //  console.log("Succesfully uploaded User picture", res)
                dispatch(fetchUserInfo());

            },
        )

        );
    }

}

export const showSubscribeOfferList = (callback = () => { }) => {
    return executeApiAction(
        'get',
        'user/tag/subscribed ',
        FETCH_USER_SUBSCRIBED_OFFER_LIST,
        true,
        {},
        res => {
            //console.log("Succesfully fetched  Subscribe offer list", res)
            callback()
        },
    );
}

export const showUnSubscribeOfferList = (callback = () => { }) => {
    return executeApiAction(
        'get',
        'user/tag/notSubscribed ',
        FETCH_USER_UNSUBSCRIBED_OFFER_LIST,
        true,
        {},
        res => {
            //console.log("Succesfully fetched  UnSubscribe offer list", res)
            callback()
        },
    );
}
//Unsubscribed Offer added to subscribed list
export const subscribedOffer = (offerId, callback = () => { }) => {
    return (dispatch) => {
        dispatch(clickedonSubscribe(offerId));
        dispatch(executeApiAction(
            'get',
            `user/tag/subscribe/${offerId}`,
            OFFER_ADDED_TO_SUBSCRIBE,
            true,
            {},
            res => {
                //  console.log("Succesfully subscribed unsubscribed offer", res)
                dispatch(removeOfferFromUnSubscribeList(offerId));
                dispatch(subscribeActionComplete());
                callback();

            },
        )
        );
    }
}

export const removeOfferFromUnSubscribeList = offerId => {
    return dispatch => {
        dispatch({
            type: REMOVE_OFFER_FROM_UNSUBSCRIBE_LIST,
            payload: offerId,
        });
    };
};

export const clickedonSubscribe = offerId => {
    return dispatch => {
        dispatch({
            type: SUBSCRIBE_OFFER_BTN_CLICK,
            payload: offerId,
        });
    };
};

export const subscribeActionComplete = () => {
    return dispatch => {
        dispatch({
            type: SUBSCRIBE_OFFER_ACTION_COMPLETE,
        });
    };
};


//Subscribed Offer added to UnSubscribed list
export const unSubscribedOffer = (offerId, callback = () => { }) => {
    return (dispatch) => {
        dispatch(clickedonUnSubscribe(offerId));
        dispatch(executeApiAction(
            'delete',
            `user/tag/unSubscribe/${offerId}`,
            OFFER_ADDED_TO_UNSUBSCRIBE,
            true,
            {},
            res => {
                //  console.log("Succesfully subscribed unsubscribed offer", res)
                dispatch(removeOfferFromSubscribeList(offerId));
                dispatch(unSubscribeActionComplete());
                callback();


            },
        )
        );
    }
}

export const removeOfferFromSubscribeList = offerId => {
    return dispatch => {
        dispatch({
            type: REMOVE_OFFER_FROM_SUBSCRIBE_LIST,
            payload: offerId,
        });
    };
};

export const clickedonUnSubscribe = offerId => {
    return dispatch => {
        dispatch({
            type: UNSUBSCRIBE_OFFER_BTN_CLICK,
            payload: offerId,
        });
    };
};

export const unSubscribeActionComplete = () => {
    return dispatch => {
        dispatch({
            type: UNSUBSCRIBE_OFFER_ACTION_COMPLETE,
        });
    };
};

export const showFollowedBrandList = () => {
    return executeApiAction(
        'get',
        'user/brand/followed ',
        FETCH_USER_FOLLOWED_BRAND_LIST,
        true,
        {},
        res => {
            //   console.log("Succesfully fetched  followed brand list", res)
            //callback()
        },
    );
}

export const removeFBrandFromBrandList = brandId => {
    return dispatch => {
        dispatch({
            type: REMOVE_FBRAND_FROM_FBRAND_LIST,
            payload: brandId,
        });
    };
};