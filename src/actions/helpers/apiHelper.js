import axios from 'axios';
import { apiBaseUrl } from '../../../app.json';

const constructUrl = api => `${apiBaseUrl}${api}`;

const callApi = async (method, api, authorizationToken, data = {}) => {
    const headers = () => {
        if (authorizationToken) {

            return {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${authorizationToken}`,
            };
        }
        return {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
    };

    let parameters = {
        method: method,
        url: constructUrl(api),
        headers: headers(),
    };
    if (method !== 'get') {
        parameters = { ...parameters, data: data };
    }

    try {
        const response = await axios(parameters);
        //  console.log(response);

        return { status: response.status, data: response.data };
    } catch (error) {
        // console.log(error);
        throw error;
    }
};

export default callApi;
