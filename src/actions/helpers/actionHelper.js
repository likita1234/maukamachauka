import NetInfo from "@react-native-community/netinfo";
import callApi from './apiHelper';
import { SUCCESS_SUFFIX, ERROR_SUFFIX, NO_INTERNET_CONNECTION, INTERNET_CONNECTION_RESTORED } from '../actionTypes';
import { logOut } from '../../actions';

// note: remember to define action constants with both SUCCESS and ERROR suffixes
// in the file which contains the root action constant (current file is actionTypes.js).
// e.g. if your action constant is FOO, you should also define FOO_SUCCESS and FOO_ERROR
// before calling this function.
const executeApiAction = (
    method,
    api,
    action,
    authorize = false,
    data = {},
    onSuccess = () => { },
    errMsg = '',
) => {
    const actionSuccess = action + SUCCESS_SUFFIX;
    const actionError = action + ERROR_SUFFIX;

    return async (dispatch, getState) => {
        try {
            // Check internet connection
            let hasNetwork = await NetInfo.fetch();
            if (hasNetwork.isConnected) { // No Connection
                dispatch({ type: INTERNET_CONNECTION_RESTORED, payload: 'No Internet Connection!' });
            } else {
                dispatch({ type: NO_INTERNET_CONNECTION, payload: 'No Internet Connection!' });
                return;
            }

            dispatch({ type: action });

            const authorizationToken = authorize ? getState().auth.aToken : '';
            const response = await callApi(method, api, authorizationToken, data);
            // console.log('response = ');
            // console.log(response);

            if (response.status !== 200) {
                console.log('STATUS NOT EQUAL TO 200 ---', response.status)
                throw new Error();
            }

            dispatch({ type: actionSuccess, payload: response.data });

            onSuccess(response.data);

        } catch (error) {

            console.log('ERROR ON API CALL RESULT ---', error.response);
            console.log("err", error.response.data.message)

            if (isAuthorizationError(error)) {
                // clear local storage and redirect to login screen
                dispatch({ type: actionError, payload: error.response.data.message ? error.response.data.message : null });
                dispatch(logOut());
                return;
            }

            if (is500Error(error)) {
                console.log('500 error occured', error);
                dispatch({ type: actionError, payload: errMsg });
                return;
            }

            if (is400Error(error)) {
                console.log('400 error occured - ', error.response);
                dispatch({ type: actionError, payload: error.response.data });
                return;
            }

            if (is422Error(error)) {
                console.log('422 error occured - ', error.response.data.errors);
                let keys = Object.keys(error.response.data.errors);
                let firstKey = keys[0];
                errMsg = error.response.data.errors[firstKey][0];
                console.log("errMsg", errMsg);
                dispatch({ type: actionError, payload: errMsg });
                return;
            }

            if (is403Error(error)) {
                console.log('403 error occured - ', error.response);

                if (error.response.data.type === 'unverifiedEmail') {
                    dispatch({ type: actionError, payload: error.response.data });
                    return;
                } else {
                    dispatch({ type: actionError, payload: error.response.data.message });
                    return;
                }

            }

            dispatch({ type: actionError, payload: error.response.data.message });
        }
    };
};

const isAuthorizationError = (error) => {
    return error.response && parseInt(error.response.status, 10) === 401;
};

const is500Error = (error) => {
    return error.response && parseInt(error.response.status, 10) === 500;
};

const is400Error = (error) => {
    return error.response && parseInt(error.response.status, 10) === 400;
};

const is422Error = (error) => {
    return error.response && parseInt(error.response.status, 10) === 422;
};

const is403Error = (error) => {
    return error.response && parseInt(error.response.status, 10) === 403;
};


export default executeApiAction;