const SignInValidate = (email, pwd) => {
    const errors = {};
    if (email === undefined) {
        errors.email = "Email Address Required";
    } else if (email.trim() === "") {
        errors.email = "Email Address must not be blank";
    } else if (!emailValidator(email)) {
        errors.email = "Invalid Email Address";
    }


    if (pwd === undefined) {
        errors.pwd = "Password Required";
    } else if (pwd.trim() === "") {
        errors.pwd = "Password must not be blank";
    } else if (pwd.length < 6) {
        errors.pwd = "Password length must be at least 6";
    }


    return errors;
}
const emailValidator = val => {
    return /^\s*(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/.test(val);
};

export default SignInValidate;