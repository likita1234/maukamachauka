import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import DashboardReducer from './DashboardReducer';
import ProfileReducer from './ProfileReducer';
import UIReducer from './UiReducer';


export default combineReducers({
    auth: AuthReducer,
    dashboard: DashboardReducer,
    profile: ProfileReducer,
    ui: UIReducer,

});
