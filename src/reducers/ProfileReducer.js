import {
    // FETCH_USER_FAVOURITE_OFFER_LIST,
    // FETCH_USER_FAVOURITE_OFFER_LIST_SUCCESS,
    // FETCH_USER_FAVOURITE_OFFER_LIST_ERROR,
    REMOVE_OFFER_FROM_FAVOURITE_LISTS,
    FETCH_USER_INFO_SUCCESS,
    FETCH_USER_INFO_ERROR,
    UPDATE_USER_PROFILE,
    UPDATE_USER_PROFILE_SUCCESS,
    UPDATE_USER_PROFILE_ERROR,
    RESET_ERROR_SUCCESS_MSG,
    UPDATE_USER_PROFILE_IMAGE,
    UPDATE_USER_PROFILE_IMAGE_SUCCESS,
    UPDATE_USER_PROFILE_IMAGE_ERROR,
    FETCH_USER_UNSUBSCRIBED_OFFER_LIST,
    FETCH_USER_UNSUBSCRIBED_OFFER_LIST_SUCCESS,
    FETCH_USER_UNSUBSCRIBED_OFFER_LIST_ERROR,
    REMOVE_OFFER_FROM_UNSUBSCRIBE_LIST,
    SUBSCRIBE_OFFER_BTN_CLICK,
    SUBSCRIBE_OFFER_ACTION_COMPLETE,
    FETCH_USER_SUBSCRIBED_OFFER_LIST,
    FETCH_USER_SUBSCRIBED_OFFER_LIST_SUCCESS,
    FETCH_USER_SUBSCRIBED_OFFER_LIST_ERROR,
    REMOVE_OFFER_FROM_SUBSCRIBE_LIST,
    UNSUBSCRIBE_OFFER_BTN_CLICK,
    UNSUBSCRIBE_OFFER_ACTION_COMPLETE,
    FETCH_USER_FOLLOWED_BRAND_LIST,
    FETCH_USER_FOLLOWED_BRAND_LIST_SUCCESS,
    FETCH_USER_FOLLOWED_BRAND_LIST_ERROR,
    REMOVE_FBRAND_FROM_FBRAND_LIST

} from '../actions/actionTypes';

const INITIAL_STATE = {
    favOffers: null,
    error: null,
    favOffListLoading: false,
    user: null,
    isloading: false,
    successMsg: null,
    errorMsg: null,
    UnSubscribedOffers: null,
    UnSubOffListLoading: false,
    currIdSubscribe: null,
    subscribedOffers: null,
    subOffListLoading: false,
    currIdUnSubscribe: null,
    followBrand: null,
    followBrandLoading: false,
    refreshList: false,


};
const ProfileReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        // case FETCH_USER_FAVOURITE_OFFER_LIST:
        //     return {
        //         ...state,
        //         favOffListLoading: true
        //     };
        // case FETCH_USER_FAVOURITE_OFFER_LIST_SUCCESS:
        //     return {
        //         ...state,
        //         favOffers: action.payload,
        //         favOffListLoading: false
        //     };
        // case FETCH_USER_FAVOURITE_OFFER_LIST_ERROR:
        //     return {
        //         ...state,
        //         favOffListLoading: true
        //     };
        // case REMOVE_OFFER_FROM_FAVOURITE_LISTS:
        //     return {
        //         ...state,
        //         offers: transformFavListAfterRemove(state.favOffers, action.payload),
        //     };
        case FETCH_USER_INFO_SUCCESS:
            return {
                ...state,
                user: action.payload
            }
        case FETCH_USER_INFO_ERROR:
            return {
                ...state,
                error: 'Error on Fetching User Info'
            }
        case UPDATE_USER_PROFILE:
            return {
                ...state,
                isloading: true,
                successMsg: null,
                errorMsg: null
            };
        case UPDATE_USER_PROFILE_SUCCESS:
            return {
                ...state,
                successMsg: action.payload.message,
                isloading: false
            };
        case UPDATE_USER_PROFILE_ERROR:
            return {
                ...state,
                isloading: false,
                errorMsg: action.payload
            };
        case UPDATE_USER_PROFILE_IMAGE:
            return {
                ...state,
                isloading: true,
                successMsg: null,
                errorMsg: null
            };
        case UPDATE_USER_PROFILE_IMAGE_SUCCESS:
            return {
                ...state,
                successMsg: action.payload.message,
                isloading: false
            };
        case UPDATE_USER_PROFILE_IMAGE_ERROR:
            return {
                ...state,
                isloading: false,
                errorMsg: action.payload
            };
        case RESET_ERROR_SUCCESS_MSG: {
            return {
                ...state,
                errorMsg: null,
                successMsg: null,
            }
        }
        case FETCH_USER_UNSUBSCRIBED_OFFER_LIST:
            return {
                ...state,
                UnSubOffListLoading: true
            };
        case FETCH_USER_UNSUBSCRIBED_OFFER_LIST_SUCCESS:
            return {
                ...state,
                UnSubscribedOffers: action.payload,
                UnSubOffListLoading: false
            };
        case FETCH_USER_UNSUBSCRIBED_OFFER_LIST_ERROR:
            return {
                ...state,
                UnSubOffListLoading: true
            };
        case REMOVE_OFFER_FROM_UNSUBSCRIBE_LIST:
            return {
                ...state,
                UnSubscribedOffers: transformUnSubscribedList(state.UnSubscribedOffers, action.payload),
            };
        case SUBSCRIBE_OFFER_BTN_CLICK:
            return {
                ...state,
                currIdSubscribe: action.payload,
            };
        case SUBSCRIBE_OFFER_ACTION_COMPLETE:
            return {
                ...state,
                currIdSubscribe: null,
            };
        case FETCH_USER_SUBSCRIBED_OFFER_LIST:
            return {
                ...state,
                subOffListLoading: true
            };
        case FETCH_USER_SUBSCRIBED_OFFER_LIST_SUCCESS:
            return {
                ...state,
                subscribedOffers: action.payload,
                subOffListLoading: false,

            };
        case FETCH_USER_SUBSCRIBED_OFFER_LIST_ERROR:
            return {
                ...state,
                subOffListLoading: true
            };
        case REMOVE_OFFER_FROM_SUBSCRIBE_LIST:
            return {
                ...state,
                subscribedOffers: transformSubscribedList(state.subscribedOffers, action.payload),
            };
        case UNSUBSCRIBE_OFFER_BTN_CLICK:
            return {
                ...state,
                currIdUnSubscribe: action.payload,
            };
        case UNSUBSCRIBE_OFFER_ACTION_COMPLETE:
            return {
                ...state,
                currIdUnSubscribe: null,
            };
        case FETCH_USER_FOLLOWED_BRAND_LIST:
            return {
                ...state,
                followBrandLoading: true
            }
        case FETCH_USER_FOLLOWED_BRAND_LIST_SUCCESS:
            return {
                ...state,
                followBrandLoading: false,
                followBrand: action.payload
            }
        case FETCH_USER_FOLLOWED_BRAND_LIST_ERROR:
            return {
                ...state,
                followBrandLoading: true
            }
        case REMOVE_FBRAND_FROM_FBRAND_LIST: {
            return {
                ...state,
                refreshList: !state.refreshList,
                followBrand: transformBrandAfterUnFollow(state.followBrand, action.payload),
            }
        }
        default:
            return state;
    }
};

const transformUnSubscribedList = (oldUnSubList, offerIdToRemove) => {
    if (offerIdToRemove) {
        let objIndex = oldUnSubList.findIndex((obj => obj.id === offerIdToRemove));
        if (objIndex != null) {
            oldUnSubList.splice(objIndex, 1);
        }
        return oldUnSubList;

    }
    return oldUnSubList;

}
const transformSubscribedList = (oldSubList, offerIdToRemove) => {
    if (offerIdToRemove) {
        let objIndex = oldSubList.findIndex((obj => obj.id === offerIdToRemove));
        if (objIndex != null) {
            oldSubList.splice(objIndex, 1);
        }
        return oldSubList;

    }
    return oldSubList;

}

const transformBrandAfterUnFollow = (oldFollowBrand, brandIdToRemove) => {
    if (brandIdToRemove) {
        let objIndex = oldFollowBrand.data.findIndex((obj => obj.id === brandIdToRemove));
        if (objIndex != null) {
            oldFollowBrand.data.splice(objIndex, 1);
        }

        return oldFollowBrand;

    }
    return oldFollowBrand;

}

export default ProfileReducer;
