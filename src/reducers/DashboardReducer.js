import {
    FETCH_USER_OFFER,
    FETCH_USER_OFFER_SUCCESS,
    FETCH_USER_OFFER_ERROR,
    OFFER_LIKE_BY_USER_SUCCESS,
    OFFER_DISLIKE_BY_USER_SUCCESS,
    ADD_LIKE_PREFERENCE_OF_OFFERS,
    ADD_LIKE_PREFERENCE_OF_CAT_OFFERS,
    ADD_LIKE_PREFERENCE_OF_BRAND_OFFERS,
    REMOVE_LIKE_PREFERENCE_OF_OFFERS,
    REMOVE_LIKE_PREFERENCE_OF_CAT_OFFERS,
    REMOVE_LIKE_PREFERENCE_OF_BRAND_OFFERS,
    FETCH_BRAND_DETAIL,
    FETCH_BRAND_DETAIL_SUCCESS,
    FETCH_BRAND_DETAIL_ERROR,
    ADD_FAVOURITE_PREFERENCE_OF_OFFERS,
    ADD_FAVOURITE_PREFERENCE_OF_CAT_OFFERS,
    ADD_FAVOURITE_PREFERENCE_OF_BRAND_OFFERS,
    REMOVE_FAVOURITE_PREFERENCE_OF_OFFERS,
    REMOVE_FAVOURITE_PREFERENCE_OF_CAT_OFFERS,
    REMOVE_FAVOURITE_PREFERENCE_OF_BRAND_OFFERS,
    FETCH_BRAND_OFFER_LISTS,
    FETCH_BRAND_OFFER_LISTS_SUCCESS,
    FETCH_BRAND_OFFER_LISTS_ERROR,
    FOLLOW_BRAND_OF_OFFERS_SUCCESS,
    ADD_BRAND_FOLLOWED_BY_USER,
    UNFOLLOW_BRAND_OF_OFFERS_SUCCESS,
    REMOVE_BRAND_FOLLOWED_BY_USER,
    FETCH_USER_FAVOURITE_OFFER_LIST,
    FETCH_USER_FAVOURITE_OFFER_LIST_SUCCESS,
    FETCH_USER_FAVOURITE_OFFER_LIST_ERROR,
    SEND_ENQUIRY_MESSAGE,
    SEND_ENQUIRY_MESSAGE_SUCCESS,
    SEND_ENQUIRY_MESSAGE_ERROR,
    RESET_SUCCESS_ERROR_MESSAGE,
    FETCH_CATEGORY_OFFER_LISTS,
    FETCH_CATEGORY_OFFER_LISTS_SUCCESS,
    FETCH_CATEGORY_OFFER_LISTS_ERROR,
    SEARCH_OFFER_BY_TEXT,
    SEARCH_OFFER_BY_TEXT_SUCCESS,
    SEARCH_OFFER_BY_TEXT_ERROR
} from '../actions/actionTypes';

const INITIAL_STATE = {
    offers: null,
    error: null,
    refreshList: false,
    offerListLoading: false,
    likeStatus: null,
    dislikeStatus: null,
    brandDetailLoading: false,
    brandInfo: null,
    brandListLoading: false,
    brandOfferList: null,
    categoryList: null,
    lastPage: null,
    followStatus: null,
    unFollowStatus: null,
    favOffers: null,
    favOffListLoading: false,
    success: null,
    catofferLists: null,
    catoffListLoading: false,
    catLastPage: null,
    searchList: null,
    isSearching: false

};

const DashboardReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FETCH_USER_OFFER:
            return {
                ...state,
                error: null,
                offerListLoading: true
            };
        case FETCH_USER_OFFER_SUCCESS:
            let currentPage = action.payload.offers.current_page;
            if (currentPage === 1) {
                return {
                    ...state,
                    offers: action.payload.offers.data,
                    categoryList: action.payload.categories,
                    offerListLoading: false,
                    lastPage: action.payload.offers.last_page
                };
            }
            else {
                return {
                    ...state,
                    offers: [...state.offers, ...action.payload.offers.data],//mergeOfferOnLoadMore
                }
            }
        case FETCH_USER_OFFER_ERROR:
            return {
                ...state,
                error: action.payload ? action.payload : 'Error on fetching User Offers.',
                offerListLoading: false
            };
        case FETCH_CATEGORY_OFFER_LISTS:
            return {
                ...state,
                catoffListLoading: true
            };
        case FETCH_CATEGORY_OFFER_LISTS_SUCCESS:
            let currPage = action.payload.current_page;
            if (currPage === 1) {
                return {
                    ...state,
                    catofferLists: action.payload.data,
                    catoffListLoading: false,
                    catLastPage: action.payload.last_page,
                };
            } else {
                return {
                    ...state,
                    refreshList: !state.refreshList,
                    catofferLists: [...state.catofferLists, ...action.payload.data]
                }
            }
        case FETCH_CATEGORY_OFFER_LISTS_ERROR:
            return {
                ...state,
                catoffListLoading: true
            };
        case OFFER_LIKE_BY_USER_SUCCESS:
            return {
                ...state,
                likeStatus: action.payload.message,
            };
        case FETCH_USER_FAVOURITE_OFFER_LIST:
            return {
                ...state,
                favOffListLoading: true
            };
        case FETCH_USER_FAVOURITE_OFFER_LIST_SUCCESS:
            return {
                ...state,
                favOffers: action.payload,
                favOffListLoading: false
            };
        case FETCH_USER_FAVOURITE_OFFER_LIST_ERROR:
            return {
                ...state,
                favOffListLoading: true
            };
        case ADD_LIKE_PREFERENCE_OF_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                offers: transformOfferListAfterLike(state.offers, action.payload),

            };
        case ADD_LIKE_PREFERENCE_OF_CAT_OFFERS: {
            return {
                ...state,
                refreshList: !state.refreshList,
                catofferLists: transformCatListAfterLike(state.catofferLists, action.payload)
            };
        }
        case ADD_LIKE_PREFERENCE_OF_BRAND_OFFERS: {
            return {
                ...state,
                refreshList: !state.refreshList,
                brandOfferList: transformBrandOfferListAfterLike(state.brandOfferList, action.payload)
            };
        }
        case REMOVE_LIKE_PREFERENCE_OF_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                offers: transformOfferListAfterRemoveLike(state.offers, action.payload),


            };
        case REMOVE_LIKE_PREFERENCE_OF_CAT_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                catofferLists: transformCatListAfterRemoveLike(state.catofferLists, action.payload)

            };
        case REMOVE_LIKE_PREFERENCE_OF_BRAND_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                brandOfferList: transformBrandOfferListAfterRemoveLike(state.brandOfferList, action.payload)

            };
        case OFFER_DISLIKE_BY_USER_SUCCESS:
            return {
                ...state,
                dislikeStatus: action.payload.message
            };
        case ADD_FAVOURITE_PREFERENCE_OF_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                offers: transformOfferListAfterFavourite(state.offers, action.payload),
            };
        case ADD_FAVOURITE_PREFERENCE_OF_CAT_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                catofferLists: transformCatListAfterFavourite(state.catofferLists, action.payload),
            };
        case ADD_FAVOURITE_PREFERENCE_OF_BRAND_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                brandOfferList: transformBrandOfferListAfterFavourite(state.brandOfferList, action.payload),
            };
        case REMOVE_FAVOURITE_PREFERENCE_OF_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                favOffers: transformFavListAfterRemovefav(state.favOffers, action.payload),
                offers: transformOfferListAfterRemoveFavourite(state.offers, action.payload),
            };
        case REMOVE_FAVOURITE_PREFERENCE_OF_CAT_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                favOffers: transformFavListAfterRemovefav(state.favOffers, action.payload),
                catofferLists: transformcatListAfterRemoveFavourite(state.catofferLists, action.payload),
            };
        case REMOVE_FAVOURITE_PREFERENCE_OF_BRAND_OFFERS:
            return {
                ...state,
                refreshList: !state.refreshList,
                favOffers: transformFavListAfterRemovefav(state.favOffers, action.payload),
                brandOfferList: transformBrandOfferListAfterRemoveFavourite(state.brandOfferList, action.payload),
            };
        case FETCH_BRAND_DETAIL:
            return {
                ...state,
                brandDetailLoading: true
            };
        case FETCH_BRAND_DETAIL_SUCCESS:
            return {
                ...state,
                brandInfo: action.payload,
                brandDetailLoading: false
            };
        case FETCH_BRAND_DETAIL_ERROR:
            return {
                ...state,
                brandDetailLoading: true
            };
        case FETCH_BRAND_OFFER_LISTS:
            return {
                ...state,
                brandListLoading: true
            }
        case FETCH_BRAND_OFFER_LISTS_SUCCESS:
            return {
                ...state,
                brandOfferList: action.payload,
                brandListLoading: false
            }
        case FETCH_BRAND_OFFER_LISTS_ERROR:
            return {
                ...state,
                brandListLoading: true
            }
        case FOLLOW_BRAND_OF_OFFERS_SUCCESS:
            return {
                ...state,
                followStatus: action.payload.message
            }
        case ADD_BRAND_FOLLOWED_BY_USER:
            return {
                ...state,
                brandInfo: transformBrandAfterFollow(state.brandInfo, action.payload),
            };
        case UNFOLLOW_BRAND_OF_OFFERS_SUCCESS:
            return {
                ...state,
                unFollowStatus: action.payload.message
            }
        case REMOVE_BRAND_FOLLOWED_BY_USER:
            return {
                ...state,
                brandInfo: transformBrandAfterUnFollow(state.brandInfo, action.payload),
            }

        case SEND_ENQUIRY_MESSAGE:
            return {
                ...state,
                error: null,
                success: null
            }

        case SEND_ENQUIRY_MESSAGE_SUCCESS:
            return {
                ...state,
                success: action.payload.message
            }

        case SEND_ENQUIRY_MESSAGE_ERROR:
            return {
                ...state,
                error: action.payload ? action.payload : action.payload.message
            }

        case RESET_SUCCESS_ERROR_MESSAGE:
            return {
                ...state,
                error: null,
                success: null
            }

        case SEARCH_OFFER_BY_TEXT:
            return {
                ...state,
                offers: null,
                //isSearching: true
            }

        case SEARCH_OFFER_BY_TEXT_SUCCESS:
            return {
                ...state,
                offers: action.payload.data,
                // searchList: action.payload.data,
                //  isSearching: false,

            }
        case SEARCH_OFFER_BY_TEXT_ERROR:
            return {
                ...state,
                error: action.payload ? action.payload : 'Error on fetching Offers.',
                // isSearching: false,

            }

        default:
            return state;
    }
};
const transformOfferListAfterLike = (offerList, offerIdToLike) => {
    if (offerIdToLike) {
        let objIndex = offerList.findIndex((obj => obj.id === offerIdToLike));

        if (objIndex != null) {
            let offerObjIndex = offerList[objIndex];
            offerObjIndex.liked_status = true;
            offerObjIndex.likes_count += 1;
        }
        return offerList;

    }
    return offerList;

}

const transformCatListAfterLike = (catOfferList, catIdToLike) => {
    if (catIdToLike) {
        let objIndex = catOfferList.findIndex((obj => obj.id === catIdToLike));

        if (objIndex != null) {
            let offerObjIndex = catOfferList[objIndex];
            offerObjIndex.liked_status = true;
            offerObjIndex.likes_count += 1;
        }

        return catOfferList;

    }
    return catOfferList;
}

const transformBrandOfferListAfterLike = (brandOfferList, brandOfferIdToLike) => {
    if (brandOfferIdToLike) {
        let objIndex = brandOfferList.data.findIndex((obj => obj.id === brandOfferIdToLike));

        if (objIndex != null) {
            let offerObjIndex = brandOfferList.data[objIndex];
            offerObjIndex.liked_status = true;
            offerObjIndex.likes_count += 1;
        }

        return brandOfferList;

    }
    return brandOfferList;
}

const transformOfferListAfterRemoveLike = (offerList, offerIdToRemoveLike) => {
    if (offerIdToRemoveLike) {
        let objIndex = offerList.findIndex((obj => obj.id === offerIdToRemoveLike));
        if (objIndex != null) {
            let offerObjIndex = offerList[objIndex];
            offerObjIndex.liked_status = false;
            offerObjIndex.likes_count -= 1;
        }

        return offerList;

    }
    return offerList;

}

const transformCatListAfterRemoveLike = (catOfferList, catIdToLike) => {
    if (catIdToLike) {
        let objIndex = catOfferList.findIndex((obj => obj.id === catIdToLike));

        if (objIndex != null) {
            let offerObjIndex = catOfferList[objIndex];
            offerObjIndex.liked_status = false;
            offerObjIndex.likes_count -= 1;
        }

        return catOfferList;

    }
    return catOfferList;
}


const transformBrandOfferListAfterRemoveLike = (brandOfferList, brandOfferIdToLike) => {
    if (brandOfferIdToLike) {
        let objIndex = brandOfferList.data.findIndex((obj => obj.id === brandOfferIdToLike));

        if (objIndex != null) {
            let offerObjIndex = brandOfferList.data[objIndex];
            offerObjIndex.liked_status = false;
            offerObjIndex.likes_count -= 1;
        }

        return brandOfferList;

    }
    return brandOfferList;
}

const transformOfferListAfterFavourite = (offerList, FavofferId) => {
    if (FavofferId) {
        let objIndex = offerList.findIndex((obj => obj.id === FavofferId));
        if (objIndex != null) {
            let offerObjIndex = offerList[objIndex];
            offerObjIndex.favorite_status = true;
        }
        return offerList;

    }
    return offerList;

}
const transformCatListAfterFavourite = (catOfferList, FavCatOfferId) => {
    if (FavCatOfferId) {
        let objIndex = catOfferList.findIndex((obj => obj.id === FavCatOfferId));
        if (objIndex != null) {
            let offerObjIndex = catOfferList[objIndex];
            offerObjIndex.favorite_status = true;
        }
        return catOfferList;

    }
    return catOfferList;

};

const transformBrandOfferListAfterFavourite = (brandOfferList, FavBrandOfferId) => {
    if (FavBrandOfferId) {
        let objIndex = brandOfferList.data.findIndex((obj => obj.id === FavBrandOfferId));
        if (objIndex != null) {
            let offerObjIndex = brandOfferList.data[objIndex];
            offerObjIndex.favorite_status = true;
        }
        return brandOfferList;
    }
    return brandOfferList;
};

const transformOfferListAfterRemoveFavourite = (offerList, UnFavofferId) => {
    if (UnFavofferId) {
        let objIndex = offerList.findIndex((obj => obj.id === UnFavofferId));
        if (objIndex != null) {
            let offerObjIndex = offerList[objIndex];
            offerObjIndex.favorite_status = false;
        }
        return offerList;

    }
    return offerList;

}

const transformcatListAfterRemoveFavourite = (catOfferList, UnFavCatOfferId) => {
    if (UnFavCatOfferId) {
        let objIndex = catOfferList.findIndex((obj => obj.id === UnFavCatOfferId));
        if (objIndex != null) {
            let offerObjIndex = catOfferList[objIndex];
            offerObjIndex.favorite_status = false;
        }
        return catOfferList;

    }
    return catOfferList;

}

const transformBrandOfferListAfterRemoveFavourite = (brandOfferList, UnFavBrandOfferId) => {
    if (UnFavBrandOfferId) {
        let objIndex = brandOfferList.data.findIndex((obj => obj.id === UnFavBrandOfferId));
        if (objIndex != null) {
            let offerObjIndex = brandOfferList.data[objIndex];
            offerObjIndex.favorite_status = false;
        }
        return brandOfferList;

    }
    return brandOfferList;

}

const transformFavListAfterRemovefav = (favList, offerIdToRemove) => {
    console.log("favList", favList);
    if (offerIdToRemove) {
        let objIndex = favList.data.findIndex((obj => obj.id === offerIdToRemove));
        if (objIndex != null) {
            favList.data.splice(objIndex, 1);
            favList.total -= 1;
        }
        return favList;

    }
    return favList;

}

const transformBrandAfterFollow = (brandInfo, brandIdToFollow) => {
    if (brandInfo.id === brandIdToFollow) {
        brandInfo.followed_status = true;
    }
    return brandInfo;

}

const transformBrandAfterUnFollow = (brandInfo, brandIdToFollow) => {
    if (brandInfo.id === brandIdToFollow) {
        brandInfo.followed_status = false;
    }
    return brandInfo;

}

export default DashboardReducer;