import {
    AUTH_SET_TOKEN,
    AUTH_SET_ACCESS_TOKEN,
    SET_DEVICE_TOKEN,
    CLEAR_AUTH,
    REGISTER_BY_EMAIL,
    REGISTER_BY_EMAIL_ERROR,
    REGISTER_BY_EMAIL_SUCCESS,
    SIGNIN_BY_EMAIL,
    SIGNIN_BY_EMAIL_ERROR,
    SIGNIN_BY_EMAIL_SUCCESS,
    SIGNIN_BY_SOCIAL_LOGIN,
    SIGNIN_BY_SOCIAL_LOGIN_SUCCESS,
    SIGNIN_BY_SOCIAL_LOGIN_ERROR,
    SET_LIST_OF_NEARBY_LOCATION,
    RESET_ERROR_SUCCESS_MSG,
    SEND_EMAIL_VERIFICATION,
    SEND_EMAIL_VERIFICATION_SUCCESS,
    SEND_EMAIL_VERIFICATION_ERROR,

} from '../actions/actionTypes';

const INITIAL_STATE = {
    aToken: null,
    rToken: null,
    deviceToken: null,
    user: null,
    error: null,
    success: null,
    loading: false,
    nearbylocationData: null,
    noNetConnection: false,
};

const AuthReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case AUTH_SET_TOKEN:
            return {
                ...state,
                aToken: action.aToken,
                rToken: action.rToken,
            };
        case AUTH_SET_ACCESS_TOKEN:
            return {
                ...state,
                aToken: action.payload,
            };
        case SET_DEVICE_TOKEN:
            return {
                ...state,
                deviceToken: action.payload,
            };
        case CLEAR_AUTH:
            return {
                ...state,
                ...INITIAL_STATE,
            };
        case RESET_ERROR_SUCCESS_MSG: {
            return {
                ...state,
                error: null,
                success: null,
            }
        }
        case REGISTER_BY_EMAIL:
            return {
                ...state,
                error: null,
                success: null,
                loading: true,
            }
        case REGISTER_BY_EMAIL_ERROR:
            return {
                ...state,
                error: action.payload,
                success: null,
                loading: false,
            }
        case REGISTER_BY_EMAIL_SUCCESS:
            return {
                ...state,
                success: action.payload.message,
                error: null,
                loading: false,
            }
        case SIGNIN_BY_EMAIL:
            return {
                ...state,
                error: null,
                success: null,
                loading: true,
            }
        case SIGNIN_BY_EMAIL_ERROR:
            return {
                ...state,
                error: action.payload ? action.payload : 'Password doesnot match',
                success: null,
                loading: false,
            }
        case SIGNIN_BY_EMAIL_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
            }
        case SIGNIN_BY_SOCIAL_LOGIN:
            return {
                ...state,
                error: null,
                success: null,
                loading: true,
            }
        case SIGNIN_BY_SOCIAL_LOGIN_ERROR:
            return {
                ...state,
                error: action.payload,
                success: null,
                loading: false,
            }
        case SIGNIN_BY_SOCIAL_LOGIN_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
            }
        case SEND_EMAIL_VERIFICATION:
            return {
                ...state,
                error: null,
                success: null,
                loading: true,
            }
        case SEND_EMAIL_VERIFICATION_ERROR:
            return {
                ...state,
                error: action.payload,
                success: null,
                loading: false,
            }
        case SEND_EMAIL_VERIFICATION_SUCCESS:
            return {
                ...state,
                success: action.payload,
                error: null,
                loading: false,
            }

        case SET_LIST_OF_NEARBY_LOCATION:
            return {
                ...state,
                nearbylocationData: action.payload,
            }
        default:
            return state;
    }
};
export default AuthReducer;
