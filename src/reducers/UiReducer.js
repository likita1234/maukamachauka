import { NO_INTERNET_CONNECTION, INTERNET_CONNECTION_RESTORED } from '../actions/actionTypes';

const initialState = {
    noNetConnection: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {

        case NO_INTERNET_CONNECTION:
            return {
                ...state,
                noNetConnection: true,
            };
        case INTERNET_CONNECTION_RESTORED:
            return {
                ...state,
                noNetConnection: false,
            };
        default:
            return state;
    }
};

export default reducer;
