/**
 * @providesModule Spacing
 */
import { StyleSheet, Platform } from 'react-native';

import colors from './colors';
import constants from './constants.js';

let defaultSpacer = constants.defaultSpacer;
let positions = ['Top', 'Right', 'Left', 'Bottom'];
const spacing = StyleSheet.create({
    container: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        padding: constants.viewPadding,
        backgroundColor: colors.white,
        justifyContent: 'flex-start',
        height: constants.viewHeight,
    },
    scrollContainer: {
        backgroundColor: colors.white,
        flex: 0,
        minHeight: constants.viewHeight,
    },
    viewPadding: {
        padding: constants.viewPadding,
    },
    viewPaddingHorizontal: {
        paddingHorizontal: constants.viewPadding,
    },
    contentCover: {
        flex: 0,
    },
    viewPaddingVertical: {
        paddingVertical: constants.viewPadding,
    },
    viewCover: {
        flex: 1,
    },
    contentCenter: {
        justifyContent: 'center',
        alignItems: 'center',
    },
});
positions.forEach(position => {
    var paddingString = 'padding' + position;
    var marginString = 'margin' + position;
    spacing['addPadding' + position] = {
        [paddingString]: defaultSpacer,
    };
    spacing['addMargin' + position] = {
        [marginString]: defaultSpacer,
    };
});
export default spacing;
