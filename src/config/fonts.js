/**
 * @providesModule Fonts
 */
const fonts = {
  barlowBold: 'Barlow-Bold',
  barlowExtraBold: 'Barlow-Black',
  barlowLight: 'Barlow-Light',
  barlowMedium: 'Barlow-Medium',
  barlowRegular: 'Barlow-Regular',
  barlowSemiBold: 'Barlow-SemiBold',
  barlowThin: 'Barlow-Thin',
};
export default fonts;
