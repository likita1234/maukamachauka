/**
 * @providesModule PcFunctions
 */
import Toast from 'react-native-tiny-toast';
import Moment from 'moment';

export const showErrToast = {
    showToast: (message, duration = 5000) => {
        Toast.show(message, {
            duration,
            textStyle: { textAlign: 'center' },
            containerStyle: {
                backgroundColor: 'rgba(255, 0, 0, 0.75)',
                paddingHorizontal: 10,
            },
            position: Toast.position.bottom,
        });
    },
};
export const showSuccessToast = {
    showToast: (message, duration = 5000) => {
        Toast.show(message, {
            duration,
            textStyle: { textAlign: 'center' },
            containerStyle: {
                backgroundColor: 'green',
                paddingHorizontal: 10,
            },
            position: Toast.position.bottom,
        });
    },
};

export const formatDate = date => {
    return Moment(date).format('Do MMM, YYYY');
};