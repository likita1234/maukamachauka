/**
 * @providesModule TextStyles
 */
import { StyleSheet } from 'react-native';
import fonts from './fonts';

const largeText = 16;
const mediumText = 14;
const smallText = 12;
const semiboldText = fonts.barlowSemiBold;
const extraboldText = fonts.barlowExtraBold;
const boldText = fonts.barlowBold;
const regularText = fonts.barlowRegular;
const lightText = fonts.barlowLight;
const thinText = fonts.barlowThin;

const textStyles = StyleSheet.create({
  largeText: {
    fontFamily: regularText,
    fontSize: largeText,
  },
  largeTextSemibold: {
    fontFamily: semiboldText,
    fontSize: largeText,
  },
  largeTextBold: {
    fontFamily: boldText,
    fontSize: largeText,
  },
  largeTextLight: {
    fontFamily: lightText,
    fontSize: largeText,
  },
  largeTextThin: {
    fontFamily: thinText,
    fontSize: largeText,
  },

  mediumText: {
    fontFamily: regularText,
    fontSize: mediumText,
  },
  mediumTextSemibold: {
    fontFamily: semiboldText,
    fontSize: mediumText,
  },
  mediumTextBold: {
    fontFamily: boldText,
    fontSize: mediumText,
  },
  mediumTextLight: {
    fontFamily: lightText,
    fontSize: mediumText,
  },
  mediumTextThin: {
    fontFamily: thinText,
    fontSize: mediumText,
  },

  smallText: {
    fontFamily: regularText,
    fontSize: smallText,
  },
  smallTextSemibold: {
    fontFamily: semiboldText,
    fontSize: smallText,
  },
  smallTextBold: {
    fontFamily: boldText,
    fontSize: smallText,
  },
  smallTextLight: {
    fontFamily: lightText,
    fontSize: smallText,
  },
  smallTextThin: {
    fontFamily: thinText,
    fontSize: smallText,
  },
  lightText: {
    fontFamily: lightText,
    fontSize: mediumText,
  },
  lightTextBold: {
    fontFamily: boldText,
    fontSize: mediumText,
  },
});
export default textStyles;
