import React from 'react';
import { TextInput, StyleSheet } from 'react-native';
import fonts from './fonts';

const inputs = React.forwardRef((props, ref) => (
    <TextInput
        underlineColorAndroid="transparent"
        {...props}
        style={[styles.input, props.style]}
        ref={ref}
        allowFontScaling={false}
    />
));
const regularText = fonts.barlowRegular;

const styles = StyleSheet.create({
    input: {
        width: '100%',
        borderWidth: 1,
        borderColor: '#D8D8E1',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 24,
        paddingRight: 24,
        marginTop: 2,
        height: 50,
        marginBottom: 8,
        backgroundColor: 'white',
        color: 'black',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.0,
        elevation: 1,
        fontFamily: regularText,

    },
});

export default inputs;
