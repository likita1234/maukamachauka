/**
 * @providesModule shadow
 */
// import {Dimensions} from 'react-native';

import colors from './colors';

import { StyleSheet } from 'react-native';
const shadow = StyleSheet.create({
    shadowStyle: {
        shadowColor: colors.default,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 4,
    },
});
export default shadow;
