/**
 * @providesModule Colors
 */
const colors = {
  primaryColor: '#E51E2A', // '#4285F4',
  secondaryColor: '#00b2bf',
  accentColor: '#00897b',
  black: '#000000',
  title: '#606060',
  default: '#707070',
  border: '#E5E5E5',
  night: '#333333',
  charcoal: '#474747',
  darkgray: '#d4d4d4',
  gray: '#7D7D7D',
  lightishgray: '#9D9D9D',
  lightgray: '#D6D6D6',
  smoke: '#EEEEEE',
  white: '#FFFFFF',
  ypsDark: '#47546E',
  yps: '#637599',
  ypsLight: '#7B92BF',
  cosmic: '#963D32',
  red: '#FF0000',
  skyBlue: '#00CBFF',
  yellow: '#FFD300',
  lightYellow: '#FFFF00',
  green: '#00b300',
};
export default colors;
