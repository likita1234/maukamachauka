import React from 'react';
import { Text, StyleSheet } from 'react-native';

const CustomText = props => (
    <Text {...props} style={[styles.mainText, props.style]}>
        {props.children}
    </Text>
);

const styles = StyleSheet.create({
    mainText: {
        color: '#616184',
        fontSize: 12,
        backgroundColor: 'transparent',
        //fontFamily: 'Montserrat-Light',
    },
});

export default CustomText;
