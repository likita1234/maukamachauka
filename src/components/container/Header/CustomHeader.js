import React from 'react';
import { View, TouchableOpacity, Image, StyleSheet, Text } from 'react-native';
import constants from '../../../config/constants';
import colors from '../../../config/colors';
import shadow from '../../../config/shadow';
import Icon from 'react-native-vector-icons/Ionicons';

const CustomHeader = ({
    title,
    user,
    navigation
}) => {



    return (
        <View style={styles.offerHeader}>
            <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{ width: 40, height: 50, alignItems: 'center', justifyContent: 'center' }}>
                <Icon
                    name="md-arrow-back"
                    size={28}
                    style={{ marginLeft: 8 }}
                    color={colors.white} />
            </TouchableOpacity>
            <View style={{ alignItems: 'center', width: '90%' }}>
                <Text style={{ textAlign: 'center', fontSize: 14, marginLeft: 20, color: colors.white }}>{title}</Text>
            </View>

        </View>
    );
};

const styles = StyleSheet.create({

    offerHeader: {
        //  justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        padding: constants.viewPadding,
        paddingTop: 10,
        backgroundColor: colors.primaryColor,
        height: 58,
        ...shadow.shadowStyle,
    },


})

export default CustomHeader;
