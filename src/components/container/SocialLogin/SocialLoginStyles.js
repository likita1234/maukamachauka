import { StyleSheet } from 'react-native';
import colors from '../../../config/colors';

export const styles = StyleSheet.create({

    emailSignInForm: {
        marginTop: 40,
        flex: 1,
        //justifyContent: 'center'
    },
    emailBtn: {
        backgroundColor: colors.primaryColor,
        marginTop: 8,
        width: 300,
        height: 36,
        borderRadius: 4
    },
    txtStyle: {
        color: colors.white,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    loginBtn: {
        width: 300,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.primaryColor,
        marginTop: 8,
        borderRadius: 4,
        padding: 0
    },
    linkStyle: {
        textDecorationLine: 'underline',
        fontWeight: 'bold',
        textAlign: 'center',
        color: colors.primaryColor,
        marginLeft: 5
    },
    btmTxtCont: {
        flexDirection: 'row',
        marginTop: 8,
        alignItems: "center",
        justifyContent: 'center'
    },
    forgotPswCont: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15
    }

})