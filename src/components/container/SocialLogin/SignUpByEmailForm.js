import React, { useState } from 'react';
import { View, TouchableOpacity, Text, ActivityIndicator } from 'react-native';
import DefaultInput from '../../../config/inputs';
import CustomText from '../../../config/text';
import SignUpValidate from '../../../utility/validation/SignUpValidate';
import { showErrToast } from '../../../config/functions';
import { styles } from './SocialLoginStyles';

const SignUpByEmailForm = props => {
    const [fname, setFirstName] = useState('');
    const [lname, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [pwd, setPassword] = useState('');

    const { hideShowForms } = props;

    const signUpHandler = () => {
        let validate = SignUpValidate(fname, lname, email, pwd);
        let validError = Object.keys(validate).length;
        if (parseInt(validError)) {
            console.log('Error present --- ');
            console.log(validate);
            let errMsg = validate[Object.keys(validate)[0]];
            showErrToast.showToast(errMsg);
        } else {
            console.log("Successfully validate")
            props.userSignUp(fname, lname, email, pwd);
        }
    };
    const renderSignUpButton = () => {
        if (props.loading) {
            return (
                <View style={[styles.loginBtn, { backgroundColor: '' }]} >
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <TouchableOpacity style={styles.loginBtn} onPress={() => signUpHandler()}>
                <Text style={styles.txtStyle}>Sign up</Text>
            </TouchableOpacity>
        );
    };


    return (
        <View style={[styles.emailSignInForm, { marginTop: 30 }]}>
            <View style={{ flex: 1 }}>
                <DefaultInput placeholder={'First Name'}
                    onChangeText={fname => setFirstName(fname)}
                    onSubmitEditing={() => {
                        this.lastName.focus();
                    }}
                    returnKeyType="next"
                    //autoFocus={true}
                    blurOnSubmit={false} />

                <DefaultInput placeholder={'Last Name'}
                    ref={ref => {
                        lastName = ref;
                    }}
                    onChangeText={lname => setLastName(lname)}
                    onSubmitEditing={() => {
                        this.mail.focus();
                    }}
                    returnKeyType="next"
                    blurOnSubmit={false} />

                <DefaultInput placeholder={'Email'}
                    ref={ref => {
                        mail = ref;
                    }}
                    onChangeText={email => setEmail(email)}
                    onSubmitEditing={() => {
                        this.psw.focus();
                    }}
                    returnKeyType="next"
                    blurOnSubmit={false} />

                <DefaultInput placeholder={'Password'}
                    ref={ref => {
                        psw = ref;
                    }}
                    onChangeText={pwd => setPassword(pwd)}
                    secureTextEntry={true}
                    returnKeyType="go"
                />

                {renderSignUpButton()}
                <View style={styles.btmTxtCont}>
                    <CustomText>Already have account?</CustomText>
                    <TouchableOpacity onPress={() => hideShowForms(true, false)}>
                        <CustomText style={styles.linkStyle}>Sign In</CustomText>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
};

export default SignUpByEmailForm;