import React, { useState } from 'react';
import { View, TouchableOpacity, Text, Keyboard, ActivityIndicator } from 'react-native';
import DefaultInput from '../../../config/inputs';
import CustomText from '../../../config/text';
import SignInValidate from '../../../utility/validation/SignInValidate';
import { showErrToast } from '../../../config/functions';
import { styles } from './SocialLoginStyles';

const SignInByEmailForm = props => {
    const [email, setEmail] = useState('');
    const [pwd, setPassword] = useState('');
    const { hideShowForms, navigation } = props;

    const signInHandler = () => {
        Keyboard.dismiss();
        let validate = SignInValidate(email, pwd);
        let validError = Object.keys(validate).length;

        if (parseInt(validError)) {
            console.log('Error present --- ');
            console.log(validate);
            let errMsg = validate[Object.keys(validate)[0]];
            showErrToast.showToast(errMsg);
        } else {
            console.log("Successfully validate")
            props.userSignIn(email, pwd);
        }
    }
    const renderSignInButton = () => {
        if (props.loading) {
            return (
                <View style={[styles.loginBtn, { backgroundColor: '' }]} >
                    <ActivityIndicator />
                </View>
            );
        }

        return (

            <TouchableOpacity style={styles.loginBtn} onPress={() => signInHandler()}>
                <Text style={styles.txtStyle}>LOGIN</Text>
            </TouchableOpacity>

        );
    };

    return (
        <View style={[styles.emailSignInForm]}>
            <View style={{ flex: 1 }}>
                <DefaultInput placeholder={'Email'}
                    onChangeText={email => setEmail(email)}
                    onSubmitEditing={() => {
                        password.focus();
                    }}
                    returnKeyType="next"
                    blurOnSubmit={false} />
                <DefaultInput placeholder={'Password'}
                    ref={ref => {
                        password = ref;
                    }}
                    onChangeText={pwd => setPassword(pwd)}
                    secureTextEntry={true}
                    returnKeyType="go" />
                {renderSignInButton()}
                <View style={styles.forgotPswCont}>
                    <TouchableOpacity onPress={() => navigation.navigate("ForgetPsw")}>
                        <CustomText>Forget password?</CustomText>
                    </TouchableOpacity>

                    <View style={{ flexDirection: 'row' }}>
                        <CustomText>New user?</CustomText>
                        <TouchableOpacity onPress={() => hideShowForms(false, true)}>
                            <CustomText style={styles.linkStyle}>Sign Up</CustomText>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    );
};
export default SignInByEmailForm;