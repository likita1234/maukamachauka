import React from 'react';
import { View, TouchableOpacity, Text, Image, Share } from 'react-native';
import FastImage from 'react-native-fast-image';
import CustomText from '../../../config/text';
import { styles } from './DashboardStyles';
import colors from '../../../config/colors';
import Moment from 'moment';
import Icon from 'react-native-vector-icons/EvilIcons';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import MatIco from 'react-native-vector-icons/Entypo';
import LargeText from '../../presentational/typography/LargeText';
import CustomLink from '../../presentational/typography/link';
import MediumText from '../../presentational/typography/MediumText';
import LightText from '../../presentational/typography/LightText';

const SingleOffer = ({
  offer,
  showOfferDetail,
  onClickPreference,
  snippetTextShown,
  toggleNumOfSnippetLines,
  showBrandDetail,
  onClickFavOffer,
}) => {

  const offerDate = () => {
    let offerExpiryDate = offer.expires_in;
    let remainingDate = new Moment().to(Moment(offerExpiryDate));
    return (
      <View style={styles.offerExpirationBox}>
        <CustomText style={{ fontSize: 10 }}>Expire {remainingDate}</CustomText>
      </View>
    );
  };

  const showDetailScreen = () => {
    const offerDetail = {
      offer,
    };
    showOfferDetail(offerDetail);
  };

  const showOfferBrandDetail = () => {
    let brandID = offer.brand.id;
    showBrandDetail(brandID);
  };

  const offerBrandTitle = () => {
    return (
      <View>
        <LargeText style={styles.offerTitle}>{offer.brand.name}</LargeText>
      </View>
    );
  };

  const renderLikecount = () => {
    if (offer.likes_count > 0) {
      if (offer.likes_count === 1) {
        return (
          <CustomText style={{ fontSize: 10 }}>
            {offer.likes_count} like
          </CustomText>
        );
      }
      return (
        <CustomText style={{ fontSize: 10 }}>
          {offer.likes_count} likes
        </CustomText>
      );
    }
    return null;
  };

  const likePreferenceButton = () => {
    let prefvalOnClick = 'unliked';
    prefvalOnClick = offer.liked_status === true ? 'alreadyliked' : 'unliked';
    return (
      <TouchableOpacity
        onPress={() => {
          onClickPreference(prefvalOnClick, offer.id);
          //color = colors.black ? colors.skyBlue : colors.black
        }}
        style={styles.opinionIcons}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: offer.liked_status === true ? colors.primaryColor : colors.white,
            height: 45,
            width: 45,
            borderWidth: 1,
            borderColor: colors.primaryColor,
            borderRadius: 100,
            justifyContent: 'center',
          }}>
          <FAIcon name="thumbs-up" size={22} solid color={offer.liked_status === true ? colors.white : colors.primaryColor} />
          {/* {renderLikecount()} */}
        </View>
      </TouchableOpacity>
    );
  };

  const addToFavouriteBtn = () => {

    let prefvalOnClick = 'unFav';
    prefvalOnClick = offer.favorite_status === true ? 'alreadyFav' : 'unFav';
    return (
      <TouchableOpacity
        onPress={() => onClickFavOffer(prefvalOnClick, offer.id)}
        style={styles.opinionIcons}>
        <View
          style={{
            alignItems: 'center',
            backgroundColor: offer.favorite_status === true ? colors.primaryColor : colors.white,
            height: 45,
            width: 45,
            borderWidth: 1,
            borderColor: colors.primaryColor,
            borderRadius: 100,
            justifyContent: 'center',
          }}>
          <FAIcon name="heart" size={22} color={offer.favorite_status === true ? colors.white : colors.primaryColor} />
        </View>
      </TouchableOpacity>
    );
  };

  const shareOfferMsg = () => {
    let msg =
      'Hey, check out this offer, see if you like it :-) Offer:\n ' +
      offer.title;

    Share.share({
      message: msg,
    });
  };

  const shareButton = () => {
    return (
      <TouchableOpacity
        onPress={() => shareOfferMsg()}
        style={styles.opinionIcons}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: colors.white,
            height: 45,
            width: 45,
            borderWidth: 1,
            borderColor: colors.primaryColor,
            borderRadius: 100,
            justifyContent: 'center',
          }}>
          <FAIcon name="share-alt" size={22} color={colors.primaryColor} />
        </View>
      </TouchableOpacity>
    );
  };

  const offerInfo = () => {
    return (
      <View style={styles.offerHolder}>
        <View style={styles.offerHeader}>
          <View style={styles.offerBrandDetail}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
              onPress={() => showOfferBrandDetail()}>
              <View style={styles.brandImgholder}>
                <FastImage
                  source={{
                    uri: offer.brand.logo_src,
                    priority: FastImage.priority.high,
                    cache: FastImage.cacheControl.immutable,
                  }}

                  style={styles.brandImgCont}
                />
              </View>
              {offerBrandTitle()}
            </TouchableOpacity>

          </View>

          <View style={styles.offerHeadRight}>
            {offerDate()}
            {/* <View style={styles.offerSettings}>
              <TouchableOpacity style={styles.offerSettingsBtn}>
                <View style={styles.offerSettingsIcon}>
                  <MatIco
                    name="dots-three-horizontal"
                    size={20}
                    style={styles.iconStyle}
                    color={colors.primaryColor}></MatIco>
                </View>
              </TouchableOpacity>
            </View> */}
          </View>
        </View>

        <View style={styles.offerBox}>
          <View style={styles.offerImgHolder}>
            <TouchableOpacity onPress={() => showDetailScreen()} >
              <FastImage
                source={{
                  uri: offer.image_src,
                  priority: FastImage.priority.high,
                }}
                style={styles.imgCont}
              // resizeMode={FastImage.resizeMode.cover}

              />
            </TouchableOpacity>
          </View>

          <View style={styles.offerDetails}>

            <View style={styles.opinionStyle}>
              <View style={{ width: '100%' }}>
                <View style={{ flexDirection: 'row' }}>
                  <LargeText style={styles.brandTxt} bold={true}>
                    {offer.title}
                  </LargeText>
                </View>
              </View>
            </View>


            <View style={styles.offerDescContainer}>
              <MediumText
                style={{ textAlign: 'justify' }}
                numberOfLines={snippetTextShown === offer.id ? undefined : 2}
                style={styles.offerDesc}>
                {offer.description}
              </MediumText>
              {offer.description.length > 120 && (
                <CustomLink onPress={() => toggleNumOfSnippetLines(offer.id)}>
                  {snippetTextShown === offer.id ? '..see less' : '..see more'}
                </CustomLink>
              )}
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: -25,
            }}>
            {likePreferenceButton()}
            {addToFavouriteBtn()}
            {shareButton()}
          </View>
        </View>
      </View>
    );
  };
  return <View style={styles.offerHolder}>{offerInfo()}</View>;
};

export default SingleOffer;
