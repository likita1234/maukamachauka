import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Share } from 'react-native';
import FastImage from 'react-native-fast-image';
import CustomText from '../../../config/text';
import CustomLink from '../../presentational/typography/link';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import MatIco from 'react-native-vector-icons/Entypo';
import LargeText from '../../presentational/typography/LargeText';
import MediumText from '../../presentational/typography/MediumText';
import Moment from 'moment';
import colors from '../../../config/colors';
import { styles } from './DashboardStyles';

const SingleBrandOffer = ({
    brandOffer,
    navigation,
    snippetTextShown,
    onClickPreference,
    onClickFavOffer }) => {

    const offerDate = () => {
        let offerExpiryDate = brandOffer.expires_in;
        let remainingDate = new Moment().to(Moment(offerExpiryDate));
        return (
            <View style={[styles.offerExpirationBox, { backgroundColor: colors.darkgray, marginBottom: 10 }]}>
                <CustomText style={{ fontSize: 10, }}>Expire {remainingDate}</CustomText>
            </View>
        )
    }

    const showDetailScreen = () => {
        navigation.navigate("OfferDetail", { offerDetailInfo: brandOffer, originScreen: 'brand' });
    };

    const likePreferenceButton = () => {
        let prefvalOnClick = 'unliked';
        prefvalOnClick = brandOffer.liked_status === true ? 'alreadyliked' : 'unliked';

        return (
            <TouchableOpacity
                onPress={() => {
                    onClickPreference(prefvalOnClick, brandOffer.id);
                    //color = colors.black ? colors.skyBlue : colors.black
                }}
                style={styles.opinionIcons}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: brandOffer.liked_status === true ? colors.primaryColor : colors.white,
                        height: 45,
                        width: 45,
                        borderWidth: 1,
                        borderColor: colors.primaryColor,
                        borderRadius: 100,
                        justifyContent: 'center',
                    }}>
                    <FAIcon name="thumbs-up" size={22} solid color={brandOffer.liked_status === true ? colors.white : colors.primaryColor} />
                    {/* {renderLikecount()} */}
                </View>
            </TouchableOpacity>
        );
    };

    const addToFavouriteBtn = () => {

        let prefvalOnClick = 'unFav';
        prefvalOnClick = brandOffer.favorite_status === true ? 'alreadyFav' : 'unFav';
        return (
            <TouchableOpacity
                onPress={() => onClickFavOffer(prefvalOnClick, brandOffer.id)}
                style={styles.opinionIcons}>
                <View
                    style={{
                        alignItems: 'center',
                        backgroundColor: brandOffer.favorite_status === true ? colors.primaryColor : colors.white,
                        height: 45,
                        width: 45,
                        borderWidth: 1,
                        borderColor: colors.primaryColor,
                        borderRadius: 100,
                        justifyContent: 'center',
                    }}>
                    <FAIcon name="heart" size={22} color={brandOffer.favorite_status === true ? colors.white : colors.primaryColor} />
                </View>
            </TouchableOpacity>
        );
    };

    const shareOfferMsg = () => {
        let msg =
            'Hey, check out this offer, see if you like it :-) Offer:\n ' +
            brandOffer.title;

        Share.share({
            message: msg,
        });
    };

    const shareButton = () => {
        return (
            <TouchableOpacity
                onPress={() => shareOfferMsg()}
                style={styles.opinionIcons}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: colors.white,
                        height: 45,
                        width: 45,
                        borderWidth: 1,
                        borderColor: colors.primaryColor,
                        borderRadius: 100,
                        justifyContent: 'center',
                    }}>
                    <FAIcon name="share-alt" size={22} color={colors.primaryColor} />
                </View>
            </TouchableOpacity>
        );
    };

    return (
        <View style={[styles.offerHolder, { width: '100%' }]}>
            <View style={[styles.offerBox, { marginBottom: 40 }]}>

                <View style={[styles.offerSettings, { flexDirection: 'row' }]}>
                    {offerDate()}
                    {/* <TouchableOpacity style={styles.offerSettingsBtn}>
                        <View style={styles.offerSettingsIcon}>
                            <MatIco
                                name="dots-three-horizontal"
                                size={20}
                                style={styles.iconStyle}
                                color={colors.primaryColor}></MatIco>
                        </View>
                    </TouchableOpacity> */}
                </View>


                <View style={styles.offerImgHolder}>
                    <TouchableOpacity onPress={() => showDetailScreen()}>
                        <FastImage
                            source={{
                                uri: brandOffer.image_src,
                                priority: FastImage.priority.high,
                            }}
                            style={styles.imgCont}
                            resizeMode={FastImage.resizeMode.cover}

                        />
                    </TouchableOpacity>
                </View>

                <View style={styles.offerDetails}>
                    <View style={styles.opinionStyle}>
                        <View style={{ width: '100%' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <LargeText style={styles.brandTxt} bold={true}>
                                    {brandOffer.title}
                                </LargeText>
                            </View>
                        </View>
                    </View>

                    <View style={styles.offerDescContainer}>
                        <MediumText style={{ textAlign: 'justify' }}
                            numberOfLines={snippetTextShown === brandOffer.id ? undefined : 2}
                            style={styles.offerDesc}>
                            {brandOffer.description}
                        </MediumText>
                        {brandOffer.description.length > 120 && (
                            <CustomLink onPress={() => toggleNumOfSnippetLines(brandOffer.id)}>
                                {snippetTextShown === brandOffer.id ? '..see less' : '..see more'}
                            </CustomLink>
                        )}

                    </View>
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: -25,
                    }}>
                    {likePreferenceButton()}
                    {addToFavouriteBtn()}
                    {shareButton()}
                </View>
            </View>
        </View>

    );
}

export default SingleBrandOffer;