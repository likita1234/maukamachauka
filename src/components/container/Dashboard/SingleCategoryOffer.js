import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Share } from 'react-native';
import FastImage from 'react-native-fast-image';
import Moment from 'moment';
import CustomText from '../../../config/text';
import LargeText from '../../presentational/typography/LargeText';
import CustomLink from '../../presentational/typography/link';
import MediumText from '../../presentational/typography/MediumText';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import MatIco from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/EvilIcons';
import colors from '../../../config/colors';
import { styles } from './DashboardStyles';

const SingleCategoryOffer = ({
    catoffer,
    navigation,
    snippetTextShown,
    toggleNumOfSnippetLines,
    onClickPreference,
    onClickFavOffer,
    showBrandDetail }) => {
    const offerDate = () => {
        let offerExpiryDate = catoffer.expires_in;
        let remainingDate = new Moment().to(Moment(offerExpiryDate));
        return (
            <View style={styles.offerExpirationBox}>
                <CustomText style={{ fontSize: 10 }}>Expire {remainingDate}</CustomText>
            </View>
        )
    };

    const offerBrandTitle = () => {
        return (
            <View>
                <Text style={styles.offerTitle}>{catoffer.brand.name}</Text>
            </View>
        )
    };

    const likePreferenceButton = () => {
        let prefvalOnClick = 'unliked';
        prefvalOnClick = catoffer.liked_status === true ? 'alreadyliked' : 'unliked';
        return (
            <TouchableOpacity
                onPress={() => {
                    onClickPreference(prefvalOnClick, catoffer.id);
                }}
                style={styles.opinionIcons}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: catoffer.liked_status === true ? colors.primaryColor : colors.white,
                        height: 45,
                        width: 45,
                        borderWidth: 1,
                        borderColor: colors.primaryColor,
                        borderRadius: 100,
                        justifyContent: 'center',
                    }}>

                    <FAIcon name="thumbs-up" size={22}
                        solid color={catoffer.liked_status === true ? colors.white : colors.primaryColor} />
                    {/* {renderLikecount()} */}
                </View>

            </TouchableOpacity>
        );
    };

    const addToFavouriteBtn = () => {
        let prefvalOnClick = 'unFav';
        prefvalOnClick = catoffer.favorite_status === true ? 'alreadyFav' : 'unFav';
        return (
            <TouchableOpacity
                onPress={() => onClickFavOffer(prefvalOnClick, catoffer.id)}
                style={styles.opinionIcons}>
                <View style={{
                    alignItems: 'center',
                    backgroundColor: catoffer.favorite_status === true ? colors.primaryColor : colors.white,
                    height: 45,
                    width: 45,
                    borderWidth: 1,
                    borderColor: colors.primaryColor,
                    borderRadius: 100,
                    justifyContent: 'center',
                }}>
                    <FAIcon name="heart" size={22} color={catoffer.favorite_status === true ? colors.white : colors.primaryColor} />

                </View>

            </TouchableOpacity>
        );
    };

    const shareOfferMsg = () => {
        let msg =
            'Hey, check out this offer, see if you like it :-) Offer:\n ' +
            catoffer.title

        Share.share({
            message: msg,
        });
    };

    const shareButton = () => {
        return (
            <TouchableOpacity
                onPress={() => shareOfferMsg()}
                style={styles.opinionIcons}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor: colors.white,
                    height: 45,
                    width: 45,
                    borderWidth: 1,
                    borderColor: colors.primaryColor,
                    borderRadius: 100,
                    justifyContent: 'center',
                }}>
                    <FAIcon name="share-alt" size={22} color={colors.primaryColor} />

                </View>
            </TouchableOpacity>
        );
    };

    const showOfferBrandDetail = () => {
        let brandID = catoffer.brand.id
        showBrandDetail(brandID);
    };

    const showDetailScreen = () => {
        navigation.navigate("OfferDetail", { offerDetailInfo: catoffer, originScreen: 'catOffer' });
    }

    return (

        <View style={styles.offerHolder}>
            <View style={styles.offerHeader}>
                <View style={styles.offerBrandDetail}>
                    <TouchableOpacity
                        style={{ flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => showOfferBrandDetail()}
                    >
                        <View style={styles.brandImgholder}>
                            <FastImage
                                source={{
                                    uri: catoffer.brand.logo_src,
                                    priority: FastImage.priority.high,
                                    cache: FastImage.cacheControl.immutable,
                                }}
                                style={styles.brandImgCont} />
                        </View>
                        {offerBrandTitle()}
                    </TouchableOpacity>
                </View>

                <View style={styles.offerHeadRight}>
                    {offerDate()}
                    <View style={styles.offerSettings}>
                        <TouchableOpacity style={styles.offerSettingsBtn}>
                            <View style={styles.offerSettingsIcon}>
                                <MatIco
                                    name="dots-three-horizontal"
                                    size={20}
                                    style={styles.iconStyle}
                                    color={colors.primaryColor}></MatIco>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

            <View style={styles.offerBox}>
                <View style={styles.offerImgHolder}>
                    <TouchableOpacity onPress={() => showDetailScreen()}>
                        <FastImage
                            source={{
                                uri: catoffer.image_src,
                                priority: FastImage.priority.high,
                            }}
                            style={styles.imgCont}
                            resizeMode={FastImage.resizeMode.cover}
                        />
                    </TouchableOpacity>
                </View>

                <View style={styles.offerDetails}>

                    <View style={styles.opinionStyle}>
                        <View style={{ width: '100%' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <LargeText style={styles.brandTxt} bold={true}>
                                    {catoffer.title}
                                </LargeText>
                            </View>
                        </View>
                    </View>


                    <View style={styles.offerDescContainer}>
                        <MediumText
                            style={{ textAlign: 'justify' }}
                            numberOfLines={snippetTextShown === catoffer.id ? undefined : 2}
                            style={styles.offerDesc}>
                            {catoffer.description}
                        </MediumText>
                        {catoffer.description.length > 120 && (
                            <CustomLink onPress={() => toggleNumOfSnippetLines(catoffer.id)}>
                                {snippetTextShown === catoffer.id ? '..see less' : '..see more'}
                            </CustomLink>
                        )}
                    </View>
                </View>

                <View style={{
                    flexDirection: 'row',
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: -25,
                }}>
                    {likePreferenceButton()}
                    {addToFavouriteBtn()}
                    {shareButton()}
                </View>
            </View>
        </View>
    );
}

export default SingleCategoryOffer;