import React from 'react';
import { View, TouchableOpacity, Image, StyleSheet, TextInput, Text } from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';
import colors from '../../../config/colors';
import IconOct from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Close from 'react-native-vector-icons/MaterialIcons'
import LargeText from '../../presentational/typography/LargeText';

const DashboardHeader = ({
  headerLayout,
  user,
  navigation,
  searchText,
  onSearchTextChange,
  filterBySearchText,
  onResetSearchForm,
}) => {
  const userAvatar = () => {

    if (user && user.user_avatar) {
      return (
        <Image source={{ uri: user.user_avatar }} style={styles.avatarCont} />
      );
    }
    return (
      <Icon
        name="user"
        style={[styles.avatarCont, { borderColor: 'transparent' }]}
        size={20}
        color={colors.white}
      />
    );
  };

  return (
    <View style={styles.offerHeader} onLayout={headerLayout}>
      <View style={styles.searchBox}>
        <View
          style={[styles.searchBtn, styles.btnStyle, { justifyContent: 'space-between' }]}
        >
          <View style={{ flexDirection: 'row' }}>
            <IconOct
              style={styles.searchIcon}
              name="search"
              size={18}
              color={'#575757'}
              style={{ marginTop: 8 }}
            />
            <TextInput
              value={
                searchText
              }
              placeholderTextColor="#575757"
              onChangeText={val => onSearchTextChange(val)}
              returnKeyLabel="Search"
              returnKeyType="search"
              placeholder="Search"
              style={{ fontSize: 12, width: '90%' }}
              onSubmitEditing={() => filterBySearchText()}
            />

          </View>
          <View>
            <TouchableOpacity style={{ marginRight: 10 }} onPress={() => onResetSearchForm()}>
              <Close name="close" size={18} color="#575757" />
            </TouchableOpacity>
          </View>


        </View>
      </View>




      <View style={styles.notificationBox}>
        <TouchableOpacity onPress={() => navigation.navigate("Notification")}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 10,
              position: 'relative',
              marginRight: 10

            }}>
            <IconOct
              name="bell"
              style={[styles.avatarCont, { borderColor: 'transparent' }]}
              size={25}
              color={colors.white}></IconOct>

            <View style={styles.countBox}>
              <LargeText bold={true} style={styles.notificationCount}>
                20
              </LargeText>
            </View>
          </View>
        </TouchableOpacity>
      </View>
      {/* 
    // {/* <View>
    //   <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
    //     {userAvatar()}
    //   </TouchableOpacity>
    // </View> */}
    </View >
  );
};

const styles = StyleSheet.create({
  offerHeader: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    // flex: 1,
    width: '100%',
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: colors.primaryColor,
    // ...shadow.shadowStyle,
  },
  notificationCount: {
    fontSize: 10,
    marginTop: -2,
    color: colors.primaryColor,
  },
  countBox: {
    position: 'absolute',
    backgroundColor: colors.white,
    height: 25,
    minWidth: 20,
    borderWidth: 2,
    top: -5,
    right: -5,
    borderColor: colors.primaryColor,
    paddingHorizontal: 5,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchIcon: {
    marginRight: 6,
    marginTop: 6,
  },
  searchBtn: {
    flexDirection: 'row',
    borderRadius: 3,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: colors.white,
    marginLeft: 10,
  },
  btnStyle: {
    width: '100%',
    height: 38,
  },
  avatarCont: {
    width: 38,
    height: 38,
    borderRadius: 80,
    borderWidth: 2,
    borderColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchBox: {
    width: '82%',
    alignSelf: 'center',
  },
  notificationBox: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    maxHeight: 40,
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1,
  },
  descriptionContainer: {
    // `backgroundColor` needs to be set otherwise the
    // autocomplete input will disappear on text input.
    backgroundColor: '#F5FCFF',
    marginTop: 25
  },
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
    paddingTop: 25
  },
  itemText: {
    fontSize: 15,
    margin: 2
  },
  autoList: {
    position: 'relative',
    top: -20,
    paddingTop: Platform.OS === 'ios' ? 20 : 40,
    zIndex: 8,
    maxHeight: 160,
    backgroundColor: '#fff',
    width: '100%',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
  },
});

export default DashboardHeader;
