import React from 'react';
import { View, Text, FlatList, ActivityIndicator } from 'react-native';
import SingleOffer from './SingleOffer';
import SkeletonScreen from '../../presentational/skeleton/SkeletonScreen';
import spacing from '../../../config/spacing'
import { styles } from './DashboardStyles';
const OfferList = ({
    renderHeaderAndCatList,
    offers,
    offerListLoading,
    offListRefreshing,
    onRefreshOffList,
    loadMoreOffers,
    showOfferDetail,
    onClickPreference,
    snippetTextShown,
    toggleNumOfSnippetLines,
    showBrandDetail,
    onClickFavOffer,
    renderLoadMoreLoader,
    offerListEmpty,
    refreshList,
}) => {
    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 40
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom
    };

    const offerData = () => {

        if (Array.isArray(offers)) {
            return (
                <FlatList
                    //  ListHeaderComponent={renderHeaderAndCatList}
                    showsVerticalScrollIndicator={false}
                    data={offers}
                    renderItem={({ item }) => (
                        <SingleOffer
                            offer={item}
                            showOfferDetail={showOfferDetail}
                            onClickPreference={onClickPreference}
                            snippetTextShown={snippetTextShown}
                            toggleNumOfSnippetLines={toggleNumOfSnippetLines}
                            showBrandDetail={showBrandDetail}
                            onClickFavOffer={onClickFavOffer}
                        />
                    )}
                    keyExtractor={(item, index) => index.toString()}//(index.toString())

                    contentContainerStyle={
                        [
                            styles.flatListCont
                        ]}
                    //  onRefresh={() => onRefreshOffList()}
                    //  refreshing={offListRefreshing}
                    onMomentumScrollEnd={({ nativeEvent }) => {
                        //console.log(nativeEvent);
                        if (isCloseToBottom(nativeEvent)) {
                            console.log('LOAD more --- ');
                            loadMoreOffers();
                        }
                    }}
                    onEndReachedThreshold={0}
                    ListFooterComponent={() => renderLoadMoreLoader()}
                    extraData={refreshList}
                    ListEmptyComponent={() => offerListEmpty()}
                />
            )

        } else {
            // return <View style={styles.loader}><ActivityIndicator /></View>
            return <View style={{ marginTop: 20 }}><SkeletonScreen /></View>
        }
    }
    return (
        <View >
            {/* <Text style={[styles.headingTxt, { marginBottom: 0 }]}>Featured Offers</Text> */}
            {offerData()}
        </View>);
}

export default OfferList;