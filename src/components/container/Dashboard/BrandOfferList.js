import React, { Component } from 'react';
import { View, Text, ActivityIndicator, FlatList, TouchableOpacity } from 'react-native';
import { styles } from './DashboardStyles';
import SingleBrandOffer from './SingleBrandOffer';

const BrandOfferList = ({ renderBrandData, brandOfferList, brandListLoading, navigation, onClickPreference, refreshList, onClickFavOffer, brandListEmpty }) => {
    if (Array.isArray(brandOfferList)) {
        return (
            <View style={styles.flatListCont}>
                <FlatList
                    ListHeaderComponent={renderBrandData}
                    data={brandOfferList}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => (
                        <SingleBrandOffer
                            brandOffer={item}
                            navigation={navigation}
                            onClickPreference={onClickPreference}
                            onClickFavOffer={onClickFavOffer}
                        />
                    )}
                    keyExtractor={(item, index) => (item.id.toString())}
                    extraData={refreshList}
                    ListEmptyComponent={() => brandListEmpty()}
                // onRefresh={() => onRefreshFavList()}
                // refreshing={favListRefreshing}

                />

            </View>
        );

    } else {
        return <ActivityIndicator />
    }
}

export default BrandOfferList;