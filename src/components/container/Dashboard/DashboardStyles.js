import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../../config/colors';

export const styles = StyleSheet.create({
  offerList: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'relative',
  },
  offerHolder: {
    width: '95%',
    marginBottom: 10,
    //marginTop: 6,
  },
  offerImgHolder: {
    width: Dimensions.get('window').width - 20,
    height: 200,
    overflow: 'hidden',
    backgroundColor: colors.white,
  },
  descCont: {
    width: '60%',
    paddingTop: 10,
    padding: 8,
  },
  descHead: {
    fontWeight: 'bold',
    marginBottom: 6,
    fontSize: 16,
  },
  descBody: {
    textAlign: 'justify',
    fontSize: 12,
    marginBottom: 6,
  },
  imgCont: {
    width: '100%',
    height: '100%',
    marginBottom: 8,
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,

  },
  brandImgCont: {
    width: 53,
    height: 53,
    borderRadius: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 3,
    borderColor: colors.white,
  },
  brandImgholder: {
    borderWidth: 2,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
    height: 60,
    borderRadius: 100,
    borderColor: colors.primaryColor,
  },
  loader: {
    //alignItems: "center",
    justifyContent: 'center',
    flex: 1,
  },
  offerTitle: {
    fontWeight: 'bold',
    fontSize: 17,
  },
  brandInfo: {
    flexDirection: 'row',
    width: '100%',
    //height: '100%',
  },
  offBtmCont: {
    marginLeft: 20,
    // marginTop: 8
  },
  flatListCont: {
    width: '100%',
    //height: '100%',
    alignItems: 'center',
    flex: 1,
  },
  opinionIcon: {
    marginLeft: 10,
    marginTop: 20,
  },
  opinionList: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingVertical: 10,
  },
  opinionStyle: {
    flexDirection: 'row',
    marginBottom: 10,


  },
  offerDescContainer: {
    // marginBottom: 10,
  },
  offerDesc: {
    color: '#575757',
    lineHeight: 16,
    fontSize: 14,
  },

  btnStyle: {
    width: 160,
    height: 36,
    backgroundColor: colors.primaryColor,
    marginTop: 8,
    borderRadius: 4,
    marginTop: 10,
  },
  brandTxt: {
    fontSize: 16,
    textAlign: 'center',
    color: colors.primaryColor,
  },
  txtStyle: {
    color: '#fff',
    fontWeight: 'bold',
    marginTop: 4,
    textAlign: 'center',
  },
  iconContainer: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 8,
    position: 'absolute',
    top: 10,
  },
  offerHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  offerBrandDetail: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 2,
  },
  offerSettings: {
    flex: 0.9,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  offerSettingsIcon: {
    position: 'relative',
    height: 50,
    width: 40,
    justifyContent: 'center',
    alignItems: 'flex-end',

  },
  offerSettingsBtn: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  offerBox: {
    //width: '100%',
    // height: 200
    width: Dimensions.get('window').width - 20,
  },
  offerDetails: {
    borderWidth: 2,
    marginTop: 0,
    borderTopWidth: 0,
    padding: 15,
    borderColor: colors.border,
    paddingBottom: 50,
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
  },
  offerExpirationBox: {
    backgroundColor: '#F1F1F1',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,

  },
  offerHeadRight: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1.4,
    marginTop: 50

  },
  opinionIcons: {
    marginRight: 10,
  },
  categoryImg: {
    width: 50,
    height: 50,
    padding: 6,
    color: colors.red,
    overflow: 'hidden',
    // borderColor: colors.primaryColor,
    // borderWidth: 1,
    // borderRadius: 220,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: colors.primaryColor,

  },
  categoryIcons: {
    marginTop: 10,
  },
  categoryList: {
    width: 60,
    marginRight: 15,
    marginLeft: 6,
    alignItems: 'center',
    justifyContent: 'center',
    height: 90,

  },
  categoryImgCont: {
    // justifyContent: 'flex-end',
    //   height: 60,
    // paddingBottom: 3,
  },
  categoryTxt: {
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 28 : 0,
    fontSize: 10,
    fontWeight: 'bold',
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
    //color: '#4e4e4e',
    marginTop: 10,
    color: colors.primaryColor
  },

  activeCategory: {
    borderColor: colors.primaryColor,
    borderWidth: 3,
    borderRadius: 8,
    borderTopLeftRadius: 8
  },
  enquiryBtn: {
    backgroundColor: colors.primaryColor,
    borderRadius: 6,
    width: 110,
    height: 24,
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginBottom: 10,
    padding: 4
  },
  enquiryBtnTxt: {
    textAlign: 'center',
    padding: 4,
    fontSize: 11,
    fontWeight: 'bold',
    color: colors.white
  },

  mapViewHolder: {
    height: 200

  },
  mapContainer: {
    height: '100%',
  },
  offerDetailFooter: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: '70%',
    position: 'absolute',
    bottom: 20,
    opacity: 0,
  },
  headingTxt: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  tagContainer: {
    marginTop: 10,
    width: '42%',
    borderColor: colors.darkgray,
    borderWidth: 1,
    flexDirection: 'row',
    height: 32,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    borderRadius: 6
  }
});
