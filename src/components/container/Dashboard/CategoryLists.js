import React from 'react';
import { View, Text, FlatList, ImageBackground, ActivityIndicator, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { styles } from './DashboardStyles';
import colors from '../../../config/colors';
import FastImage from 'react-native-fast-image';
import { SvgUri, SvgCssUri } from 'react-native-svg';

const CategoryList = ({
    categoriesList,
    fetchCategoryOffers,
    activeCatId,
    navigation

}) => {

    const categoryData = () => {
        if (Array.isArray(categoriesList) && categoriesList.length) {
            //    const catinfoIndex = categoriesList.map(function (obj) { return obj.id; }).indexOf(catId);
            //   console.log("catinfoIndex", catinfoIndex);

            return (
                <FlatList
                    horizontal
                    data={categoriesList}
                    showsHorizontalScrollIndicator={false}
                    style={styles.categoryIcons}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ width: 72, alignItems: 'center' }} onPress={() => navigation.navigate("CatOffer", { catId: item.id, catName: item.name })} //fetchCategoryOffers(item.id)}
                        >
                            <View style={styles.categoryList}>

                                <View style={styles.categoryImg}>
                                    <SvgUri
                                        uri={item.image_src}
                                        width="100%"
                                        height="100%"
                                        fill={colors.primaryColor}
                                    />
                                </View>

                                <View style={styles.categoryImgCont}>
                                    <Text style={styles.categoryTxt}>
                                        {item.name}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity >
                    )}
                    keyExtractor={(item, index) => (item.id.toString())}

                />
            )
        } else {
            <View>
                return <ActivityIndicator />
            </View>
        }
    }
    return (
        <View>
            {/* <Text style={[styles.headingTxt, { fontSize: 16 }]}>Categories</Text> */}
            {categoryData()}
        </View>);
}

export default CategoryList;