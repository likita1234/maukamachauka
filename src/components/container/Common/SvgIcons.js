import React from 'react';
import { View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import Google from '../../../assets/svg/google.svg';
import AppLogo from '../../../assets/svg/appLogo.svg';

export { Google, AppLogo };

export const SvgIcon = ({ width = 30, height = 30, icon, style }) => (
    <View style={style}>
        <SvgXml width={width} height={height} xml={icon} />
    </View>
);