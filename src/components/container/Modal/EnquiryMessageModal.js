import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Keyboard } from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/AntDesign';
import { connect } from 'react-redux';
import { sendBrandEnquiry } from '../../../actions';
import { showErrToast, showSuccessToast } from '../../../config/functions';
import { styles } from '../Dashboard/DashboardStyles';
import colors from '../../../config/colors';

class EnquiryMessageModal extends Component {
    state = {
        message: null,
    }

    sendEnquiryMessage = () => {
        Keyboard.dismiss();
        const { idToEnquiry, fromScreen } = this.props;
        const { message } = this.state;
        this.props.sendBrandEnquiry(idToEnquiry, message, fromScreen);
    }


    render() {
        const { error, success, visible, close } = this.props;
        return (
            <Modal
                backdropColor={'#000000'}
                animationType={'slide'}
                isVisible={visible}
                onRequestClose={() => console.warn('This is close request')} >
                <View
                    style={{
                        marginLeft: 10,
                        marginRight: 10,
                        alignItems: 'center',
                        height: 280,
                        backgroundColor: colors.white
                    }}>

                    <View style={styles.iconContainer}>
                        <Text style={{ fontSize: 16, fontWeight: '400' }}>Enquiry Message</Text>
                        <TouchableOpacity onPress={close}>
                            <Icon name="close" size={20} color="#000" />
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        width: '90%',
                        marginTop: 60,
                        borderColor: colors.lightishgray,
                        borderWidth: 1,
                        height: 120,

                    }}>
                        <TextInput
                            placeholder={"Your message here"}
                            onChangeText={val => this.setState({ message: val })}
                            //autoFocus={true}
                            multiline={true}
                        />
                    </View>

                    <TouchableOpacity style={styles.btnStyle} onPress={() => this.sendEnquiryMessage()}>
                        <Text style={styles.txtStyle}>send</Text>
                    </TouchableOpacity>
                    {error && (
                        <Text style={{ color: colors.red }}>{error}</Text>
                    )}
                    {success && (
                        <Text style={{ color: colors.accentColor }}>{success}</Text>
                    )}
                </View>
            </Modal>
        );
    }
}

// const mapStateToProps = state => {
//     const { error, success } = state.dashboard;
//     return {
//         error, success
//     };
// }

export default EnquiryMessageModal = connect(null, {
    sendBrandEnquiry,
    //  removeErrorSuccessMsg
})(EnquiryMessageModal);
