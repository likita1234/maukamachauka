import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image } from 'react-native';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/AntDesign';
import { connect } from 'react-redux';
import { sendBrandEnquiry } from '../../../actions';
import UploadFeat from '../../../assets/img/uploadFeat.jpg';
import { showErrToast, showSuccessToast } from '../../../config/functions';
import { styles } from '../Dashboard/DashboardStyles';
import colors from '../../../config/colors';

class AddNewBrandModal extends Component {
    state = {
        brandName: null,
        chooseBImgFilename: null
    }
    addNewBrand = () => {
        const { brandName, chooseBImgFilename } = this.state;
        console.log("brandName", brandName);
        console.log("chooseBImgFilename", chooseBImgFilename);
        this.props.close();
    }

    selectPhoto = async () => {
        console.log("selectPhoto");
        const options = {
            title: 'Select Photo',
            takePhotoButtonTitle: 'Take Photo',
            chooseFromLibraryButtonTitle: 'Choose from gallery',
            quality: 1,
            //chooseImgFilename: null
        };

        ImagePicker.showImagePicker(options, response => {
            //console.log("Response = ", response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log(response);
                let fileSize = response.fileSize;
                var fileType = response.type;
                let msg = '';
                console.log('fileType', fileType);
                if (fileSize > 5242880) {
                    msg = 'The photo Size can not be more than 5MB'
                    showErrToast(msg);
                    return false;
                }
                else if (fileType !== 'image/jpeg' && fileType !== 'image/jpg' && fileType !== 'image/png' && fileType !== 'image/bmp') {
                    // msg = 'The photo must be a file of type: jpeg, jpg, bmp, png'
                    showErrToast(msg);
                    return false;
                }

                else if (response.uri) {
                    this.setState({ chooseBImgFilename: response.uri });
                }


            }
        });
    };




    render() {
        const { error, success, visible, close } = this.props;
        const { chooseBImgFilename } = this.state;
        return (
            <Modal
                backdropColor={'#000000'}
                animationType={'slide'}
                isVisible={visible}
                onRequestClose={() => console.warn('This is close request')} >
                <View
                    style={{
                        marginLeft: 10,
                        marginRight: 10,
                        alignItems: 'center',
                        height: 280,
                        backgroundColor: colors.white
                    }}>

                    <View style={styles.iconContainer}>
                        <Text style={{ fontSize: 16, fontWeight: '400' }}>Add New Brand</Text>
                        <TouchableOpacity onPress={close}>
                            <Icon name="close" size={20} color="#000" />
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        width: '100%',
                        marginTop: 60,
                        flexDirection: 'row',
                        marginLeft: 10,
                        marginRight: 10

                    }}>
                        <Text style={{ marginLeft: 20, width: 70 }}>Brand Name:</Text>
                        <TextInput
                            placeholder={"Brand Name"}
                            onChangeText={val => this.setState({ brandName: val })}
                            style={{
                                borderColor: colors.lightgray,
                                borderWidth: 1,
                                height: 40,
                                width: '64%'
                            }}
                            multiline={true}
                        />
                    </View>
                    <View style={{
                        width: '100%',
                        marginTop: 20,
                        flexDirection: 'row',
                        marginLeft: 10,
                        marginRight: 10

                    }}>
                        <Text style={{ marginLeft: 20, width: 70 }}>Brand Image:</Text>
                        <TouchableOpacity style={{ borderColor: colors.lightgray, borderWidth: 1, width: 134, padding: 6 }}
                            onPress={() => this.selectPhoto()}>
                            <Image source={chooseBImgFilename ? { uri: chooseBImgFilename } : UploadFeat}
                                style={{ width: 120, height: 80 }}
                                resizeMode="cover" />
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity style={styles.btnStyle} onPress={() => this.addNewBrand()}>
                        <Text style={styles.txtStyle}>Add</Text>
                    </TouchableOpacity>
                    {/* {error && (
                        <Text style={{ color: colors.red }}>{error}</Text>
                    )}
                    {success && (
                        <Text style={{ color: colors.accentColor }}>{success}</Text>
                    )} */}
                </View>
            </Modal >
        );
    }
}

// const mapStateToProps = state => {
//     const { error, success } = state.dashboard;
//     return {
//         error, success
//     };
// }

export default AddNewBrandModal = connect(null, {
    sendBrandEnquiry,
    //  removeErrorSuccessMsg
})(AddNewBrandModal);
