import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../../config/colors';

export const styles = StyleSheet.create({
    flatListCont: {
        width: '100%',
        flex: 1,
        marginTop: 10,
        //marginLeft: 10,
        // alignItems: 'center',
    },
    favListCont: {
        width: '86%',
        flex: 1,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },
    singleListCont: {
        flexDirection: 'row',
        marginBottom: 2,
        borderColor: colors.darkgray,
        borderWidth: 1,
        // width: '100%',
        justifyContent: 'space-between',
        padding: 6,
        height: 50,
        backgroundColor: colors.white,
        alignItems: 'center',
        // alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 10,

    },
    tagTxt: {
        marginLeft: 10,
        fontSize: 14,
        fontWeight: '300'
    },
    favItemStyle: {
        backgroundColor: colors.white,
        height: 80,
        flexDirection: 'row',
        paddingVertical: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderRadius: 10,
        marginHorizontal: 5
    },
    brandItemStyle: {
        backgroundColor: colors.white,
        height: 56,
        flexDirection: 'row',
        paddingVertical: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderRadius: 10,
        marginHorizontal: 5,
        // marginBottom: 2
    },
    singleItemView: {
        backgroundColor: '#FF6F00',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingVertical: 16,
        paddingLeft: 16,
        margin: 5,
        borderRadius: 8
    },


});