import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Animated, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../../config/colors';
import { styles } from './ProfileStyles';

const width = Dimensions.get('window').width;

const AnimateInView = (props) => {
    const [fadeAnim] = useState(new Animated.Value(0))  // Initial value for opacity: 0
    const translate_Animation_Object = fadeAnim.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [-width, 0, width],
    });

    const opacity_Animation_Object = fadeAnim.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [0, 1, 0],
    });

    React.useEffect(() => {
        Animated.timing(
            fadeAnim,
            {
                toValue: 0.5,
                duration: 510,
                useNativeDriver: true,
            }
        ).start();
    }, [])

    return (
        <Animated.View
            style={{
                ...props.style,
                opacity: opacity_Animation_Object,
                justifyContent: 'space-between',
                flexDirection: 'row',
                width: '100%',
                transform: [{ translateX: translate_Animation_Object }]
            }}
        >
            {props.children}
        </Animated.View>
    );
}

const SingleunSubscribedOffer = ({ unSubscribedOffer, subscribeOffer, currIdSubscribe, idToUnSubscribe }) => {

    return (

        <View >
            {idToUnSubscribe === unSubscribedOffer.id ?
                <View style={[styles.singleListCont, { backgroundColor: colors.lightgray }]} >
                    <AnimateInView>
                        <Text style={styles.tagTxt} > {unSubscribedOffer.name}
                        </Text>
                        <TouchableOpacity style={{ marginLeft: 10 }}
                            onPress={() => subscribeOffer(unSubscribedOffer.id)}
                            disable={currIdSubscribe && currIdSubscribe === unSubscribedOffer.id ? true : false}>
                            <Icon name="md-add-circle" size={24} color={colors.accentColor} />
                        </TouchableOpacity>
                    </AnimateInView>
                </View> :
                <View style={styles.singleListCont}>
                    <Text style={styles.tagTxt} > {unSubscribedOffer.name}
                    </Text>
                    <TouchableOpacity style={{ marginLeft: 10 }}
                        onPress={() => subscribeOffer(unSubscribedOffer.id)}
                        disable={currIdSubscribe && currIdSubscribe === unSubscribedOffer.id ? true : false}>
                        <Icon name="md-add-circle" size={24} color={colors.accentColor} />
                    </TouchableOpacity>
                </View>
            }

        </View >
    );
}

export default SingleunSubscribedOffer;