import React from 'react';
import { View, Text, } from 'react-native';
import FastImage from 'react-native-fast-image';
import { styles } from './ProfileStyles'

const SingleFollowedOffer = ({ followBData, removeBrandFromFollow }) => {
    return (
        <View style={styles.brandItemStyle}>
            {/* <View style={{ flexDirection: 'row' }}> */}
            <FastImage
                source={{
                    uri: followBData.logo_src,
                    priority: FastImage.priority.high,
                }}

                style={{ width: 80, height: 40 }}
            />
            <Text style={{ fontWeight: 'bold', fontSize: 16, padding: 6 }}>{followBData.name}</Text>
            {/* </View> */}
            {/* <TouchableOpacity onPress={() => removeBrandFromFollow(followBData.id)}>
                <CloseIcon name="close" size={14} />
            </TouchableOpacity> */}

        </View>
    );
}

export default SingleFollowedOffer;