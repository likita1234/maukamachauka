import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CloseIcon from 'react-native-vector-icons/AntDesign';
import colors from '../../../config/colors';
import Moment from 'moment';
import { styles } from './ProfileStyles';
import CustomText from '../../../config/text';

const SingleFavouriteOffer = ({ favOffer, idToRemove, removeOfferFromFav }) => {
    const offerExpiryDate = () => {
        let offerExpiryDate = favOffer.expires_in;
        let remainingDate = new Moment().to(Moment(offerExpiryDate));
        return (
            <View>
                <Text style={{ fontSize: 12 }}>Expire {remainingDate}</Text>
            </View>
        )
    }
    return (
        <View style={styles.favItemStyle} >

            <FastImage
                source={{
                    uri: favOffer.image_src,
                    priority: FastImage.priority.high,
                }}

                style={{ width: 70, height: 60, marginLeft: 10 }}
            />
            <View style={{ justifyContent: 'space-around', flexDirection: 'row' }}>
                <View style={{ marginLeft: 8, width: idToRemove === favOffer.id ? '56%' : '70%' }}>
                    <Text style={{ fontSize: 15, fontWeight: 'bold' }} > {favOffer.title}</Text>
                    {offerExpiryDate()}
                </View>

                {idToRemove === favOffer.id ?
                    <View style={{ marginLeft: 8, width: '44%' }}>
                        <CustomText style={{ marginTop: 20, marginRight: 80 }}>Removing...</CustomText>
                    </View> :
                    <TouchableOpacity
                        style={{ marginLeft: 8, width: '30%' }}
                        onPress={() => removeOfferFromFav(favOffer.id)}>
                        <CloseIcon name="close" size={16} />
                    </TouchableOpacity>
                }

            </View>

        </View >
    );
}

export default SingleFavouriteOffer;