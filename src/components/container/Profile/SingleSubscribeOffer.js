import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Animated, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import { styles } from './ProfileStyles';
import colors from '../../../config/colors';

const width = Dimensions.get('window').width;

const AnimateInView = (props) => {
    const [fadeAnim] = useState(new Animated.Value(0))  // Initial value for opacity: 0
    const translate_Animation_Object = fadeAnim.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [-width, 0, width],
    });

    const opacity_Animation_Object = fadeAnim.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [0, 1, 0],
    });

    React.useEffect(() => {
        Animated.timing(
            fadeAnim,
            {
                toValue: 0.5,
                duration: 510,
                useNativeDriver: true,
            }
        ).start();
    }, [])

    return (
        <Animated.View                 // Special animatable View
            style={{
                ...props.style,
                opacity: opacity_Animation_Object,         // Bind opacity to animated value
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                transform: [{ translateX: translate_Animation_Object }]
            }}
        >
            {props.children}
        </Animated.View>
    );
}

const SingleSubscribedOffer = ({ subscribedOffer, unSubscribeOffer, currIdUnSubscribe, idToSubscribe }) => {

    return (
        <View>
            {idToSubscribe === subscribedOffer.id ?

                <View style={[styles.singleListCont, { backgroundColor: colors.lightgray }]}>
                    <AnimateInView>
                        <Text style={styles.tagTxt} > {subscribedOffer.name}
                        </Text>

                        <TouchableOpacity
                            onPress={() => unSubscribeOffer(subscribedOffer.id)}
                            disable={currIdUnSubscribe && currIdUnSubscribe === subscribedOffer.id ? true : false}>
                            <Icon name="circle-with-minus" size={24} color={colors.primaryColor} />
                        </TouchableOpacity>
                    </AnimateInView>

                </View> :
                <View style={styles.singleListCont}>
                    <Text style={styles.tagTxt} > {subscribedOffer.name}
                    </Text>

                    <TouchableOpacity
                        onPress={() => unSubscribeOffer(subscribedOffer.id)}
                        disable={currIdUnSubscribe && currIdUnSubscribe === subscribedOffer.id ? true : false}>
                        <Icon name="circle-with-minus" size={24} color={colors.primaryColor} />
                    </TouchableOpacity>
                </View>
            }
        </View >
    );
}

export default SingleSubscribedOffer;