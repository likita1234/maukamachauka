/**
 * @providesModule SmallText
 */
import { useScreens } from 'react-native-screens';
import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import textStyles from '../../../config/typography';
import colors from '../../../config/colors';

//useScreens();

class SmallText extends React.Component {
  render() {
    return (
      <Text
        {...this.props}
        style={[
          this.props.bold == true
            ? textStyles.smallTextBold
            : textStyles.smallText,
          { color: this.props.color },
          this.props.style,
        ]}
        allowFontScaling={false}>
        {this.props.children}
      </Text>
    );
  }
}
SmallText.defaultProps = {
  color: colors.night,
  bold: false,
};
export default SmallText;
