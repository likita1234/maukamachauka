import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native';

import spacing from '../../../config/spacing';
import colors from '../../../config/colors';

class CustomLink extends Component {
    render() {
        return (
            <TouchableOpacity {...this.props}>
                <View style={[spacing.contentCover, this.props.style]}>
                    <Text style={styles.textStyle}>{this.props.children}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    textStyle: {
        color: colors.primaryColor,
        fontSize: 12
    },
});

export default CustomLink;
