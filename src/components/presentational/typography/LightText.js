/**
 * @providesModule LargeText
 */
import React from 'react';
import {Text} from 'react-native';
import textStyles from '../../../config/typography';

class LightText extends React.Component {
  render() {
    return (
      <Text
        {...this.props}
        style={[
          this.props.bold == true
            ? textStyles.lightTextBold
            : textStyles.lightText,
          {
            color: this.props.color,
            textAlign: this.props.textAlign,
            paddingBottom: this.props.paddingBottom,
            paddingTop: this.props.paddingTop,
            lineHeight: this.props.lineHeight,
          },
          this.props.style,
        ]}
        allowFontScaling={false}>
        {this.props.children}
      </Text>
    );
  }
}
LightText.defaultProps = {
  color: '#8d8c8c',
  bold: false,
};
export default LightText;
