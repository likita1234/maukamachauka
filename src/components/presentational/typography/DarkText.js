/**
 * @providesModule LargeText
 */
import React from 'react';
import { Text } from 'react-native';
import textStyles from '../../../config/typography';
import colors from '../../../config/colors';
class DarkText extends React.Component {
  render() {
    return (
      <Text
        {...this.props}
        style={[
          this.props.bold == true
            ? textStyles.mediumTextBold
            : textStyles.mediumText,
          {
            color: this.props.color,
            paddingBottom: this.props.paddingBottom,
            paddingTop: this.props.paddingTop,
            lineHeight: this.props.lineHeight,
            textAlign: this.props.textAlign,
          },
          this.props.style,
        ]}
        allowFontScaling={false} //property not allow font size change on phone setting for font change
      >
        {this.props.children}
      </Text>
    );
  }
}
DarkText.defaultProps = {
  color: colors.black,
  bold: false,
  paddingBottom: 5,
  paddingTop: 5,
  lineHeight: 15,
  textAlign: 'justify',
};
export default DarkText;
