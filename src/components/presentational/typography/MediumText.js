/**
 * @providesModule MediumText
 */
import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import textStyles from '../../../config/typography';
import colors from '../../../config/colors';
class MediumText extends React.Component {
  render() {
    return (
      <Text
        {...this.props}
        style={[
          this.props.bold == true
            ? textStyles.mediumTextBold
            : textStyles.mediumText,
          { color: this.props.color },
          this.props.style,
        ]}
        allowFontScaling={false}>
        {this.props.children}
      </Text>
    );
  }
}
MediumText.defaultProps = {
  color: colors.night,
  bold: false,
};
export default MediumText;
