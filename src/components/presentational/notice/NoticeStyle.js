import { StyleSheet } from 'react-native';
import colors from '../../../config/colors';

export const styles = StyleSheet.create({
    noticeWrapper: {
        backgroundColor: colors.black,
        alignItems: 'center',
        width: '100%',
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    noticePopupWrapper: {
        backgroundColor: colors.primaryColor,
        alignItems: 'center',
        width: '100%',
        marginBottom: 10,
        padding: 30
    },
    noticeText: {
        color: colors.white,
        padding: 2,
        textAlign: 'center',
        fontSize: 12

    },
    orngBtn: {
        padding: 6,
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: colors.primaryColor,
    },
    retryBtn: {
        marginTop: 5,
        borderColor: '#fff',
        borderWidth: 1,
        width: 60,
        height: 20,
        marginBottom: 5
    },
    retryBtn2: {
        marginTop: 20,
        borderColor: '#fff',
        borderWidth: 2,
        width: 60,
        height: 30,
        marginBottom: 5,
    }
});
