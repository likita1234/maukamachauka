import React, { useState } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import { styles } from './NoticeStyle';
import Modal from 'react-native-modal';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import colors from '../../../config/colors';

const NoInternetNotice = ({ onretryNetConnection, showPopUp = false }) => {
    const [loader, showLoader] = useState(false);

    const onRetryPress = () => {
        showLoader(true);
        onretryNetConnection();
        setTimeout(() => {
            showLoader(false);
        }, 5000);
    }

    if (showPopUp) {
        return (<Modal
            backdropColor={'#000000'}
            animationType={'slide'}
            transparent={true}
            isVisible={true}
            onBackdropPress={() => { }}
            onRequestClose={() => console.warn('This is close request')}>
            <View style={styles.noticePopupWrapper}>
                <Icon name={'wifi-off'} color={'#fff'} size={34} />
                <Text allowFontScaling={false} style={[styles.noticeText, { marginTop: 20 }]}>No Internet Connection!</Text>
                <Text allowFontScaling={false} style={styles.noticeText}>Please switch to 3G or a different Wi-Fi network</Text>
                <TouchableOpacity onPress={() => onRetryPress()}>
                    {(loader) ? <ActivityIndicator color={colors.white} /> : <View style={[styles.retryBtn2]}><Text style={styles.noticeText}>Retry</Text></View>}</TouchableOpacity>
            </View>
        </Modal>)
    }

    return (<View style={styles.noticeWrapper}><Text style={styles.noticeText}>No Internet Connection!</Text>
        <TouchableOpacity onPress={() => onRetryPress()}>
            {(loader) ? <ActivityIndicator color={colors.white} /> :
                <View style={[styles.retryBtn]}>
                    <Text allowFontScaling={false} style={styles.noticeText}>Retry</Text>
                </View>
            }
        </TouchableOpacity>
    </View>);


};

export default NoInternetNotice;
