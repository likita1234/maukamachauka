import React from 'react';
import { Dimensions } from 'react-native';
import SvgAnimatedLinearGradient from 'react-native-svg-animated-linear-gradient'
import { Circle, Rect } from 'react-native-svg';

const SkeletonScreen = () => {
    let dimWidth = Dimensions.get('window').width;
    const svgFullWidth = dimWidth - 35;

    return <SvgAnimatedLinearGradient
        height={1000}
        width={dimWidth}
    >

        {/* First */}
        <Circle cx="40" cy="30" r="30" />
        <Rect x="75" y="13" rx="4" ry="4" width="100" height="13" />
        <Rect x="264" y="35" rx="4" ry="4" width="80" height="10" />
        <Rect x="15" y="75" rx="0" ry="0" width={svgFullWidth} height="180" />

        <Rect x="15" y="260" rx="0" ry="0" width="220" height="10" />
        <Rect x="15" y="280" rx="0" ry="0" width="340" height="10" />

        {/* Second */}
        <Circle cx="40" cy="350" r="30" />
        <Rect x="75" y="340" rx="4" ry="4" width="100" height="13" />
        <Rect x="264" y="360" rx="4" ry="4" width="80" height="10" />
        <Rect x="15" y="400" rx="0" ry="0" width={svgFullWidth} height="180" />

        <Rect x="15" y="586" rx="0" ry="0" width="220" height="10" />
        <Rect x="15" y="606" rx="0" ry="0" width="340" height="10" />

        {/* Third */}
        <Circle cx="40" cy="680" r="30" />
        <Rect x="75" y="670" rx="4" ry="4" width="100" height="13" />
        <Rect x="264" y="690" rx="4" ry="4" width="80" height="10" />
        <Rect x="15" y="730" rx="0" ry="0" width={svgFullWidth} height="180" />

        <Rect x="15" y="920" rx="0" ry="0" width="220" height="10" />
        <Rect x="15" y="940" rx="0" ry="0" width="340" height="10" />


    </SvgAnimatedLinearGradient>;
}

export default SkeletonScreen;