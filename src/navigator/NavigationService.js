import { NavigationActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
    _navigator = navigatorRef;
}

function navigate(routeName, params, subRouteName) {
    _navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
            // in case you want to navigate into specific sub route
            //action: NavigationActions.navigate({ routeName: subRouteName })
        }),
    );
}

export default {
    navigate,
    setTopLevelNavigator,
};
