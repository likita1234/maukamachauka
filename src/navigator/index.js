import React, { Component } from 'react';
import { View } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import NavigationService from './NavigationService';
import SocialLoginScreen from '../screens/App/Auth/SocialLogInScreen';
import ForgotPassword from '../screens/App/Auth/ForgetPasswordScreen';
import AuthLoadingScreen from '../screens/AuthLoading';
import CategoryOfferListScreen from '../screens/App/Dashboard/CategoryOfferListScreen';
import DashboardScreen from '../screens/App/Dashboard/DashboardScreen';
import NotificationScreen from '../screens/App/Dashboard/NotificationScreen';
import OfferDetailScreen from '../screens/App/Dashboard/OfferDetailScreen';
import BrandDetailScreen from '../screens/App/Dashboard/BrandDetailScreen';
import ProfileScreen from '../screens/App/Profile/ProfileScreen';
import PersonalDetailScreen from '../screens/App/Profile/PersonalDetailScreen';
import FavouriteOfferScreen from '../screens/App/Profile/FavouriteOfferScreen';
import SubUnSubscribeOfferScreen from '../screens/App/Profile/SubUnSubscribeOfferScreen';
import FollowedBrandScreen from '../screens/App/Profile/FollowedBrandScreen';
import AboutUsScreen from '../screens/App/Profile/AboutUsScreen';
import UpdateProfileScreen from '../screens/App/Profile/UpdateProfileScreen';
import AddOfferScreen from '../screens/App/AddOffer/AddOfferScreen';
import Icon from 'react-native-vector-icons/AntDesign';
import FaIcon from 'react-native-vector-icons/FontAwesome';
import colors from '../config/colors';
const AuthStack = createStackNavigator(
  {
    Login: { screen: SocialLoginScreen },
    ForgetPsw: { screen: ForgotPassword },
  },
  {
    defaultNavigationOptions: {
      headerShown: false,
    },
  },
);

const DashboardStack = createStackNavigator(
  {
    Dashboard: { screen: DashboardScreen },
    Notification: { screen: NotificationScreen },
    OfferDetail: { screen: OfferDetailScreen },
    BrandDetail: { screen: BrandDetailScreen },
    CatOffer: { screen: CategoryOfferListScreen }
  },
  {
    initialRouteName: 'Dashboard',
    defaultNavigationOptions: {
      headerShown: false
    }
    // defaultNavigationOptions: ({ navigation }) => ({
    //     headerTitleAlign: 'center'
    // })
  },
);


const FavouriteStack = createStackNavigator(
  {

    Favourite: { screen: FavouriteOfferScreen },

  },

  {
    initialRouteName: 'Favourite',
    defaultNavigationOptions: {
      headerShown: false
    }
  },

);

const ProfileStack = createStackNavigator(
  {
    Profile: { screen: ProfileScreen },
    PersonalInfo: { screen: PersonalDetailScreen },
    UpdateProfile: { screen: UpdateProfileScreen },
    Favourite: { screen: FavouriteOfferScreen },
    Subscribe: { screen: SubUnSubscribeOfferScreen },
    Follow: { screen: FollowedBrandScreen },
    About: { screen: AboutUsScreen }
  },
  {
    initialRouteName: 'Profile',
    defaultNavigationOptions: {
      headerShown: false,
    },
  },
);

const AppStack = createBottomTabNavigator(
  {
    Dashboard: {
      screen: DashboardStack,
      navigationOptions: {
        // tabBarLabel: 'Home',
        tabBarLabel: () => {
          return null;
        },
        tabBarIcon: ({ tintColor }) => (
          <Icon
            style={{ position: 'relative', top: 10, marginBottom: 6 }}
            name="home"
            size={24}
            color={tintColor}
          />
        ),
      },
    },

    WishList: {
      screen: FavouriteStack,
      navigationOptions: {
        // tabBarLabel: 'Discover',
        tabBarLabel: () => {
          return null;
        },
        tabBarIcon: ({ tintColor }) => (
          <FaIcon
            style={{ position: 'relative', top: 10, marginBottom: 6 }}
            name="heart-o"
            size={24}
            color={tintColor}
          />
        ),
      },
    },
    Profile: {
      screen: ProfileStack,
      navigationOptions: {
        // tabBarLabel: 'Profile',
        tabBarLabel: () => {
          return null;
        },
        tabBarIcon: ({ tintColor }) => (
          <Icon
            style={{ position: 'relative', top: 10, marginBottom: 6 }}
            name="user"
            size={24}
            color={tintColor}
          />
        ),
      },
    },
    AddOffer: {
      screen: AddOfferScreen,
      navigationOptions: {
        // tabBarLabel: 'Add',
        tabBarLabel: () => {
          return null;
        },
        tabBarIcon: ({ tintColor }) => (
          <Icon
            style={{ position: 'relative', top: 10, marginBottom: 6 }}
            name="plus"
            size={24}
            color={tintColor}
          />
        ),
      },
    },
  },
  {
    initialRouteName: 'Dashboard',
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarOptions: {
        activeTintColor: '#E51E2A',
        inactiveTintColor: colors.lightgray,
        showLabel: true,

        style: {
          borderTopColor: 'transparent',
          backgroundColor: '#fff',
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
          paddingBottom: 10,
        },
      },
    }),
  },
);

const RootStack = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Auth: AuthStack,
    App: AppStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

const AppContainer = createAppContainer(RootStack);

export default class Nav extends Component {
  render() {
    return (
      <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
